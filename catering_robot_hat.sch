<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.05" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.005" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="STMicroelectronics" urn="urn:adsk.eagle:library:7560276">
<packages>
<package name="QFP-05-64P" urn="urn:adsk.eagle:footprint:7539873/2" library_version="4">
<smd name="P$1" x="-5.75" y="3.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$2" x="-5.75" y="3.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$3" x="-5.75" y="2.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$4" x="-5.75" y="2.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="P$5" x="-5.75" y="1.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="P$6" x="-5.75" y="1.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="P$7" x="-5.75" y="0.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="P$8" x="-5.75" y="0.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="P$9" x="-5.75" y="-0.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="P$10" x="-5.75" y="-0.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="P$11" x="-5.75" y="-1.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="P$12" x="-5.75" y="-1.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="P$13" x="-5.75" y="-2.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="P$14" x="-5.75" y="-2.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="P$15" x="-5.75" y="-3.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="P$16" x="-5.75" y="-3.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="P$17" x="-3.75" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="P$18" x="-3.25" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="P$19" x="-2.75" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="P$20" x="-2.25" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="P$21" x="-1.75" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="P$22" x="-1.25" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="P$23" x="-0.75" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="P$24" x="-0.25" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="P$25" x="0.25" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="P$26" x="0.75" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="P$27" x="1.25" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="P$28" x="1.75" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="P$29" x="2.25" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="P$30" x="2.75" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="P$31" x="3.25" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="P$32" x="3.75" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="P$33" x="5.7" y="-3.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$34" x="5.7" y="-3.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$35" x="5.7" y="-2.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$36" x="5.7" y="-2.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$37" x="5.7" y="-1.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$38" x="5.7" y="-1.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$39" x="5.7" y="-0.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$40" x="5.7" y="-0.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$41" x="5.7" y="0.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$42" x="5.7" y="0.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$43" x="5.7" y="1.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$44" x="5.7" y="1.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$45" x="5.7" y="2.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$46" x="5.7" y="2.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$47" x="5.7" y="3.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$48" x="5.7" y="3.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="P$49" x="3.75" y="5.8" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="P$50" x="3.25" y="5.8" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="P$51" x="2.75" y="5.8" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="P$52" x="2.25" y="5.8" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="P$53" x="1.75" y="5.8" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="P$54" x="1.25" y="5.8" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="P$55" x="0.75" y="5.8" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="P$56" x="0.25" y="5.8" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="P$57" x="-0.25" y="5.8" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="P$58" x="-0.75" y="5.8" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="P$59" x="-1.25" y="5.8" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="P$60" x="-1.75" y="5.8" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="P$61" x="-2.25" y="5.8" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="P$62" x="-2.75" y="5.8" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="P$63" x="-3.25" y="5.8" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="P$64" x="-3.75" y="5.8" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<wire x1="-4" y1="5" x2="5" y2="5" width="0.127" layer="21"/>
<wire x1="5" y1="5" x2="5" y2="-5" width="0.127" layer="21"/>
<wire x1="5" y1="-5" x2="-5" y2="-5" width="0.127" layer="21"/>
<wire x1="-5" y1="-5" x2="-5" y2="4" width="0.127" layer="21"/>
<wire x1="-5" y1="4" x2="-4" y2="5" width="0.127" layer="21"/>
<circle x="-4" y="4" locked="yes" radius="0.25495" width="0.127" layer="21"/>
<text x="-4" y="6.5" size="1.016" layer="21">&gt;NAME</text>
</package>
</packages>
<packages3d>
<package3d name="QFP50P1200X1200X120-64" urn="urn:adsk.eagle:package:7539880/3" type="model" library_version="4">
<packageinstances>
<packageinstance name="QFP-05-64P"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="STM32F446RET6" urn="urn:adsk.eagle:symbol:8397168/2" library_version="6">
<wire x1="-30.48" y1="-63.5" x2="27.94" y2="-63.5" width="0.254" layer="94"/>
<wire x1="27.94" y1="-63.5" x2="27.94" y2="20.32" width="0.254" layer="94"/>
<wire x1="27.94" y1="20.32" x2="-30.48" y2="20.32" width="0.254" layer="94"/>
<wire x1="-30.48" y1="20.32" x2="-30.48" y2="-63.5" width="0.254" layer="94"/>
<text x="-30.48" y="21.59" size="1.778" layer="95">&gt;NAME</text>
<text x="-30.48" y="-66.04" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VBAT" x="-35.56" y="17.78" length="middle"/>
<pin name="PC13" x="-35.56" y="15.24" length="middle"/>
<pin name="PC14/OSC32_IN" x="-35.56" y="12.7" length="middle"/>
<pin name="PC15/OSC32_OUT" x="-35.56" y="10.16" length="middle"/>
<pin name="PH0/OSC_IN" x="-35.56" y="7.62" length="middle"/>
<pin name="PH1/OSC_OUT" x="-35.56" y="5.08" length="middle"/>
<pin name="NRST" x="-35.56" y="2.54" length="middle"/>
<pin name="PC0" x="-35.56" y="0" length="middle"/>
<pin name="PC1" x="-35.56" y="-2.54" length="middle"/>
<pin name="PC2" x="-35.56" y="-5.08" length="middle"/>
<pin name="PC3" x="-35.56" y="-7.62" length="middle"/>
<pin name="VSSA" x="-35.56" y="-10.16" length="middle"/>
<pin name="VDDA" x="-35.56" y="-12.7" length="middle"/>
<pin name="PA0" x="-35.56" y="-15.24" length="middle"/>
<pin name="PA1" x="-35.56" y="-17.78" length="middle"/>
<pin name="PA2" x="-35.56" y="-20.32" length="middle"/>
<pin name="PA3" x="-35.56" y="-22.86" length="middle"/>
<pin name="VSS1" x="-35.56" y="-25.4" length="middle"/>
<pin name="VDD1" x="-35.56" y="-27.94" length="middle"/>
<pin name="PA4" x="-35.56" y="-30.48" length="middle"/>
<pin name="PA5" x="-35.56" y="-33.02" length="middle"/>
<pin name="PA6" x="-35.56" y="-35.56" length="middle"/>
<pin name="PA7" x="-35.56" y="-38.1" length="middle"/>
<pin name="PC4" x="-35.56" y="-40.64" length="middle"/>
<pin name="PC5" x="-35.56" y="-43.18" length="middle"/>
<pin name="PB0" x="-35.56" y="-45.72" length="middle"/>
<pin name="PB1" x="-35.56" y="-48.26" length="middle"/>
<pin name="PB2/BOOT1" x="-35.56" y="-50.8" length="middle"/>
<pin name="PB10" x="-35.56" y="-53.34" length="middle"/>
<pin name="VCAP_1" x="-35.56" y="-55.88" length="middle"/>
<pin name="VSS2" x="-35.56" y="-58.42" length="middle"/>
<pin name="VDD2" x="-35.56" y="-60.96" length="middle"/>
<pin name="PB12" x="33.02" y="-60.96" length="middle" rot="R180"/>
<pin name="PB13" x="33.02" y="-58.42" length="middle" rot="R180"/>
<pin name="PB14" x="33.02" y="-55.88" length="middle" rot="R180"/>
<pin name="PB15" x="33.02" y="-53.34" length="middle" rot="R180"/>
<pin name="PC6" x="33.02" y="-50.8" length="middle" rot="R180"/>
<pin name="PC7" x="33.02" y="-48.26" length="middle" rot="R180"/>
<pin name="PC8" x="33.02" y="-45.72" length="middle" rot="R180"/>
<pin name="PC9" x="33.02" y="-43.18" length="middle" rot="R180"/>
<pin name="PA8" x="33.02" y="-40.64" length="middle" rot="R180"/>
<pin name="PA9" x="33.02" y="-38.1" length="middle" rot="R180"/>
<pin name="PA10" x="33.02" y="-35.56" length="middle" rot="R180"/>
<pin name="PA11" x="33.02" y="-33.02" length="middle" rot="R180"/>
<pin name="PA12" x="33.02" y="-30.48" length="middle" rot="R180"/>
<pin name="PA13/STMS/SWDIO" x="33.02" y="-27.94" length="middle" rot="R180"/>
<pin name="VSS3" x="33.02" y="-25.4" length="middle" rot="R180"/>
<pin name="VDD3" x="33.02" y="-22.86" length="middle" rot="R180"/>
<pin name="PA14/JTCK/SWCLK" x="33.02" y="-20.32" length="middle" rot="R180"/>
<pin name="PA15/JTDI" x="33.02" y="-17.78" length="middle" rot="R180"/>
<pin name="PC10" x="33.02" y="-15.24" length="middle" rot="R180"/>
<pin name="PC11" x="33.02" y="-12.7" length="middle" rot="R180"/>
<pin name="PC12" x="33.02" y="-10.16" length="middle" rot="R180"/>
<pin name="PD2" x="33.02" y="-7.62" length="middle" rot="R180"/>
<pin name="PB3/JTDO/TRACESWO" x="33.02" y="-5.08" length="middle" rot="R180"/>
<pin name="PB4/NJTRST" x="33.02" y="-2.54" length="middle" rot="R180"/>
<pin name="PB5" x="33.02" y="0" length="middle" rot="R180"/>
<pin name="PB6" x="33.02" y="2.54" length="middle" rot="R180"/>
<pin name="PB7" x="33.02" y="5.08" length="middle" rot="R180"/>
<pin name="BOOT0" x="33.02" y="7.62" length="middle" rot="R180"/>
<pin name="PB8" x="33.02" y="10.16" length="middle" rot="R180"/>
<pin name="PB9" x="33.02" y="12.7" length="middle" rot="R180"/>
<pin name="VSS4" x="33.02" y="15.24" length="middle" rot="R180"/>
<pin name="VDD4" x="33.02" y="17.78" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="STM32F446RET6" urn="urn:adsk.eagle:component:8397169/2" prefix="U" library_version="6">
<gates>
<gate name="G$1" symbol="STM32F446RET6" x="35.56" y="-17.78"/>
</gates>
<devices>
<device name="QFP" package="QFP-05-64P">
<connects>
<connect gate="G$1" pin="BOOT0" pad="P$60"/>
<connect gate="G$1" pin="NRST" pad="P$7"/>
<connect gate="G$1" pin="PA0" pad="P$14"/>
<connect gate="G$1" pin="PA1" pad="P$15"/>
<connect gate="G$1" pin="PA10" pad="P$43"/>
<connect gate="G$1" pin="PA11" pad="P$44"/>
<connect gate="G$1" pin="PA12" pad="P$45"/>
<connect gate="G$1" pin="PA13/STMS/SWDIO" pad="P$46"/>
<connect gate="G$1" pin="PA14/JTCK/SWCLK" pad="P$49"/>
<connect gate="G$1" pin="PA15/JTDI" pad="P$50"/>
<connect gate="G$1" pin="PA2" pad="P$16"/>
<connect gate="G$1" pin="PA3" pad="P$17"/>
<connect gate="G$1" pin="PA4" pad="P$20"/>
<connect gate="G$1" pin="PA5" pad="P$21"/>
<connect gate="G$1" pin="PA6" pad="P$22"/>
<connect gate="G$1" pin="PA7" pad="P$23"/>
<connect gate="G$1" pin="PA8" pad="P$41"/>
<connect gate="G$1" pin="PA9" pad="P$42"/>
<connect gate="G$1" pin="PB0" pad="P$26"/>
<connect gate="G$1" pin="PB1" pad="P$27"/>
<connect gate="G$1" pin="PB10" pad="P$29"/>
<connect gate="G$1" pin="PB12" pad="P$33"/>
<connect gate="G$1" pin="PB13" pad="P$34"/>
<connect gate="G$1" pin="PB14" pad="P$35"/>
<connect gate="G$1" pin="PB15" pad="P$36"/>
<connect gate="G$1" pin="PB2/BOOT1" pad="P$28"/>
<connect gate="G$1" pin="PB3/JTDO/TRACESWO" pad="P$55"/>
<connect gate="G$1" pin="PB4/NJTRST" pad="P$56"/>
<connect gate="G$1" pin="PB5" pad="P$57"/>
<connect gate="G$1" pin="PB6" pad="P$58"/>
<connect gate="G$1" pin="PB7" pad="P$59"/>
<connect gate="G$1" pin="PB8" pad="P$61"/>
<connect gate="G$1" pin="PB9" pad="P$62"/>
<connect gate="G$1" pin="PC0" pad="P$8"/>
<connect gate="G$1" pin="PC1" pad="P$9"/>
<connect gate="G$1" pin="PC10" pad="P$51"/>
<connect gate="G$1" pin="PC11" pad="P$52"/>
<connect gate="G$1" pin="PC12" pad="P$53"/>
<connect gate="G$1" pin="PC13" pad="P$2"/>
<connect gate="G$1" pin="PC14/OSC32_IN" pad="P$3"/>
<connect gate="G$1" pin="PC15/OSC32_OUT" pad="P$4"/>
<connect gate="G$1" pin="PC2" pad="P$10"/>
<connect gate="G$1" pin="PC3" pad="P$11"/>
<connect gate="G$1" pin="PC4" pad="P$24"/>
<connect gate="G$1" pin="PC5" pad="P$25"/>
<connect gate="G$1" pin="PC6" pad="P$37"/>
<connect gate="G$1" pin="PC7" pad="P$38"/>
<connect gate="G$1" pin="PC8" pad="P$39"/>
<connect gate="G$1" pin="PC9" pad="P$40"/>
<connect gate="G$1" pin="PD2" pad="P$54"/>
<connect gate="G$1" pin="PH0/OSC_IN" pad="P$5"/>
<connect gate="G$1" pin="PH1/OSC_OUT" pad="P$6"/>
<connect gate="G$1" pin="VBAT" pad="P$1"/>
<connect gate="G$1" pin="VCAP_1" pad="P$30"/>
<connect gate="G$1" pin="VDD1" pad="P$19"/>
<connect gate="G$1" pin="VDD2" pad="P$32"/>
<connect gate="G$1" pin="VDD3" pad="P$48"/>
<connect gate="G$1" pin="VDD4" pad="P$64"/>
<connect gate="G$1" pin="VDDA" pad="P$13"/>
<connect gate="G$1" pin="VSS1" pad="P$18"/>
<connect gate="G$1" pin="VSS2" pad="P$31"/>
<connect gate="G$1" pin="VSS3" pad="P$47"/>
<connect gate="G$1" pin="VSS4" pad="P$63"/>
<connect gate="G$1" pin="VSSA" pad="P$12"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7539880/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-jst-ph" urn="urn:adsk.eagle:library:7974963">
<packages>
<package name="B6B-PH-K-S" urn="urn:adsk.eagle:footprint:7974975/2" library_version="7">
<wire x1="12" y1="1.7" x2="9.4" y2="1.7" width="0.127" layer="21"/>
<wire x1="9.4" y1="1.7" x2="0.6" y2="1.7" width="0.127" layer="21"/>
<wire x1="0.6" y1="1.7" x2="-2" y2="1.7" width="0.127" layer="21"/>
<wire x1="12" y1="-3" x2="-2" y2="-3" width="0.127" layer="21"/>
<wire x1="-2" y1="-3" x2="-2" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.6" x2="-2" y2="0.3" width="0.127" layer="21"/>
<wire x1="-2" y1="0.3" x2="-2" y2="1.7" width="0.127" layer="21"/>
<wire x1="12" y1="-3" x2="12" y2="-0.6" width="0.127" layer="21"/>
<wire x1="12" y1="-0.6" x2="12" y2="0.3" width="0.127" layer="21"/>
<wire x1="12" y1="0.3" x2="12" y2="1.7" width="0.127" layer="21"/>
<wire x1="11.4" y1="-2.4" x2="-1.4" y2="-2.4" width="0.127" layer="21"/>
<wire x1="11.4" y1="-2.4" x2="11.4" y2="-0.6" width="0.127" layer="21"/>
<wire x1="11.4" y1="-0.6" x2="11.4" y2="0.3" width="0.127" layer="21"/>
<wire x1="11.4" y1="0.3" x2="11.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="11.4" y1="1.1" x2="9.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-2.4" x2="-1.4" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-0.6" x2="-1.4" y2="0.3" width="0.127" layer="21"/>
<wire x1="-1.4" y1="0.3" x2="-1.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="12" y1="0.3" x2="11.4" y2="0.3" width="0.127" layer="21"/>
<wire x1="12" y1="-0.6" x2="11.4" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-0.6" x2="-2" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-1.4" y1="0.3" x2="-2" y2="0.3" width="0.127" layer="21"/>
<wire x1="0.6" y1="1.1" x2="-1.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="0.6" y1="1.1" x2="0.6" y2="1.7" width="0.127" layer="21"/>
<wire x1="9.4" y1="1.1" x2="9.4" y2="1.7" width="0.127" layer="21"/>
<pad name="2" x="2" y="0" drill="0.8" rot="R180"/>
<pad name="1" x="0" y="0" drill="0.8" shape="square" rot="R180"/>
<pad name="3" x="4" y="0" drill="0.8" rot="R180"/>
<pad name="4" x="6" y="0" drill="0.8" rot="R180"/>
<pad name="5" x="8" y="0" drill="0.8" rot="R180"/>
<pad name="6" x="10" y="0" drill="0.8" rot="R180"/>
<text x="4" y="-3.5001" size="1.27" layer="25" rot="R180">&gt;NAME</text>
<text x="0.0999" y="3.4999" size="1.27" layer="21" rot="R180">1</text>
</package>
<package name="S6B-PH-K-S" urn="urn:adsk.eagle:footprint:7974967/3" library_version="7">
<wire x1="12" y1="1.35" x2="11.4" y2="1.35" width="0.127" layer="21"/>
<wire x1="-1.4" y1="1.35" x2="-2" y2="1.35" width="0.127" layer="21"/>
<wire x1="12" y1="-6.25" x2="-2" y2="-6.25" width="0.127" layer="21"/>
<wire x1="-2" y1="-6.25" x2="-2" y2="1.35" width="0.127" layer="21"/>
<wire x1="12" y1="-6.25" x2="12" y2="1.35" width="0.127" layer="21"/>
<wire x1="11.4" y1="-1" x2="-1.4" y2="-1" width="0.127" layer="21"/>
<wire x1="11.4" y1="-1" x2="11.4" y2="1.35" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-1" x2="-1.4" y2="1.35" width="0.127" layer="21"/>
<wire x1="0" y1="-1.5" x2="-0.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-2.5" x2="0.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="0.5" y1="-2.5" x2="0" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1" y1="-1.5" x2="-1" y2="-4" width="0.127" layer="21"/>
<wire x1="-1" y1="-4" x2="-1.5" y2="-4" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-4" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="11" y1="-1.5" x2="11.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="11.5" y1="-1.5" x2="11.5" y2="-4" width="0.127" layer="21"/>
<wire x1="11.5" y1="-4" x2="11" y2="-4" width="0.127" layer="21"/>
<wire x1="11" y1="-4" x2="11" y2="-1.5" width="0.127" layer="21"/>
<pad name="2" x="2" y="0" drill="0.8" rot="R180"/>
<pad name="1" x="0" y="0" drill="0.8" shape="square" rot="R180"/>
<pad name="3" x="4" y="0" drill="0.8" rot="R180"/>
<pad name="4" x="6" y="0" drill="0.8" rot="R180"/>
<pad name="5" x="8" y="0" drill="0.8" rot="R180"/>
<pad name="6" x="10" y="0" drill="0.8" rot="R180"/>
<text x="4" y="-6.5001" size="1.27" layer="25" rot="R180">&gt;NAME</text>
</package>
<package name="B3B-PH-K-S" urn="urn:adsk.eagle:footprint:7974978/2" library_version="7">
<wire x1="6" y1="1.7" x2="3.4" y2="1.7" width="0.127" layer="21"/>
<wire x1="3.4" y1="1.7" x2="0.6" y2="1.7" width="0.127" layer="21"/>
<wire x1="0.6" y1="1.7" x2="-2" y2="1.7" width="0.127" layer="21"/>
<wire x1="6" y1="-3" x2="-2" y2="-3" width="0.127" layer="21"/>
<wire x1="-2" y1="-3" x2="-2" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.6" x2="-2" y2="0.3" width="0.127" layer="21"/>
<wire x1="-2" y1="0.3" x2="-2" y2="1.7" width="0.127" layer="21"/>
<wire x1="6" y1="-3" x2="6" y2="-0.6" width="0.127" layer="21"/>
<wire x1="6" y1="-0.6" x2="6" y2="0.3" width="0.127" layer="21"/>
<wire x1="6" y1="0.3" x2="6" y2="1.7" width="0.127" layer="21"/>
<wire x1="5.4" y1="-2.4" x2="-1.4" y2="-2.4" width="0.127" layer="21"/>
<wire x1="5.4" y1="-2.4" x2="5.4" y2="-0.6" width="0.127" layer="21"/>
<wire x1="5.4" y1="-0.6" x2="5.4" y2="0.3" width="0.127" layer="21"/>
<wire x1="5.4" y1="0.3" x2="5.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="5.4" y1="1.1" x2="3.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-2.4" x2="-1.4" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-0.6" x2="-1.4" y2="0.3" width="0.127" layer="21"/>
<wire x1="-1.4" y1="0.3" x2="-1.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="6" y1="0.3" x2="5.4" y2="0.3" width="0.127" layer="21"/>
<wire x1="6" y1="-0.6" x2="5.4" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-0.6" x2="-2" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-1.4" y1="0.3" x2="-2" y2="0.3" width="0.127" layer="21"/>
<wire x1="0.6" y1="1.1" x2="-1.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="0.6" y1="1.1" x2="0.6" y2="1.7" width="0.127" layer="21"/>
<wire x1="3.4" y1="1.1" x2="3.4" y2="1.7" width="0.127" layer="21"/>
<pad name="2" x="2" y="0" drill="0.8" rot="R180"/>
<pad name="1" x="0" y="0" drill="0.8" shape="square" rot="R180"/>
<pad name="3" x="4" y="0" drill="0.8" rot="R180"/>
<text x="4" y="-3.5001" size="1.27" layer="25" rot="R180">&gt;NAME</text>
<text x="0.0999" y="3.4999" size="1.27" layer="21" rot="R180">1</text>
</package>
<package name="S3B-PH-K-S" urn="urn:adsk.eagle:footprint:7974970/3" library_version="7">
<wire x1="6" y1="1.35" x2="5.4" y2="1.35" width="0.127" layer="21"/>
<wire x1="-1.4" y1="1.35" x2="-2" y2="1.35" width="0.127" layer="21"/>
<wire x1="6" y1="-6.25" x2="-2" y2="-6.25" width="0.127" layer="21"/>
<wire x1="-2" y1="-6.25" x2="-2" y2="1.35" width="0.127" layer="21"/>
<wire x1="6" y1="-6.25" x2="6" y2="1.35" width="0.127" layer="21"/>
<wire x1="5.4" y1="-1" x2="-1.4" y2="-1" width="0.127" layer="21"/>
<wire x1="5.4" y1="-1" x2="5.4" y2="1.35" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-1" x2="-1.4" y2="1.35" width="0.127" layer="21"/>
<wire x1="0" y1="-1.5" x2="-0.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-2.5" x2="0.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="0.5" y1="-2.5" x2="0" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1" y1="-1.5" x2="-1" y2="-4" width="0.127" layer="21"/>
<wire x1="-1" y1="-4" x2="-1.5" y2="-4" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-4" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="5" y1="-1.5" x2="5.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="5.5" y1="-1.5" x2="5.5" y2="-4" width="0.127" layer="21"/>
<wire x1="5.5" y1="-4" x2="5" y2="-4" width="0.127" layer="21"/>
<wire x1="5" y1="-4" x2="5" y2="-1.5" width="0.127" layer="21"/>
<pad name="2" x="2" y="0" drill="0.8" rot="R180"/>
<pad name="1" x="0" y="0" drill="0.8" shape="square" rot="R180"/>
<pad name="3" x="4" y="0" drill="0.8" rot="R180"/>
<text x="4" y="-6.5001" size="1.27" layer="25" rot="R180">&gt;NAME</text>
</package>
<package name="B4B-PH-K-S" urn="urn:adsk.eagle:footprint:7974977/2" library_version="7">
<wire x1="8" y1="1.7" x2="5.4" y2="1.7" width="0.127" layer="21"/>
<wire x1="5.4" y1="1.7" x2="0.6" y2="1.7" width="0.127" layer="21"/>
<wire x1="0.6" y1="1.7" x2="-2" y2="1.7" width="0.127" layer="21"/>
<wire x1="8" y1="-3" x2="-2" y2="-3" width="0.127" layer="21"/>
<wire x1="-2" y1="-3" x2="-2" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.6" x2="-2" y2="0.3" width="0.127" layer="21"/>
<wire x1="-2" y1="0.3" x2="-2" y2="1.7" width="0.127" layer="21"/>
<wire x1="8" y1="-3" x2="8" y2="-0.6" width="0.127" layer="21"/>
<wire x1="8" y1="-0.6" x2="8" y2="0.3" width="0.127" layer="21"/>
<wire x1="8" y1="0.3" x2="8" y2="1.7" width="0.127" layer="21"/>
<wire x1="7.4" y1="-2.4" x2="-1.4" y2="-2.4" width="0.127" layer="21"/>
<wire x1="7.4" y1="-2.4" x2="7.4" y2="-0.6" width="0.127" layer="21"/>
<wire x1="7.4" y1="-0.6" x2="7.4" y2="0.3" width="0.127" layer="21"/>
<wire x1="7.4" y1="0.3" x2="7.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="7.4" y1="1.1" x2="5.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-2.4" x2="-1.4" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-0.6" x2="-1.4" y2="0.3" width="0.127" layer="21"/>
<wire x1="-1.4" y1="0.3" x2="-1.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="8" y1="0.3" x2="7.4" y2="0.3" width="0.127" layer="21"/>
<wire x1="8" y1="-0.6" x2="7.4" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-0.6" x2="-2" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-1.4" y1="0.3" x2="-2" y2="0.3" width="0.127" layer="21"/>
<wire x1="0.6" y1="1.1" x2="-1.4" y2="1.1" width="0.127" layer="21"/>
<wire x1="0.6" y1="1.1" x2="0.6" y2="1.7" width="0.127" layer="21"/>
<wire x1="5.4" y1="1.1" x2="5.4" y2="1.7" width="0.127" layer="21"/>
<pad name="2" x="2" y="0" drill="0.8" rot="R180"/>
<pad name="1" x="0" y="0" drill="0.8" shape="square" rot="R180"/>
<pad name="3" x="4" y="0" drill="0.8" rot="R180"/>
<pad name="4" x="6" y="0" drill="0.8" rot="R180"/>
<text x="4" y="-3.5001" size="1.27" layer="25" rot="R180">&gt;NAME</text>
<text x="0.0999" y="3.4999" size="1.27" layer="21" rot="R180">1</text>
</package>
<package name="S4B-PH-K-S" urn="urn:adsk.eagle:footprint:7974969/3" library_version="7">
<wire x1="8" y1="1.35" x2="7.4" y2="1.35" width="0.127" layer="21"/>
<wire x1="-1.4" y1="1.35" x2="-2" y2="1.35" width="0.127" layer="21"/>
<wire x1="8" y1="-6.25" x2="-2" y2="-6.25" width="0.127" layer="21"/>
<wire x1="-2" y1="-6.25" x2="-2" y2="1.35" width="0.127" layer="21"/>
<wire x1="8" y1="-6.25" x2="8" y2="1.35" width="0.127" layer="21"/>
<wire x1="7.4" y1="-1" x2="-1.4" y2="-1" width="0.127" layer="21"/>
<wire x1="7.4" y1="-1" x2="7.4" y2="1.35" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-1" x2="-1.4" y2="1.35" width="0.127" layer="21"/>
<wire x1="0" y1="-1.5" x2="-0.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-2.5" x2="0.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="0.5" y1="-2.5" x2="0" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1" y1="-1.5" x2="-1" y2="-4" width="0.127" layer="21"/>
<wire x1="-1" y1="-4" x2="-1.5" y2="-4" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-4" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="7" y1="-1.5" x2="7.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="7.5" y1="-1.5" x2="7.5" y2="-4" width="0.127" layer="21"/>
<wire x1="7.5" y1="-4" x2="7" y2="-4" width="0.127" layer="21"/>
<wire x1="7" y1="-4" x2="7" y2="-1.5" width="0.127" layer="21"/>
<pad name="2" x="2" y="0" drill="0.8" rot="R180"/>
<pad name="1" x="0" y="0" drill="0.8" shape="square" rot="R180"/>
<pad name="3" x="4" y="0" drill="0.8" rot="R180"/>
<pad name="4" x="6" y="0" drill="0.8" rot="R180"/>
<text x="4" y="-6.5001" size="1.27" layer="25" rot="R180">&gt;NAME</text>
</package>
</packages>
<packages3d>
<package3d name="B6B-PH-K-S" urn="urn:adsk.eagle:package:7974999/3" type="model" library_version="7">
<packageinstances>
<packageinstance name="B6B-PH-K-S"/>
</packageinstances>
</package3d>
<package3d name="S6B-PH-K-S" urn="urn:adsk.eagle:package:7974991/4" type="model" library_version="7">
<packageinstances>
<packageinstance name="S6B-PH-K-S"/>
</packageinstances>
</package3d>
<package3d name="B3B-PH-K-S" urn="urn:adsk.eagle:package:7975002/3" type="model" library_version="7">
<packageinstances>
<packageinstance name="B3B-PH-K-S"/>
</packageinstances>
</package3d>
<package3d name="S3B-PH-K-S" urn="urn:adsk.eagle:package:7974994/4" type="model" library_version="7">
<packageinstances>
<packageinstance name="S3B-PH-K-S"/>
</packageinstances>
</package3d>
<package3d name="B4B-PH-K-S" urn="urn:adsk.eagle:package:7975001/3" type="model" library_version="7">
<packageinstances>
<packageinstance name="B4B-PH-K-S"/>
</packageinstances>
</package3d>
<package3d name="S4B-PH-K-S" urn="urn:adsk.eagle:package:7974993/4" type="model" library_version="7">
<packageinstances>
<packageinstance name="S4B-PH-K-S"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="CONNECTOR-6P" urn="urn:adsk.eagle:symbol:7974983/1" library_version="7">
<description>Connector 3P</description>
<pin name="1" x="0" y="0" visible="pin" length="middle"/>
<pin name="2" x="0" y="-2.54" visible="pin" length="middle"/>
<pin name="3" x="0" y="-5.08" visible="pin" length="middle"/>
<pin name="4" x="0" y="-7.62" visible="pin" length="middle"/>
<pin name="5" x="0" y="-10.16" visible="pin" length="middle"/>
<pin name="6" x="0" y="-12.7" visible="pin" length="middle"/>
<text x="5.08" y="3.302" size="1.27" layer="95">&gt;NAME</text>
<text x="5.08" y="-17.018" size="1.27" layer="96">&gt;VALUE</text>
<wire x1="2.54" y1="2.54" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="2.54" x2="10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="2.54" y2="-15.24" width="0.254" layer="94"/>
<wire x1="2.54" y1="-15.24" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<circle x="5.08" y="0" radius="0.915809375" width="0.254" layer="94"/>
<circle x="5.08" y="-2.54" radius="0.915809375" width="0.254" layer="94"/>
<circle x="5.08" y="-5.08" radius="0.915809375" width="0.254" layer="94"/>
<circle x="5.08" y="-7.62" radius="0.915809375" width="0.254" layer="94"/>
<circle x="5.08" y="-10.16" radius="0.915809375" width="0.254" layer="94"/>
<circle x="5.08" y="-12.7" radius="0.915809375" width="0.254" layer="94"/>
</symbol>
<symbol name="CONNECTOR-3P" urn="urn:adsk.eagle:symbol:7974987/1" library_version="7">
<description>Connector 3P</description>
<pin name="1" x="0" y="0" visible="pin" length="middle"/>
<pin name="2" x="0" y="-2.54" visible="pin" length="middle"/>
<pin name="3" x="0" y="-5.08" visible="pin" length="middle"/>
<text x="5.08" y="3.302" size="1.27" layer="95">&gt;NAME</text>
<text x="5.08" y="-9.398" size="1.27" layer="96">&gt;VALUE</text>
<wire x1="2.54" y1="2.54" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="2.54" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<circle x="5.08" y="0" radius="0.915809375" width="0.254" layer="94"/>
<circle x="5.08" y="-2.54" radius="0.915809375" width="0.254" layer="94"/>
<circle x="5.08" y="-5.08" radius="0.915809375" width="0.254" layer="94"/>
</symbol>
<symbol name="CONNECTOR-4P" urn="urn:adsk.eagle:symbol:7974985/1" library_version="7">
<description>Connector 3P</description>
<pin name="1" x="0" y="0" visible="pin" length="middle"/>
<pin name="2" x="0" y="-2.54" visible="pin" length="middle"/>
<pin name="3" x="0" y="-5.08" visible="pin" length="middle"/>
<pin name="4" x="0" y="-7.62" visible="pin" length="middle"/>
<text x="5.08" y="3.302" size="1.27" layer="95">&gt;NAME</text>
<text x="5.08" y="-11.938" size="1.27" layer="96">&gt;VALUE</text>
<wire x1="2.54" y1="2.54" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="2.54" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<circle x="5.08" y="0" radius="0.915809375" width="0.254" layer="94"/>
<circle x="5.08" y="-2.54" radius="0.915809375" width="0.254" layer="94"/>
<circle x="5.08" y="-5.08" radius="0.915809375" width="0.254" layer="94"/>
<circle x="5.08" y="-7.62" radius="0.915809375" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PH-6P" urn="urn:adsk.eagle:component:7975007/5" prefix="CN" library_version="7">
<gates>
<gate name="G$1" symbol="CONNECTOR-6P" x="0" y="0"/>
</gates>
<devices>
<device name="B6B" package="B6B-PH-K-S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7974999/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="S6B" package="S6B-PH-K-S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7974991/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PH-3P" urn="urn:adsk.eagle:component:7975010/5" prefix="CN" library_version="7">
<gates>
<gate name="G$1" symbol="CONNECTOR-3P" x="0" y="0"/>
</gates>
<devices>
<device name="B3B" package="B3B-PH-K-S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7975002/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="S3B" package="S3B-PH-K-S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7974994/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PH-4P" urn="urn:adsk.eagle:component:7975009/5" prefix="CN" library_version="7">
<gates>
<gate name="G$1" symbol="CONNECTOR-4P" x="0" y="0"/>
</gates>
<devices>
<device name="B4B" package="B4B-PH-K-S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7975001/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="S4B" package="S4B-PH-K-S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7974993/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinsockets_2" urn="urn:adsk.eagle:library:8479384">
<packages>
<package name="2X20" urn="urn:adsk.eagle:footprint:7523861/3" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2.54</description>
<wire x1="-25.4" y1="-1.905" x2="-24.765" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-24.765" y1="-2.54" x2="-23.495" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-23.495" y1="-2.54" x2="-22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="-1.905" x2="-22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-22.225" y1="-2.54" x2="-20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-20.955" y1="-2.54" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="-2.54" x2="-18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="-2.54" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="-2.54" x2="-15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="-2.54" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="-2.54" x2="-13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-2.54" x2="-10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="-1.905" x2="-25.4" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="1.905" x2="-24.765" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-24.765" y1="2.54" x2="-23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-23.495" y1="2.54" x2="-22.86" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="1.905" x2="-22.225" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-22.225" y1="2.54" x2="-20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-20.955" y1="2.54" x2="-20.32" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="1.905" x2="-19.685" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="2.54" x2="-18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="2.54" x2="-17.78" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.905" x2="-17.145" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="2.54" x2="-15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="2.54" x2="-15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-14.605" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="2.54" x2="-12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="2.54" x2="-10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="2.54" x2="-10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.54" x2="-8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="2.54" x2="-7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="17.78" y2="1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="1.905" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="20.32" y2="1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="1.905" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="22.86" y2="1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="-2.54" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="-2.54" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="-2.54" x2="15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-2.54" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.54" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="1.905" x2="-22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="1.905" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.905" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="1.905" x2="17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="1.905" x2="20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="1.905" x2="22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="1.905" x2="23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="2.54" x2="23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="2.54" x2="25.4" y2="1.905" width="0.1524" layer="21"/>
<wire x1="25.4" y1="-1.905" x2="24.765" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="-2.54" x2="23.495" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="23.495" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="25.4" y1="1.905" x2="25.4" y2="-1.905" width="0.1524" layer="21"/>
<pad name="1" x="-24.13" y="-1.27" drill="1.016" shape="square"/>
<pad name="2" x="-24.13" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-21.59" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-21.59" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="-19.05" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="-19.05" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="-16.51" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="-16.51" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="-13.97" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="-13.97" y="1.27" drill="1.016" shape="octagon"/>
<pad name="11" x="-11.43" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="12" x="-11.43" y="1.27" drill="1.016" shape="octagon"/>
<pad name="13" x="-8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="14" x="-8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="15" x="-6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="16" x="-6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="17" x="-3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="18" x="-3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="19" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="20" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="21" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="22" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="23" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="24" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="25" x="6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="26" x="6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="27" x="8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="28" x="8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="29" x="11.43" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="30" x="11.43" y="1.27" drill="1.016" shape="octagon"/>
<pad name="31" x="13.97" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="32" x="13.97" y="1.27" drill="1.016" shape="octagon"/>
<pad name="33" x="16.51" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="34" x="16.51" y="1.27" drill="1.016" shape="octagon"/>
<pad name="35" x="19.05" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="36" x="19.05" y="1.27" drill="1.016" shape="octagon"/>
<pad name="37" x="21.59" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="38" x="21.59" y="1.27" drill="1.016" shape="octagon"/>
<pad name="39" x="24.13" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="40" x="24.13" y="1.27" drill="1.016" shape="octagon"/>
<text x="-25.4" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-25.4" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-24.384" y1="-1.524" x2="-23.876" y2="-1.016" layer="51"/>
<rectangle x1="-24.384" y1="1.016" x2="-23.876" y2="1.524" layer="51"/>
<rectangle x1="-21.844" y1="1.016" x2="-21.336" y2="1.524" layer="51"/>
<rectangle x1="-21.844" y1="-1.524" x2="-21.336" y2="-1.016" layer="51"/>
<rectangle x1="-19.304" y1="1.016" x2="-18.796" y2="1.524" layer="51"/>
<rectangle x1="-19.304" y1="-1.524" x2="-18.796" y2="-1.016" layer="51"/>
<rectangle x1="-16.764" y1="1.016" x2="-16.256" y2="1.524" layer="51"/>
<rectangle x1="-14.224" y1="1.016" x2="-13.716" y2="1.524" layer="51"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="51"/>
<rectangle x1="-16.764" y1="-1.524" x2="-16.256" y2="-1.016" layer="51"/>
<rectangle x1="-14.224" y1="-1.524" x2="-13.716" y2="-1.016" layer="51"/>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="51"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="51"/>
<rectangle x1="13.716" y1="1.016" x2="14.224" y2="1.524" layer="51"/>
<rectangle x1="13.716" y1="-1.524" x2="14.224" y2="-1.016" layer="51"/>
<rectangle x1="16.256" y1="1.016" x2="16.764" y2="1.524" layer="51"/>
<rectangle x1="16.256" y1="-1.524" x2="16.764" y2="-1.016" layer="51"/>
<rectangle x1="18.796" y1="1.016" x2="19.304" y2="1.524" layer="51"/>
<rectangle x1="18.796" y1="-1.524" x2="19.304" y2="-1.016" layer="51"/>
<rectangle x1="21.336" y1="1.016" x2="21.844" y2="1.524" layer="51"/>
<rectangle x1="21.336" y1="-1.524" x2="21.844" y2="-1.016" layer="51"/>
<rectangle x1="23.876" y1="1.016" x2="24.384" y2="1.524" layer="51"/>
<rectangle x1="23.876" y1="-1.524" x2="24.384" y2="-1.016" layer="51"/>
</package>
<package name="2X20/90" urn="urn:adsk.eagle:footprint:7523922/2" library_version="2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2.54 90°</description>
<wire x1="-1.27" y1="4.445" x2="1.27" y2="4.445" width="0.1524" layer="21"/>
<wire x1="1.27" y1="4.445" x2="1.27" y2="6.985" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="-1.27" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0" y1="13.335" x2="0" y2="7.62" width="0.762" layer="21"/>
<wire x1="1.27" y1="4.445" x2="3.81" y2="4.445" width="0.1524" layer="21"/>
<wire x1="3.81" y1="4.445" x2="3.81" y2="6.985" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="1.27" y2="6.985" width="0.1524" layer="21"/>
<wire x1="2.54" y1="13.335" x2="2.54" y2="7.62" width="0.762" layer="21"/>
<wire x1="3.81" y1="4.445" x2="6.35" y2="4.445" width="0.1524" layer="21"/>
<wire x1="6.35" y1="4.445" x2="6.35" y2="6.985" width="0.1524" layer="21"/>
<wire x1="6.35" y1="6.985" x2="3.81" y2="6.985" width="0.1524" layer="21"/>
<wire x1="5.08" y1="13.335" x2="5.08" y2="7.62" width="0.762" layer="21"/>
<wire x1="6.35" y1="4.445" x2="8.89" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.89" y1="4.445" x2="8.89" y2="6.985" width="0.1524" layer="21"/>
<wire x1="8.89" y1="6.985" x2="6.35" y2="6.985" width="0.1524" layer="21"/>
<wire x1="7.62" y1="13.335" x2="7.62" y2="7.62" width="0.762" layer="21"/>
<wire x1="8.89" y1="4.445" x2="11.43" y2="4.445" width="0.1524" layer="21"/>
<wire x1="11.43" y1="4.445" x2="11.43" y2="6.985" width="0.1524" layer="21"/>
<wire x1="11.43" y1="6.985" x2="8.89" y2="6.985" width="0.1524" layer="21"/>
<wire x1="10.16" y1="13.335" x2="10.16" y2="7.62" width="0.762" layer="21"/>
<wire x1="11.43" y1="4.445" x2="13.97" y2="4.445" width="0.1524" layer="21"/>
<wire x1="13.97" y1="4.445" x2="13.97" y2="6.985" width="0.1524" layer="21"/>
<wire x1="13.97" y1="6.985" x2="11.43" y2="6.985" width="0.1524" layer="21"/>
<wire x1="12.7" y1="13.335" x2="12.7" y2="7.62" width="0.762" layer="21"/>
<wire x1="13.97" y1="4.445" x2="16.51" y2="4.445" width="0.1524" layer="21"/>
<wire x1="16.51" y1="4.445" x2="16.51" y2="6.985" width="0.1524" layer="21"/>
<wire x1="16.51" y1="6.985" x2="13.97" y2="6.985" width="0.1524" layer="21"/>
<wire x1="15.24" y1="13.335" x2="15.24" y2="7.62" width="0.762" layer="21"/>
<wire x1="16.51" y1="4.445" x2="19.05" y2="4.445" width="0.1524" layer="21"/>
<wire x1="19.05" y1="4.445" x2="19.05" y2="6.985" width="0.1524" layer="21"/>
<wire x1="19.05" y1="6.985" x2="16.51" y2="6.985" width="0.1524" layer="21"/>
<wire x1="17.78" y1="13.335" x2="17.78" y2="7.62" width="0.762" layer="21"/>
<wire x1="19.05" y1="4.445" x2="21.59" y2="4.445" width="0.1524" layer="21"/>
<wire x1="21.59" y1="4.445" x2="21.59" y2="6.985" width="0.1524" layer="21"/>
<wire x1="21.59" y1="6.985" x2="19.05" y2="6.985" width="0.1524" layer="21"/>
<wire x1="20.32" y1="13.335" x2="20.32" y2="7.62" width="0.762" layer="21"/>
<wire x1="21.59" y1="4.445" x2="24.13" y2="4.445" width="0.1524" layer="21"/>
<wire x1="24.13" y1="4.445" x2="24.13" y2="6.985" width="0.1524" layer="21"/>
<wire x1="24.13" y1="6.985" x2="21.59" y2="6.985" width="0.1524" layer="21"/>
<wire x1="22.86" y1="13.335" x2="22.86" y2="7.62" width="0.762" layer="21"/>
<wire x1="24.13" y1="4.445" x2="26.67" y2="4.445" width="0.1524" layer="21"/>
<wire x1="26.67" y1="4.445" x2="26.67" y2="6.985" width="0.1524" layer="21"/>
<wire x1="26.67" y1="6.985" x2="24.13" y2="6.985" width="0.1524" layer="21"/>
<wire x1="25.4" y1="13.335" x2="25.4" y2="7.62" width="0.762" layer="21"/>
<wire x1="26.67" y1="4.445" x2="29.21" y2="4.445" width="0.1524" layer="21"/>
<wire x1="29.21" y1="4.445" x2="29.21" y2="6.985" width="0.1524" layer="21"/>
<wire x1="29.21" y1="6.985" x2="26.67" y2="6.985" width="0.1524" layer="21"/>
<wire x1="27.94" y1="13.335" x2="27.94" y2="7.62" width="0.762" layer="21"/>
<wire x1="29.21" y1="4.445" x2="31.75" y2="4.445" width="0.1524" layer="21"/>
<wire x1="31.75" y1="4.445" x2="31.75" y2="6.985" width="0.1524" layer="21"/>
<wire x1="31.75" y1="6.985" x2="29.21" y2="6.985" width="0.1524" layer="21"/>
<wire x1="30.48" y1="13.335" x2="30.48" y2="7.62" width="0.762" layer="21"/>
<wire x1="31.75" y1="4.445" x2="34.29" y2="4.445" width="0.1524" layer="21"/>
<wire x1="34.29" y1="4.445" x2="34.29" y2="6.985" width="0.1524" layer="21"/>
<wire x1="34.29" y1="6.985" x2="31.75" y2="6.985" width="0.1524" layer="21"/>
<wire x1="33.02" y1="13.335" x2="33.02" y2="7.62" width="0.762" layer="21"/>
<wire x1="34.29" y1="4.445" x2="36.83" y2="4.445" width="0.1524" layer="21"/>
<wire x1="36.83" y1="4.445" x2="36.83" y2="6.985" width="0.1524" layer="21"/>
<wire x1="36.83" y1="6.985" x2="34.29" y2="6.985" width="0.1524" layer="21"/>
<wire x1="35.56" y1="13.335" x2="35.56" y2="7.62" width="0.762" layer="21"/>
<wire x1="36.83" y1="4.445" x2="39.37" y2="4.445" width="0.1524" layer="21"/>
<wire x1="39.37" y1="4.445" x2="39.37" y2="6.985" width="0.1524" layer="21"/>
<wire x1="39.37" y1="6.985" x2="36.83" y2="6.985" width="0.1524" layer="21"/>
<wire x1="38.1" y1="13.335" x2="38.1" y2="7.62" width="0.762" layer="21"/>
<wire x1="39.37" y1="4.445" x2="41.91" y2="4.445" width="0.1524" layer="21"/>
<wire x1="41.91" y1="4.445" x2="41.91" y2="6.985" width="0.1524" layer="21"/>
<wire x1="41.91" y1="6.985" x2="39.37" y2="6.985" width="0.1524" layer="21"/>
<wire x1="40.64" y1="13.335" x2="40.64" y2="7.62" width="0.762" layer="21"/>
<wire x1="41.91" y1="4.445" x2="44.45" y2="4.445" width="0.1524" layer="21"/>
<wire x1="44.45" y1="4.445" x2="44.45" y2="6.985" width="0.1524" layer="21"/>
<wire x1="44.45" y1="6.985" x2="41.91" y2="6.985" width="0.1524" layer="21"/>
<wire x1="43.18" y1="13.335" x2="43.18" y2="7.62" width="0.762" layer="21"/>
<wire x1="44.45" y1="4.445" x2="46.99" y2="4.445" width="0.1524" layer="21"/>
<wire x1="46.99" y1="4.445" x2="46.99" y2="6.985" width="0.1524" layer="21"/>
<wire x1="46.99" y1="6.985" x2="44.45" y2="6.985" width="0.1524" layer="21"/>
<wire x1="45.72" y1="13.335" x2="45.72" y2="7.62" width="0.762" layer="21"/>
<wire x1="46.99" y1="4.445" x2="49.53" y2="4.445" width="0.1524" layer="21"/>
<wire x1="49.53" y1="4.445" x2="49.53" y2="6.985" width="0.1524" layer="21"/>
<wire x1="49.53" y1="6.985" x2="46.99" y2="6.985" width="0.1524" layer="21"/>
<wire x1="48.26" y1="13.335" x2="48.26" y2="7.62" width="0.762" layer="21"/>
<pad name="2" x="0" y="2.54" drill="1.016" shape="octagon"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" shape="octagon"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" shape="octagon"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" shape="octagon"/>
<pad name="10" x="10.16" y="2.54" drill="1.016" shape="octagon"/>
<pad name="12" x="12.7" y="2.54" drill="1.016" shape="octagon"/>
<pad name="14" x="15.24" y="2.54" drill="1.016" shape="octagon"/>
<pad name="16" x="17.78" y="2.54" drill="1.016" shape="octagon"/>
<pad name="18" x="20.32" y="2.54" drill="1.016" shape="octagon"/>
<pad name="20" x="22.86" y="2.54" drill="1.016" shape="octagon"/>
<pad name="22" x="25.4" y="2.54" drill="1.016" shape="octagon"/>
<pad name="24" x="27.94" y="2.54" drill="1.016" shape="octagon"/>
<pad name="26" x="30.48" y="2.54" drill="1.016" shape="octagon"/>
<pad name="28" x="33.02" y="2.54" drill="1.016" shape="octagon"/>
<pad name="30" x="35.56" y="2.54" drill="1.016" shape="octagon"/>
<pad name="32" x="38.1" y="2.54" drill="1.016" shape="octagon"/>
<pad name="34" x="40.64" y="2.54" drill="1.016" shape="octagon"/>
<pad name="36" x="43.18" y="2.54" drill="1.016" shape="octagon"/>
<pad name="38" x="45.72" y="2.54" drill="1.016" shape="octagon"/>
<pad name="1" x="0" y="0" drill="1.016" shape="square"/>
<pad name="3" x="2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="5" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="7" x="7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="9" x="10.16" y="0" drill="1.016" shape="octagon"/>
<pad name="11" x="12.7" y="0" drill="1.016" shape="octagon"/>
<pad name="13" x="15.24" y="0" drill="1.016" shape="octagon"/>
<pad name="15" x="17.78" y="0" drill="1.016" shape="octagon"/>
<pad name="17" x="20.32" y="0" drill="1.016" shape="octagon"/>
<pad name="19" x="22.86" y="0" drill="1.016" shape="octagon"/>
<pad name="21" x="25.4" y="0" drill="1.016" shape="octagon"/>
<pad name="23" x="27.94" y="0" drill="1.016" shape="octagon"/>
<pad name="25" x="30.48" y="0" drill="1.016" shape="octagon"/>
<pad name="27" x="33.02" y="0" drill="1.016" shape="octagon"/>
<pad name="29" x="35.56" y="0" drill="1.016" shape="octagon"/>
<pad name="31" x="38.1" y="0" drill="1.016" shape="octagon"/>
<pad name="33" x="40.64" y="0" drill="1.016" shape="octagon"/>
<pad name="35" x="43.18" y="0" drill="1.016" shape="octagon"/>
<pad name="37" x="45.72" y="0" drill="1.016" shape="octagon"/>
<pad name="40" x="48.26" y="2.54" drill="1.016" shape="octagon"/>
<pad name="39" x="48.26" y="0" drill="1.016" shape="octagon"/>
<text x="-1.905" y="2.54" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="51.435" y="2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.381" y1="6.985" x2="0.381" y2="7.493" layer="21"/>
<rectangle x1="2.159" y1="6.985" x2="2.921" y2="7.493" layer="21"/>
<rectangle x1="4.699" y1="6.985" x2="5.461" y2="7.493" layer="21"/>
<rectangle x1="7.239" y1="6.985" x2="8.001" y2="7.493" layer="21"/>
<rectangle x1="9.779" y1="6.985" x2="10.541" y2="7.493" layer="21"/>
<rectangle x1="12.319" y1="6.985" x2="13.081" y2="7.493" layer="21"/>
<rectangle x1="14.859" y1="6.985" x2="15.621" y2="7.493" layer="21"/>
<rectangle x1="17.399" y1="6.985" x2="18.161" y2="7.493" layer="21"/>
<rectangle x1="19.939" y1="6.985" x2="20.701" y2="7.493" layer="21"/>
<rectangle x1="22.479" y1="6.985" x2="23.241" y2="7.493" layer="21"/>
<rectangle x1="25.019" y1="6.985" x2="25.781" y2="7.493" layer="21"/>
<rectangle x1="27.559" y1="6.985" x2="28.321" y2="7.493" layer="21"/>
<rectangle x1="30.099" y1="6.985" x2="30.861" y2="7.493" layer="21"/>
<rectangle x1="32.639" y1="6.985" x2="33.401" y2="7.493" layer="21"/>
<rectangle x1="35.179" y1="6.985" x2="35.941" y2="7.493" layer="21"/>
<rectangle x1="37.719" y1="6.985" x2="38.481" y2="7.493" layer="21"/>
<rectangle x1="40.259" y1="6.985" x2="41.021" y2="7.493" layer="21"/>
<rectangle x1="42.799" y1="6.985" x2="43.561" y2="7.493" layer="21"/>
<rectangle x1="45.339" y1="6.985" x2="46.101" y2="7.493" layer="21"/>
<rectangle x1="47.879" y1="6.985" x2="48.641" y2="7.493" layer="21"/>
<rectangle x1="-0.381" y1="3.429" x2="0.381" y2="4.445" layer="21"/>
<rectangle x1="2.159" y1="3.429" x2="2.921" y2="4.445" layer="21"/>
<rectangle x1="-0.381" y1="0.889" x2="0.381" y2="1.651" layer="21"/>
<rectangle x1="-0.381" y1="1.651" x2="0.381" y2="3.429" layer="51"/>
<rectangle x1="2.159" y1="1.651" x2="2.921" y2="3.429" layer="51"/>
<rectangle x1="2.159" y1="0.889" x2="2.921" y2="1.651" layer="21"/>
<rectangle x1="4.699" y1="3.429" x2="5.461" y2="4.445" layer="21"/>
<rectangle x1="7.239" y1="3.429" x2="8.001" y2="4.445" layer="21"/>
<rectangle x1="4.699" y1="0.889" x2="5.461" y2="1.651" layer="21"/>
<rectangle x1="4.699" y1="1.651" x2="5.461" y2="3.429" layer="51"/>
<rectangle x1="7.239" y1="1.651" x2="8.001" y2="3.429" layer="51"/>
<rectangle x1="7.239" y1="0.889" x2="8.001" y2="1.651" layer="21"/>
<rectangle x1="9.779" y1="3.429" x2="10.541" y2="4.445" layer="21"/>
<rectangle x1="9.779" y1="0.889" x2="10.541" y2="1.651" layer="21"/>
<rectangle x1="9.779" y1="1.651" x2="10.541" y2="3.429" layer="51"/>
<rectangle x1="12.319" y1="3.429" x2="13.081" y2="4.445" layer="21"/>
<rectangle x1="14.859" y1="3.429" x2="15.621" y2="4.445" layer="21"/>
<rectangle x1="12.319" y1="0.889" x2="13.081" y2="1.651" layer="21"/>
<rectangle x1="12.319" y1="1.651" x2="13.081" y2="3.429" layer="51"/>
<rectangle x1="14.859" y1="1.651" x2="15.621" y2="3.429" layer="51"/>
<rectangle x1="14.859" y1="0.889" x2="15.621" y2="1.651" layer="21"/>
<rectangle x1="17.399" y1="3.429" x2="18.161" y2="4.445" layer="21"/>
<rectangle x1="19.939" y1="3.429" x2="20.701" y2="4.445" layer="21"/>
<rectangle x1="17.399" y1="0.889" x2="18.161" y2="1.651" layer="21"/>
<rectangle x1="17.399" y1="1.651" x2="18.161" y2="3.429" layer="51"/>
<rectangle x1="19.939" y1="1.651" x2="20.701" y2="3.429" layer="51"/>
<rectangle x1="19.939" y1="0.889" x2="20.701" y2="1.651" layer="21"/>
<rectangle x1="22.479" y1="3.429" x2="23.241" y2="4.445" layer="21"/>
<rectangle x1="22.479" y1="0.889" x2="23.241" y2="1.651" layer="21"/>
<rectangle x1="22.479" y1="1.651" x2="23.241" y2="3.429" layer="51"/>
<rectangle x1="25.019" y1="3.429" x2="25.781" y2="4.445" layer="21"/>
<rectangle x1="27.559" y1="3.429" x2="28.321" y2="4.445" layer="21"/>
<rectangle x1="25.019" y1="0.889" x2="25.781" y2="1.651" layer="21"/>
<rectangle x1="25.019" y1="1.651" x2="25.781" y2="3.429" layer="51"/>
<rectangle x1="27.559" y1="1.651" x2="28.321" y2="3.429" layer="51"/>
<rectangle x1="27.559" y1="0.889" x2="28.321" y2="1.651" layer="21"/>
<rectangle x1="30.099" y1="3.429" x2="30.861" y2="4.445" layer="21"/>
<rectangle x1="32.639" y1="3.429" x2="33.401" y2="4.445" layer="21"/>
<rectangle x1="30.099" y1="0.889" x2="30.861" y2="1.651" layer="21"/>
<rectangle x1="30.099" y1="1.651" x2="30.861" y2="3.429" layer="51"/>
<rectangle x1="32.639" y1="1.651" x2="33.401" y2="3.429" layer="51"/>
<rectangle x1="32.639" y1="0.889" x2="33.401" y2="1.651" layer="21"/>
<rectangle x1="35.179" y1="3.429" x2="35.941" y2="4.445" layer="21"/>
<rectangle x1="35.179" y1="0.889" x2="35.941" y2="1.651" layer="21"/>
<rectangle x1="35.179" y1="1.651" x2="35.941" y2="3.429" layer="51"/>
<rectangle x1="37.719" y1="3.429" x2="38.481" y2="4.445" layer="21"/>
<rectangle x1="40.259" y1="3.429" x2="41.021" y2="4.445" layer="21"/>
<rectangle x1="37.719" y1="0.889" x2="38.481" y2="1.651" layer="21"/>
<rectangle x1="37.719" y1="1.651" x2="38.481" y2="3.429" layer="51"/>
<rectangle x1="40.259" y1="1.651" x2="41.021" y2="3.429" layer="51"/>
<rectangle x1="40.259" y1="0.889" x2="41.021" y2="1.651" layer="21"/>
<rectangle x1="42.799" y1="3.429" x2="43.561" y2="4.445" layer="21"/>
<rectangle x1="45.339" y1="3.429" x2="46.101" y2="4.445" layer="21"/>
<rectangle x1="42.799" y1="0.889" x2="43.561" y2="1.651" layer="21"/>
<rectangle x1="42.799" y1="1.651" x2="43.561" y2="3.429" layer="51"/>
<rectangle x1="45.339" y1="1.651" x2="46.101" y2="3.429" layer="51"/>
<rectangle x1="45.339" y1="0.889" x2="46.101" y2="1.651" layer="21"/>
<rectangle x1="47.879" y1="3.429" x2="48.641" y2="4.445" layer="21"/>
<rectangle x1="47.879" y1="0.889" x2="48.641" y2="1.651" layer="21"/>
<rectangle x1="47.879" y1="1.651" x2="48.641" y2="3.429" layer="51"/>
</package>
<package name="2X20M" urn="urn:adsk.eagle:footprint:7524043/2" library_version="2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2</description>
<wire x1="39.25" y1="3.25" x2="39.25" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="39.25" y1="-1.25" x2="-1.25" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-1.25" y1="-1.25" x2="-1.25" y2="3.25" width="0.2032" layer="21"/>
<wire x1="-1.25" y1="3.25" x2="39.25" y2="3.25" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.9144" shape="square"/>
<pad name="2" x="0" y="2" drill="0.9144"/>
<pad name="3" x="2" y="0" drill="0.9144"/>
<pad name="4" x="2" y="2" drill="0.9144"/>
<pad name="5" x="4" y="0" drill="0.9144"/>
<pad name="6" x="4" y="2" drill="0.9144"/>
<pad name="7" x="6" y="0" drill="0.9144"/>
<pad name="8" x="6" y="2" drill="0.9144"/>
<pad name="9" x="8" y="0" drill="0.9144"/>
<pad name="10" x="8" y="2" drill="0.9144"/>
<pad name="11" x="10" y="0" drill="0.9144"/>
<pad name="12" x="10" y="2" drill="0.9144"/>
<pad name="13" x="12" y="0" drill="0.9144"/>
<pad name="14" x="12" y="2" drill="0.9144"/>
<pad name="15" x="14" y="0" drill="0.9144"/>
<pad name="16" x="14" y="2" drill="0.9144"/>
<pad name="17" x="16" y="0" drill="0.9144"/>
<pad name="18" x="16" y="2" drill="0.9144"/>
<pad name="19" x="18" y="0" drill="0.9144"/>
<pad name="20" x="18" y="2" drill="0.9144"/>
<pad name="21" x="20" y="0" drill="0.9144"/>
<pad name="22" x="20" y="2" drill="0.9144"/>
<pad name="23" x="22" y="0" drill="0.9144"/>
<pad name="24" x="22" y="2" drill="0.9144"/>
<pad name="25" x="24" y="0" drill="0.9144"/>
<pad name="26" x="24" y="2" drill="0.9144"/>
<pad name="27" x="26" y="0" drill="0.9144"/>
<pad name="28" x="26" y="2" drill="0.9144"/>
<pad name="29" x="28" y="0" drill="0.9144"/>
<pad name="30" x="28" y="2" drill="0.9144"/>
<pad name="31" x="30" y="0" drill="0.9144"/>
<pad name="32" x="30" y="2" drill="0.9144"/>
<pad name="33" x="32" y="0" drill="0.9144"/>
<pad name="34" x="32" y="2" drill="0.9144"/>
<pad name="35" x="34" y="0" drill="0.9144"/>
<pad name="36" x="34" y="2" drill="0.9144"/>
<pad name="37" x="36" y="0" drill="0.9144"/>
<pad name="38" x="36" y="2" drill="0.9144"/>
<pad name="39" x="38" y="0" drill="0.9144"/>
<pad name="40" x="38" y="2" drill="0.9144"/>
<text x="-2" y="-1" size="1.016" layer="25" ratio="14" rot="R90">&gt;NAME</text>
<text x="41" y="-1" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.25" y1="1.75" x2="0.25" y2="2.25" layer="51"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="51"/>
<rectangle x1="1.75" y1="1.75" x2="2.25" y2="2.25" layer="51"/>
<rectangle x1="1.75" y1="-0.25" x2="2.25" y2="0.25" layer="51"/>
<rectangle x1="3.75" y1="1.75" x2="4.25" y2="2.25" layer="51"/>
<rectangle x1="3.75" y1="-0.25" x2="4.25" y2="0.25" layer="51"/>
<rectangle x1="5.75" y1="1.75" x2="6.25" y2="2.25" layer="51"/>
<rectangle x1="5.75" y1="-0.25" x2="6.25" y2="0.25" layer="51"/>
<rectangle x1="7.75" y1="1.75" x2="8.25" y2="2.25" layer="51"/>
<rectangle x1="7.75" y1="-0.25" x2="8.25" y2="0.25" layer="51"/>
<rectangle x1="9.75" y1="1.75" x2="10.25" y2="2.25" layer="51"/>
<rectangle x1="9.75" y1="-0.25" x2="10.25" y2="0.25" layer="51"/>
<rectangle x1="11.75" y1="1.75" x2="12.25" y2="2.25" layer="51"/>
<rectangle x1="11.75" y1="-0.25" x2="12.25" y2="0.25" layer="51"/>
<rectangle x1="13.75" y1="1.75" x2="14.25" y2="2.25" layer="51"/>
<rectangle x1="13.75" y1="-0.25" x2="14.25" y2="0.25" layer="51"/>
<rectangle x1="15.75" y1="1.75" x2="16.25" y2="2.25" layer="51"/>
<rectangle x1="15.75" y1="-0.25" x2="16.25" y2="0.25" layer="51"/>
<rectangle x1="17.75" y1="1.75" x2="18.25" y2="2.25" layer="51"/>
<rectangle x1="17.75" y1="-0.25" x2="18.25" y2="0.25" layer="51"/>
<rectangle x1="19.75" y1="1.75" x2="20.25" y2="2.25" layer="51"/>
<rectangle x1="19.75" y1="-0.25" x2="20.25" y2="0.25" layer="51"/>
<rectangle x1="21.75" y1="1.75" x2="22.25" y2="2.25" layer="51"/>
<rectangle x1="21.75" y1="-0.25" x2="22.25" y2="0.25" layer="51"/>
<rectangle x1="23.75" y1="1.75" x2="24.25" y2="2.25" layer="51"/>
<rectangle x1="23.75" y1="-0.25" x2="24.25" y2="0.25" layer="51"/>
<rectangle x1="25.75" y1="1.75" x2="26.25" y2="2.25" layer="51"/>
<rectangle x1="25.75" y1="-0.25" x2="26.25" y2="0.25" layer="51"/>
<rectangle x1="27.75" y1="1.75" x2="28.25" y2="2.25" layer="51"/>
<rectangle x1="27.75" y1="-0.25" x2="28.25" y2="0.25" layer="51"/>
<rectangle x1="29.75" y1="1.75" x2="30.25" y2="2.25" layer="51"/>
<rectangle x1="29.75" y1="-0.25" x2="30.25" y2="0.25" layer="51"/>
<rectangle x1="31.75" y1="1.75" x2="32.25" y2="2.25" layer="51"/>
<rectangle x1="31.75" y1="-0.25" x2="32.25" y2="0.25" layer="51"/>
<rectangle x1="33.75" y1="1.75" x2="34.25" y2="2.25" layer="51"/>
<rectangle x1="33.75" y1="-0.25" x2="34.25" y2="0.25" layer="51"/>
<rectangle x1="35.75" y1="1.75" x2="36.25" y2="2.25" layer="51"/>
<rectangle x1="35.75" y1="-0.25" x2="36.25" y2="0.25" layer="51"/>
<rectangle x1="37.75" y1="1.75" x2="38.25" y2="2.25" layer="51"/>
<rectangle x1="37.75" y1="-0.25" x2="38.25" y2="0.25" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="HDRVR40W64P254_2X20_5080X508X850C" urn="urn:adsk.eagle:package:7524224/9" type="model" library_version="4">
<description>Double-row, 40-pin Receptacle Header (Female) Straight, 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 8.50 mm insulator length, 50.80 X 5.08 X 8.50 mm body
&lt;p&gt;Double-row (2X20), 40-pin Receptacle Header (Female) Straight package with 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 8.50 mm insulator length with overall size 50.80 X 5.08 X 8.50 mm, pin pattern - zigzag from bottom left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="2X20"/>
</packageinstances>
</package3d>
<package3d name="HDRRAR40W64P254_2X20_5080X736X556C" urn="urn:adsk.eagle:package:7524285/6" type="model" library_version="2">
<description>Double-row, 40-pin Receptacle Header (Female) Right Angle, 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 7.36 mm insulator length, 50.80 X 7.36 X 5.56 mm body
&lt;p&gt;Double-row (2X20), 40-pin Receptacle Header (Female) Right Angle package with 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 7.36 mm insulator length with body size 50.80 X 7.36 X 5.56 mm, pin pattern - zigzag from bottom left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="2X20/90"/>
</packageinstances>
</package3d>
<package3d name="HDRVR40W45P200_2X20_4000X400X440C" urn="urn:adsk.eagle:package:7524405/6" type="model" library_version="2">
<description>Double-row, 40-pin Receptacle Header (Female) Straight, 2.00 mm (0.08 in) row pitch, 2.00 mm (0.08 in) col pitch, 4.40 mm insulator length, 40.00 X 4.00 X 4.40 mm body
&lt;p&gt;Double-row (2X20), 40-pin Receptacle Header (Female) Straight package with 2.00 mm (0.08 in) row pitch, 2.00 mm (0.08 in) col pitch, 0.45 mm lead width, 2.60 mm tail length and 4.40 mm insulator length with overall size 40.00 X 4.00 X 4.40 mm, pin pattern - zigzag from bottom left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="2X20M"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PINS2X20" urn="urn:adsk.eagle:symbol:8479837/1" library_version="2">
<wire x1="-6.35" y1="-27.94" x2="8.89" y2="-27.94" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-27.94" x2="8.89" y2="25.4" width="0.4064" layer="94"/>
<wire x1="8.89" y1="25.4" x2="-6.35" y2="25.4" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="25.4" x2="-6.35" y2="-27.94" width="0.4064" layer="94"/>
<text x="-6.35" y="26.035" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-30.48" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="22.86" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="22.86" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="20.32" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="17.78" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="13" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="15" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="17" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="18" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="19" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="20" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="21" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="22" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="23" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="24" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="25" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="26" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="27" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="28" x="5.08" y="-10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="29" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="30" x="5.08" y="-12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="31" x="-2.54" y="-15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="32" x="5.08" y="-15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="33" x="-2.54" y="-17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="34" x="5.08" y="-17.78" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="35" x="-2.54" y="-20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="36" x="5.08" y="-20.32" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="37" x="-2.54" y="-22.86" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="38" x="5.08" y="-22.86" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="39" x="-2.54" y="-25.4" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="40" x="5.08" y="-25.4" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINSOCKET-2X20" urn="urn:adsk.eagle:component:8479838/3" prefix="JP" uservalue="yes" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINS2X20" x="0" y="0"/>
</gates>
<devices>
<device name="_2.54" package="2X20">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="25" pad="25"/>
<connect gate="A" pin="26" pad="26"/>
<connect gate="A" pin="27" pad="27"/>
<connect gate="A" pin="28" pad="28"/>
<connect gate="A" pin="29" pad="29"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="30" pad="30"/>
<connect gate="A" pin="31" pad="31"/>
<connect gate="A" pin="32" pad="32"/>
<connect gate="A" pin="33" pad="33"/>
<connect gate="A" pin="34" pad="34"/>
<connect gate="A" pin="35" pad="35"/>
<connect gate="A" pin="36" pad="36"/>
<connect gate="A" pin="37" pad="37"/>
<connect gate="A" pin="38" pad="38"/>
<connect gate="A" pin="39" pad="39"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="40" pad="40"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7524224/9"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2.54-90°" package="2X20/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="25" pad="25"/>
<connect gate="A" pin="26" pad="26"/>
<connect gate="A" pin="27" pad="27"/>
<connect gate="A" pin="28" pad="28"/>
<connect gate="A" pin="29" pad="29"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="30" pad="30"/>
<connect gate="A" pin="31" pad="31"/>
<connect gate="A" pin="32" pad="32"/>
<connect gate="A" pin="33" pad="33"/>
<connect gate="A" pin="34" pad="34"/>
<connect gate="A" pin="35" pad="35"/>
<connect gate="A" pin="36" pad="36"/>
<connect gate="A" pin="37" pad="37"/>
<connect gate="A" pin="38" pad="38"/>
<connect gate="A" pin="39" pad="39"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="40" pad="40"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7524285/6"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2.00" package="2X20M">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="25" pad="25"/>
<connect gate="A" pin="26" pad="26"/>
<connect gate="A" pin="27" pad="27"/>
<connect gate="A" pin="28" pad="28"/>
<connect gate="A" pin="29" pad="29"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="30" pad="30"/>
<connect gate="A" pin="31" pad="31"/>
<connect gate="A" pin="32" pad="32"/>
<connect gate="A" pin="33" pad="33"/>
<connect gate="A" pin="34" pad="34"/>
<connect gate="A" pin="35" pad="35"/>
<connect gate="A" pin="36" pad="36"/>
<connect gate="A" pin="37" pad="37"/>
<connect gate="A" pin="38" pad="38"/>
<connect gate="A" pin="39" pad="39"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="40" pad="40"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7524405/6"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="discrete" urn="urn:adsk.eagle:library:7520964">
<packages>
<package name="R2012" urn="urn:adsk.eagle:footprint:7517697/3" library_version="59">
<description>2012 metric</description>
<smd name="P$1" x="-1" y="0" dx="1.25" dy="0.7" layer="1" rot="R90"/>
<smd name="P$2" x="1" y="0" dx="1.25" dy="0.7" layer="1" rot="R90"/>
<wire x1="-1.5" y1="0.8" x2="-1.5" y2="-0.8" width="0.254" layer="21"/>
<wire x1="-1.5" y1="-0.8" x2="1.5" y2="-0.8" width="0.254" layer="21"/>
<wire x1="1.5" y1="-0.8" x2="1.5" y2="0.8" width="0.254" layer="21"/>
<wire x1="1.5" y1="0.8" x2="-1.5" y2="0.8" width="0.254" layer="21"/>
<text x="-1" y="1" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="R2012-1608" urn="urn:adsk.eagle:footprint:7517688/2" library_version="59">
<description>2012-1608 metric</description>
<smd name="P$1" x="-0.925" y="0" dx="1.25" dy="0.85" layer="1" rot="R90"/>
<smd name="P$2" x="0.925" y="0" dx="1.25" dy="0.85" layer="1" rot="R90"/>
<wire x1="-1.5" y1="0.8" x2="-1.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.8" x2="1.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.8" x2="1.5" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.8" x2="-1.5" y2="0.8" width="0.127" layer="21"/>
<text x="-1" y="1" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="R1608" urn="urn:adsk.eagle:footprint:7517691/2" library_version="59">
<description>1608 metric</description>
<smd name="P$1" x="-0.75" y="0" dx="0.8" dy="0.5" layer="1" rot="R90"/>
<smd name="P$2" x="0.75" y="0" dx="0.8" dy="0.5" layer="1" rot="R90"/>
<wire x1="-1.2" y1="0.6" x2="-1.2" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-1.2" y1="-0.6" x2="1.2" y2="-0.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.6" x2="1.2" y2="0.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.6" x2="-1.2" y2="0.6" width="0.127" layer="21"/>
<text x="-0.8" y="0.7" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="R3216" urn="urn:adsk.eagle:footprint:7517969/2" library_version="59">
<description>3216 metric</description>
<smd name="P$1" x="-1.525" y="0" dx="1.6" dy="0.85" layer="1" rot="R90"/>
<smd name="P$2" x="1.525" y="0" dx="1.6" dy="0.85" layer="1" rot="R90"/>
<wire x1="-2.1" y1="1" x2="-2.1" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1" x2="2.1" y2="-1" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1" x2="2.1" y2="1" width="0.127" layer="21"/>
<wire x1="2.1" y1="1" x2="-2.1" y2="1" width="0.127" layer="21"/>
<text x="-1.5" y="1.1" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="R3225" urn="urn:adsk.eagle:footprint:7517971/2" library_version="59">
<description>3225 metric</description>
<smd name="P$1" x="-1.525" y="0" dx="2.5" dy="0.85" layer="1" rot="R90"/>
<smd name="P$2" x="1.525" y="0" dx="2.5" dy="0.85" layer="1" rot="R90"/>
<wire x1="-2.1" y1="1.4" x2="-2.1" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.4" x2="2.1" y2="-1.4" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1.4" x2="2.1" y2="1.4" width="0.127" layer="21"/>
<wire x1="2.1" y1="1.4" x2="-2.1" y2="1.4" width="0.127" layer="21"/>
<text x="-1.5" y="1.5" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="R6332" urn="urn:adsk.eagle:footprint:7951151/1" library_version="59">
<description>6332 metric</description>
<smd name="P$1" x="-3.1" y="0" dx="3.2" dy="1" layer="1" rot="R90"/>
<smd name="P$2" x="3.1" y="0" dx="3.2" dy="1" layer="1" rot="R90"/>
<wire x1="-3.8" y1="1.8" x2="-3.8" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-3.8" y1="-1.8" x2="3.8" y2="-1.8" width="0.127" layer="21"/>
<wire x1="3.8" y1="-1.8" x2="3.8" y2="1.8" width="0.127" layer="21"/>
<wire x1="3.8" y1="1.8" x2="-3.8" y2="1.8" width="0.127" layer="21"/>
</package>
<package name="HC49S" urn="urn:adsk.eagle:footprint:7672690/4" library_version="59">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-3.2" y1="-2.35" x2="3.2" y2="-2.35" width="0.4064" layer="21"/>
<wire x1="-3.2" y1="2.35" x2="3.2" y2="2.35" width="0.4064" layer="21"/>
<wire x1="-3.05" y1="-1.65" x2="3.05" y2="-1.65" width="0.1524" layer="21"/>
<wire x1="3.05" y1="1.65" x2="-3.05" y2="1.65" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="0.254" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.762" x2="0.254" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.762" x2="-0.254" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.762" x2="-0.254" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0.762" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.762" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.2" y1="2.35" x2="-3.2" y2="-2.35" width="0.4064" layer="21" curve="180"/>
<wire x1="3.2" y1="-2.35" x2="3.2" y2="2.35" width="0.4064" layer="21" curve="180"/>
<wire x1="-3.05" y1="1.65" x2="-3.05" y2="-1.65" width="0.1524" layer="21" curve="180"/>
<wire x1="3.05" y1="-1.65" x2="3.05" y2="1.65" width="0.1524" layer="21" curve="180"/>
<wire x1="-3.2" y1="2.35" x2="3.2" y2="2.35" width="0.127" layer="21"/>
<wire x1="3.2" y1="-2.35" x2="-3.2" y2="-2.35" width="0.127" layer="21"/>
<pad name="1" x="-2.44" y="0" drill="0.8128"/>
<pad name="2" x="2.44" y="0" drill="0.8128"/>
<text x="-5.08" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
</package>
<package name="C1608" urn="urn:adsk.eagle:footprint:7517698/2" library_version="59">
<description>1608 metric</description>
<smd name="P$1" x="-0.75" y="0" dx="0.8" dy="0.5" layer="1" rot="R90"/>
<smd name="P$2" x="0.75" y="0" dx="0.8" dy="0.5" layer="1" rot="R90"/>
<wire x1="-1.2" y1="0.6" x2="-1.2" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-1.2" y1="-0.6" x2="1.2" y2="-0.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.6" x2="1.2" y2="0.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.6" x2="-1.2" y2="0.6" width="0.127" layer="21"/>
<text x="-0.8" y="0.7" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="C2012-1608" urn="urn:adsk.eagle:footprint:7517687/3" library_version="59">
<description>2012-1608 metric</description>
<smd name="P$1" x="-0.925" y="0" dx="1.25" dy="0.85" layer="1" rot="R90"/>
<smd name="P$2" x="0.925" y="0" dx="1.25" dy="0.85" layer="1" rot="R90"/>
<wire x1="-1.5" y1="0.8" x2="-1.5" y2="-0.8" width="0.254" layer="21"/>
<wire x1="-1.5" y1="-0.8" x2="1.5" y2="-0.8" width="0.254" layer="21"/>
<wire x1="1.5" y1="-0.8" x2="1.5" y2="0.8" width="0.254" layer="21"/>
<wire x1="1.5" y1="0.8" x2="-1.5" y2="0.8" width="0.254" layer="21"/>
<text x="-2.778" y="1.027" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="C2012" urn="urn:adsk.eagle:footprint:7517690/3" library_version="59">
<description>2012 metric</description>
<smd name="P$1" x="-1" y="0" dx="1.25" dy="0.7" layer="1" rot="R90"/>
<smd name="P$2" x="1" y="0" dx="1.25" dy="0.7" layer="1" rot="R90"/>
<wire x1="-1.5" y1="0.8" x2="-1.5" y2="-0.8" width="0.254" layer="21"/>
<wire x1="-1.5" y1="-0.8" x2="1.5" y2="-0.8" width="0.254" layer="21"/>
<wire x1="1.5" y1="-0.8" x2="1.5" y2="0.8" width="0.254" layer="21"/>
<wire x1="1.5" y1="0.8" x2="-1.5" y2="0.8" width="0.254" layer="21"/>
<text x="-1" y="0.9" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="C3216" urn="urn:adsk.eagle:footprint:7517968/2" library_version="59">
<description>3216 metric</description>
<smd name="P$1" x="-1.525" y="0" dx="1.6" dy="0.85" layer="1" rot="R90"/>
<smd name="P$2" x="1.525" y="0" dx="1.6" dy="0.85" layer="1" rot="R90"/>
<wire x1="-2.1" y1="1" x2="-2.1" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1" x2="2.1" y2="-1" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1" x2="2.1" y2="1" width="0.127" layer="21"/>
<wire x1="2.1" y1="1" x2="-2.1" y2="1" width="0.127" layer="21"/>
<text x="-1.5" y="1.1" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="C3225" urn="urn:adsk.eagle:footprint:7517759/2" library_version="59">
<description>3225 metric</description>
<smd name="P$1" x="-1.525" y="0" dx="2.5" dy="0.85" layer="1" rot="R90"/>
<smd name="P$2" x="1.525" y="0" dx="2.5" dy="0.85" layer="1" rot="R90"/>
<wire x1="-2.1" y1="1.4" x2="-2.1" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.4" x2="2.1" y2="-1.4" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1.4" x2="2.1" y2="1.4" width="0.127" layer="21"/>
<wire x1="2.1" y1="1.4" x2="-2.1" y2="1.4" width="0.127" layer="21"/>
<text x="-1.5" y="1.5" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="CRADIAL_5MM" urn="urn:adsk.eagle:footprint:8401270/5" library_version="59">
<pad name="P$1" x="-2.54" y="0" drill="0.7"/>
<pad name="P$2" x="2.46" y="0" drill="0.7"/>
<wire x1="-0.54" y1="1" x2="-0.54" y2="0" width="0.254" layer="21"/>
<wire x1="-0.54" y1="0" x2="-0.54" y2="-1" width="0.254" layer="21"/>
<wire x1="0.46" y1="1" x2="0.46" y2="0" width="0.254" layer="21"/>
<wire x1="0.46" y1="0" x2="0.46" y2="-1" width="0.254" layer="21"/>
<wire x1="-0.54" y1="0" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="0.46" y1="0" x2="2.46" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.5" x2="-2.54" y2="1.5" width="0.254" layer="21" curve="-180"/>
<wire x1="2.46" y1="1.5" x2="2.46" y2="-1.5" width="0.254" layer="21" curve="-180"/>
<wire x1="-2.54" y1="1.5" x2="2.46" y2="1.5" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.5" x2="2.46" y2="-1.5" width="0.254" layer="21"/>
</package>
<package name="CD66" urn="urn:adsk.eagle:footprint:7517693/4" library_version="66">
<smd name="P$1" x="-2.5" y="0" dx="3.2" dy="1.6" layer="1" rot="R180"/>
<smd name="P$2" x="2.5" y="0" dx="3.2" dy="1.6" layer="1"/>
<wire x1="-3.3" y1="-3.3" x2="-3.3" y2="-2" width="0.254" layer="21"/>
<wire x1="-3.3" y1="-2" x2="-3.3" y2="2" width="0.254" layer="21"/>
<wire x1="-3.3" y1="2" x2="-3.3" y2="3.3" width="0.254" layer="21"/>
<wire x1="-3.3" y1="3.3" x2="-2" y2="3.3" width="0.254" layer="21"/>
<wire x1="-2" y1="3.3" x2="3.3" y2="3.3" width="0.254" layer="21"/>
<wire x1="3.3" y1="3.3" x2="3.3" y2="-3.3" width="0.254" layer="21"/>
<wire x1="3.3" y1="-3.3" x2="-2" y2="-3.3" width="0.254" layer="21"/>
<wire x1="-2" y1="-3.3" x2="-3.3" y2="-3.3" width="0.254" layer="21"/>
<wire x1="-3.3" y1="2" x2="-2" y2="3.3" width="0.254" layer="21"/>
<wire x1="-3.3" y1="-2" x2="-2" y2="-3.3" width="0.254" layer="21"/>
<text x="-3.3" y="3.4" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="SOT23" urn="urn:adsk.eagle:footprint:28476/1" library_version="66">
<description>&lt;b&gt;SOT-23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.1854" x2="-1.4224" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="0.6604" x2="-0.6576" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.1854" width="0.1524" layer="21"/>
<wire x1="0.6326" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="TDSON-8" urn="urn:adsk.eagle:footprint:8397190/2" library_version="66">
<smd name="P$1" x="-1.905" y="0" dx="1.2" dy="0.65" layer="1" rot="R90"/>
<smd name="P$2" x="-0.635" y="0" dx="1.2" dy="0.65" layer="1" rot="R90"/>
<smd name="P$3" x="0.635" y="0" dx="1.2" dy="0.65" layer="1" rot="R90"/>
<smd name="P$4" x="1.905" y="0" dx="1.2" dy="0.65" layer="1" rot="R90"/>
<smd name="P$5" x="0" y="3.75" dx="4.7" dy="5" layer="1"/>
<wire x1="-2.6" y1="6.5" x2="2.6" y2="6.5" width="0.127" layer="21"/>
<wire x1="2.6" y1="6.5" x2="2.6" y2="-0.9" width="0.127" layer="21"/>
<wire x1="2.6" y1="-0.9" x2="-2.6" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-2.6" y1="-0.9" x2="-2.6" y2="6.5" width="0.127" layer="21"/>
<circle x="-1.905" y="-1.27" radius="0.2" width="0.127" layer="21"/>
<text x="-2.54" y="6.985" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="D2PAK-3P" urn="urn:adsk.eagle:footprint:8592080/1" library_version="66">
<smd name="P$1" x="-2.54" y="0" dx="3.81" dy="2.08" layer="1" rot="R90"/>
<smd name="P$2" x="2.54" y="0" dx="3.81" dy="2.08" layer="1" rot="R90"/>
<smd name="P$3" x="0" y="11.43" dx="11.43" dy="8.89" layer="1"/>
<wire x1="-5.125" y1="4.1" x2="-5.125" y2="14" width="0.508" layer="21"/>
<wire x1="-5.125" y1="14" x2="5.125" y2="14" width="0.508" layer="21"/>
<wire x1="5.125" y1="14" x2="5.125" y2="4.1" width="0.508" layer="21"/>
<wire x1="5.125" y1="4.1" x2="3.81" y2="4.1" width="0.508" layer="21"/>
<wire x1="3.81" y1="4.1" x2="2.54" y2="4.1" width="0.508" layer="21"/>
<wire x1="2.54" y1="4.1" x2="1.27" y2="4.1" width="0.508" layer="21"/>
<wire x1="1.27" y1="4.1" x2="-1.27" y2="4.1" width="0.508" layer="21"/>
<wire x1="-1.27" y1="4.1" x2="-2.54" y2="4.1" width="0.508" layer="21"/>
<wire x1="-2.54" y1="4.1" x2="-3.81" y2="4.1" width="0.508" layer="21"/>
<wire x1="-3.81" y1="4.1" x2="-5.125" y2="4.1" width="0.508" layer="21"/>
<wire x1="-2.54" y1="4.1" x2="-2.54" y2="-0.9" width="0.508" layer="21"/>
<wire x1="2.54" y1="4.1" x2="2.54" y2="-0.9" width="0.508" layer="21"/>
</package>
<package name="D2PAK-7P" urn="urn:adsk.eagle:footprint:8592066/1" library_version="66">
<smd name="P$1" x="-3.81" y="0" dx="3.2" dy="0.9" layer="1" rot="R90"/>
<smd name="P$2" x="-2.54" y="0" dx="3.2" dy="0.9" layer="1" rot="R90"/>
<smd name="P$3" x="-1.27" y="0" dx="3.2" dy="0.9" layer="1" rot="R90"/>
<smd name="P$4" x="1.27" y="0" dx="3.2" dy="0.9" layer="1" rot="R90"/>
<smd name="P$5" x="2.54" y="0" dx="3.2" dy="0.9" layer="1" rot="R90"/>
<smd name="P$6" x="3.81" y="0" dx="3.2" dy="0.9" layer="1" rot="R90"/>
<smd name="P$7" x="0" y="10.625" dx="10.3" dy="8.15" layer="1"/>
<wire x1="-5.125" y1="4.1" x2="-5.125" y2="14" width="0.508" layer="21"/>
<wire x1="-5.125" y1="14" x2="5.125" y2="14" width="0.508" layer="21"/>
<wire x1="5.125" y1="14" x2="5.125" y2="4.1" width="0.508" layer="21"/>
<wire x1="5.125" y1="4.1" x2="3.81" y2="4.1" width="0.508" layer="21"/>
<wire x1="3.81" y1="4.1" x2="2.54" y2="4.1" width="0.508" layer="21"/>
<wire x1="2.54" y1="4.1" x2="1.27" y2="4.1" width="0.508" layer="21"/>
<wire x1="1.27" y1="4.1" x2="-1.27" y2="4.1" width="0.508" layer="21"/>
<wire x1="-1.27" y1="4.1" x2="-2.54" y2="4.1" width="0.508" layer="21"/>
<wire x1="-2.54" y1="4.1" x2="-3.81" y2="4.1" width="0.508" layer="21"/>
<wire x1="-3.81" y1="4.1" x2="-5.125" y2="4.1" width="0.508" layer="21"/>
<wire x1="-1.27" y1="4.1" x2="-1.27" y2="-0.9" width="0.508" layer="21"/>
<wire x1="-2.54" y1="4.1" x2="-2.54" y2="-0.9" width="0.508" layer="21"/>
<wire x1="-3.81" y1="4.1" x2="-3.81" y2="-0.9" width="0.508" layer="21"/>
<wire x1="1.27" y1="4.1" x2="1.27" y2="-0.9" width="0.508" layer="21"/>
<wire x1="2.54" y1="4.1" x2="2.54" y2="-0.9" width="0.508" layer="21"/>
<wire x1="3.81" y1="4.1" x2="3.81" y2="-0.9" width="0.508" layer="21"/>
</package>
<package name="LED1608B" urn="urn:adsk.eagle:footprint:7669168/2" library_version="69">
<description>1608 metric</description>
<smd name="P$1" x="-0.75" y="0" dx="0.8" dy="0.5" layer="1" rot="R90"/>
<smd name="P$2" x="0.75" y="0" dx="0.8" dy="0.5" layer="1" rot="R90"/>
<wire x1="-1.2" y1="0.6" x2="-1.2" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-1.2" y1="-0.6" x2="1.2" y2="-0.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.6" x2="1.2" y2="0.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.6" x2="-1.2" y2="0.6" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-0.2" y="0.4"/>
<vertex x="-0.2" y="-0.4"/>
<vertex x="0.2" y="0"/>
</polygon>
<text x="-1.27" y="0.762" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="LED1608G" urn="urn:adsk.eagle:footprint:7669182/1" library_version="69">
<description>1608 metric</description>
<smd name="P$1" x="-0.75" y="0" dx="0.8" dy="0.5" layer="1" rot="R90"/>
<smd name="P$2" x="0.75" y="0" dx="0.8" dy="0.5" layer="1" rot="R90"/>
<wire x1="-1.2" y1="0.6" x2="-1.2" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-1.2" y1="-0.6" x2="1.2" y2="-0.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.6" x2="1.2" y2="0.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.6" x2="-1.2" y2="0.6" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-0.2" y="0.4"/>
<vertex x="-0.2" y="-0.4"/>
<vertex x="0.2" y="0"/>
</polygon>
<text x="-1.27" y="0.762" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="LED1608R" urn="urn:adsk.eagle:footprint:7669181/1" library_version="69">
<description>1608 metric</description>
<smd name="P$1" x="-0.75" y="0" dx="0.8" dy="0.5" layer="1" rot="R90"/>
<smd name="P$2" x="0.75" y="0" dx="0.8" dy="0.5" layer="1" rot="R90"/>
<wire x1="-1.2" y1="0.6" x2="-1.2" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-1.2" y1="-0.6" x2="1.2" y2="-0.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.6" x2="1.2" y2="0.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.6" x2="-1.2" y2="0.6" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-0.2" y="0.4"/>
<vertex x="-0.2" y="-0.4"/>
<vertex x="0.2" y="0"/>
</polygon>
<text x="-1.27" y="0.762" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="LED2012B" urn="urn:adsk.eagle:footprint:7669169/2" library_version="69">
<description>2012 metric</description>
<smd name="P$1" x="-1" y="0" dx="1.25" dy="0.7" layer="1" rot="R90"/>
<smd name="P$2" x="1" y="0" dx="1.25" dy="0.7" layer="1" rot="R90"/>
<wire x1="-1.5" y1="0.8" x2="-1.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.8" x2="1.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.8" x2="1.5" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.8" x2="-1.5" y2="0.8" width="0.127" layer="21"/>
<text x="-1.27" y="1.27" size="1.016" layer="21">&gt;NAME</text>
<polygon width="0.127" layer="21">
<vertex x="-0.25" y="0.5"/>
<vertex x="-0.25" y="-0.5"/>
<vertex x="0.25" y="0"/>
</polygon>
</package>
<package name="LED2012G" urn="urn:adsk.eagle:footprint:7669184/1" library_version="69">
<description>2012 metric</description>
<smd name="P$1" x="-1" y="0" dx="1.25" dy="0.7" layer="1" rot="R90"/>
<smd name="P$2" x="1" y="0" dx="1.25" dy="0.7" layer="1" rot="R90"/>
<wire x1="-1.5" y1="0.8" x2="-1.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.8" x2="1.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.8" x2="1.5" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.8" x2="-1.5" y2="0.8" width="0.127" layer="21"/>
<text x="-1.27" y="1.27" size="1.016" layer="21">&gt;NAME</text>
<polygon width="0.127" layer="21">
<vertex x="-0.25" y="0.5"/>
<vertex x="-0.25" y="-0.5"/>
<vertex x="0.25" y="0"/>
</polygon>
</package>
<package name="LED2012R" urn="urn:adsk.eagle:footprint:7669183/1" library_version="69">
<description>2012 metric</description>
<smd name="P$1" x="-1" y="0" dx="1.25" dy="0.7" layer="1" rot="R90"/>
<smd name="P$2" x="1" y="0" dx="1.25" dy="0.7" layer="1" rot="R90"/>
<wire x1="-1.5" y1="0.8" x2="-1.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.8" x2="1.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.8" x2="1.5" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.8" x2="-1.5" y2="0.8" width="0.127" layer="21"/>
<text x="-1.27" y="1.27" size="1.016" layer="21">&gt;NAME</text>
<polygon width="0.127" layer="21">
<vertex x="-0.25" y="0.5"/>
<vertex x="-0.25" y="-0.5"/>
<vertex x="0.25" y="0"/>
</polygon>
</package>
<package name="LED3216B" urn="urn:adsk.eagle:footprint:7669170/2" library_version="69">
<description>3216 metric</description>
<smd name="P$1" x="-1.525" y="0" dx="1.6" dy="0.85" layer="1" rot="R90"/>
<smd name="P$2" x="1.525" y="0" dx="1.6" dy="0.85" layer="1" rot="R90"/>
<wire x1="-2.1" y1="1" x2="-2.1" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1" x2="2.1" y2="-1" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1" x2="2.1" y2="1" width="0.127" layer="21"/>
<wire x1="2.1" y1="1" x2="-2.1" y2="1" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-0.4" y="0.8"/>
<vertex x="-0.4" y="-0.8"/>
<vertex x="0.4" y="0"/>
</polygon>
<text x="-2" y="1.1" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="LED3216G" urn="urn:adsk.eagle:footprint:7669186/1" library_version="69">
<description>3216 metric</description>
<smd name="P$1" x="-1.525" y="0" dx="1.6" dy="0.85" layer="1" rot="R90"/>
<smd name="P$2" x="1.525" y="0" dx="1.6" dy="0.85" layer="1" rot="R90"/>
<wire x1="-2.1" y1="1" x2="-2.1" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1" x2="2.1" y2="-1" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1" x2="2.1" y2="1" width="0.127" layer="21"/>
<wire x1="2.1" y1="1" x2="-2.1" y2="1" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-0.4" y="0.8"/>
<vertex x="-0.4" y="-0.8"/>
<vertex x="0.4" y="0"/>
</polygon>
<text x="-2" y="1.1" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="LED3216R" urn="urn:adsk.eagle:footprint:7669185/1" library_version="69">
<description>3216 metric</description>
<smd name="P$1" x="-1.525" y="0" dx="1.6" dy="0.85" layer="1" rot="R90"/>
<smd name="P$2" x="1.525" y="0" dx="1.6" dy="0.85" layer="1" rot="R90"/>
<wire x1="-2.1" y1="1" x2="-2.1" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1" x2="2.1" y2="-1" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1" x2="2.1" y2="1" width="0.127" layer="21"/>
<wire x1="2.1" y1="1" x2="-2.1" y2="1" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-0.4" y="0.8"/>
<vertex x="-0.4" y="-0.8"/>
<vertex x="0.4" y="0"/>
</polygon>
<text x="-2" y="1.1" size="1.016" layer="21">&gt;NAME</text>
</package>
</packages>
<packages3d>
<package3d name="RESC2012X55" urn="urn:adsk.eagle:package:7517727/4" type="model" library_version="59">
<description>2012 metric</description>
<packageinstances>
<packageinstance name="R2012"/>
</packageinstances>
</package3d>
<package3d name="RESC2012X55" urn="urn:adsk.eagle:package:7517718/3" type="model" library_version="59">
<description>2012-1608 metric</description>
<packageinstances>
<packageinstance name="R2012-1608"/>
</packageinstances>
</package3d>
<package3d name="RESC1608X45" urn="urn:adsk.eagle:package:7517721/3" type="model" library_version="59">
<description>1608 metric</description>
<packageinstances>
<packageinstance name="R1608"/>
</packageinstances>
</package3d>
<package3d name="RESC3216X60" urn="urn:adsk.eagle:package:7517980/3" type="model" library_version="59">
<description>3216 metric</description>
<packageinstances>
<packageinstance name="R3216"/>
</packageinstances>
</package3d>
<package3d name="RESC3225X60" urn="urn:adsk.eagle:package:7517978/3" type="model" library_version="59">
<description>3225 metric</description>
<packageinstances>
<packageinstance name="R3225"/>
</packageinstances>
</package3d>
<package3d name="RESC6332X60" urn="urn:adsk.eagle:package:7951152/2" type="model" library_version="59">
<description>Chip, 6.30 X 3.20 X 0.60 mm body
&lt;p&gt;Chip package with body size 6.30 X 3.20 X 0.60 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="R6332"/>
</packageinstances>
</package3d>
<package3d name="HC49S" urn="urn:adsk.eagle:package:7672691/5" type="model" library_version="59">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="HC49S"/>
</packageinstances>
</package3d>
<package3d name="CAPC1608X45" urn="urn:adsk.eagle:package:7517728/3" type="model" library_version="59">
<description>1608 metric</description>
<packageinstances>
<packageinstance name="C1608"/>
</packageinstances>
</package3d>
<package3d name="CAPC2012X55" urn="urn:adsk.eagle:package:7517717/4" type="model" library_version="59">
<description>2012-1608 metric</description>
<packageinstances>
<packageinstance name="C2012-1608"/>
</packageinstances>
</package3d>
<package3d name="CAPC2012X55" urn="urn:adsk.eagle:package:7517720/4" type="model" library_version="59">
<description>2012 metric</description>
<packageinstances>
<packageinstance name="C2012"/>
</packageinstances>
</package3d>
<package3d name="CAPC3216X80" urn="urn:adsk.eagle:package:7517981/4" type="model" library_version="59">
<description>Chip, 3.20 X 1.60 X 0.80 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 0.80 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="C3216"/>
</packageinstances>
</package3d>
<package3d name="CAPC3225X200" urn="urn:adsk.eagle:package:7517760/3" type="model" library_version="59">
<description>3225 metric</description>
<packageinstances>
<packageinstance name="C3225"/>
</packageinstances>
</package3d>
<package3d name="CRADIAL_5MM" urn="urn:adsk.eagle:package:8401290/5" type="model" library_version="59">
<packageinstances>
<packageinstance name="CRADIAL_5MM"/>
</packageinstances>
</package3d>
<package3d name="CAPAE660X450" urn="urn:adsk.eagle:package:7517723/5" type="model" library_version="66">
<packageinstances>
<packageinstance name="CD66"/>
</packageinstances>
</package3d>
<package3d name="SOT95P240X110-3" urn="urn:adsk.eagle:package:7672745/2" type="model" library_version="66">
<description>3-SOT23, 0.95 mm pitch, 2.40 mm span, 2.90 X 1.30 X 1.10 mm body
&lt;p&gt;3-pin SOT23 package with 0.95 mm pitch, 2.40 mm span with body size 2.90 X 1.30 X 1.10 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT23"/>
</packageinstances>
</package3d>
<package3d name="TDSON-8" urn="urn:adsk.eagle:package:8397191/3" type="model" library_version="66">
<packageinstances>
<packageinstance name="TDSON-8"/>
</packageinstances>
</package3d>
<package3d name="D2PAK-3P" urn="urn:adsk.eagle:package:8592081/2" type="model" library_version="66">
<packageinstances>
<packageinstance name="D2PAK-3P"/>
</packageinstances>
</package3d>
<package3d name="D2PAK-7P" urn="urn:adsk.eagle:package:8592067/2" type="model" library_version="66">
<packageinstances>
<packageinstance name="D2PAK-7P"/>
</packageinstances>
</package3d>
<package3d name="LED1608B" urn="urn:adsk.eagle:package:7669187/2" type="model" library_version="69">
<description>1608 metric</description>
<packageinstances>
<packageinstance name="LED1608B"/>
</packageinstances>
</package3d>
<package3d name="LED1608G" urn="urn:adsk.eagle:package:7669191/2" type="model" library_version="69">
<description>1608 metric</description>
<packageinstances>
<packageinstance name="LED1608G"/>
</packageinstances>
</package3d>
<package3d name="LED1608R" urn="urn:adsk.eagle:package:7669190/2" type="model" library_version="69">
<description>1608 metric</description>
<packageinstances>
<packageinstance name="LED1608R"/>
</packageinstances>
</package3d>
<package3d name="LED2012B" urn="urn:adsk.eagle:package:7669189/2" type="model" library_version="69">
<description>2012 metric</description>
<packageinstances>
<packageinstance name="LED2012B"/>
</packageinstances>
</package3d>
<package3d name="LED2012G" urn="urn:adsk.eagle:package:7669193/2" type="model" library_version="69">
<description>2012 metric</description>
<packageinstances>
<packageinstance name="LED2012G"/>
</packageinstances>
</package3d>
<package3d name="LED2012R" urn="urn:adsk.eagle:package:7669192/2" type="model" library_version="69">
<description>2012 metric</description>
<packageinstances>
<packageinstance name="LED2012R"/>
</packageinstances>
</package3d>
<package3d name="LED3216B" urn="urn:adsk.eagle:package:7669188/2" type="model" library_version="69">
<description>3216 metric</description>
<packageinstances>
<packageinstance name="LED3216B"/>
</packageinstances>
</package3d>
<package3d name="LED3216G" urn="urn:adsk.eagle:package:7669195/2" type="model" library_version="69">
<description>3216 metric</description>
<packageinstances>
<packageinstance name="LED3216G"/>
</packageinstances>
</package3d>
<package3d name="LED3216R" urn="urn:adsk.eagle:package:7669194/2" type="model" library_version="69">
<description>3216 metric</description>
<packageinstances>
<packageinstance name="LED3216R"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="RESISTER" urn="urn:adsk.eagle:symbol:7517715/1" library_version="59">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="XTAL" urn="urn:adsk.eagle:symbol:7552353/1" library_version="59">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CAPACITOR" urn="urn:adsk.eagle:symbol:7517714/1" library_version="59">
<wire x1="0" y1="1.27" x2="0" y2="0.762" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-0.762" width="0.1524" layer="94"/>
<text x="1.524" y="1.651" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.762" x2="2.032" y2="-0.254" layer="94"/>
<rectangle x1="-2.032" y1="0.254" x2="2.032" y2="0.762" layer="94"/>
<pin name="1" x="0" y="3.81" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-3.81" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="E_CAPACITOR" urn="urn:adsk.eagle:symbol:7517712/2" library_version="66">
<wire x1="-1.524" y1="0.381" x2="1.524" y2="0.381" width="0.254" layer="94"/>
<wire x1="1.524" y1="0.381" x2="1.524" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.524" y1="1.27" x2="-1.524" y2="0.381" width="0.254" layer="94"/>
<wire x1="-1.524" y1="1.27" x2="1.524" y2="1.27" width="0.254" layer="94"/>
<text x="1.143" y="1.7526" size="1.778" layer="95">&gt;NAME</text>
<text x="-0.5842" y="1.6764" size="1.27" layer="94" rot="R90">+</text>
<text x="1.143" y="-3.3274" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.27" x2="1.651" y2="-0.381" layer="94"/>
<pin name="-" x="0" y="-3.81" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="+" x="0" y="3.81" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="N-MOSFET" urn="urn:adsk.eagle:symbol:7672744/1" library_version="66">
<wire x1="0.762" y1="0.762" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.175" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.905" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.762" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<wire x1="4.572" y1="0.762" x2="4.318" y2="0.508" width="0.1524" layer="94"/>
<wire x1="4.318" y1="0.508" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.302" y1="0.508" x2="3.048" y2="0.254" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.3592" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.3592" width="0" layer="94"/>
<text x="-11.43" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="-11.43" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="3.81" y="0.508"/>
<vertex x="3.302" y="-0.254"/>
<vertex x="4.318" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="1.016" y="0"/>
<vertex x="2.032" y="0.762"/>
<vertex x="2.032" y="-0.762"/>
</polygon>
</symbol>
<symbol name="LED" urn="urn:adsk.eagle:symbol:7669161/3" library_version="69">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<pin name="K" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTER" urn="urn:adsk.eagle:component:7517745/7" prefix="R" uservalue="yes" library_version="59">
<description>resister</description>
<gates>
<gate name="G$1" symbol="RESISTER" x="0" y="0"/>
</gates>
<devices>
<device name="2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7517727/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2012-1608" package="R2012-1608">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7517718/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1608" package="R1608">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7517721/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7517980/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7517978/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7951152/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XTAL" urn="urn:adsk.eagle:component:7552363/3" prefix="X" library_version="59">
<gates>
<gate name="G$1" symbol="XTAL" x="2.54" y="0"/>
</gates>
<devices>
<device name="HC49S" package="HC49S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7672691/5"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" urn="urn:adsk.eagle:component:7517744/9" prefix="C" uservalue="yes" library_version="59">
<description>capacitor</description>
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7517728/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2012-1608" package="C2012-1608">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7517717/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7517720/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7517981/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7517760/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RADIAL_5MM" package="CRADIAL_5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8401290/5"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="E_CAPACITOR" urn="urn:adsk.eagle:component:7517740/6" prefix="C" uservalue="yes" library_version="66">
<gates>
<gate name="G$1" symbol="E_CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="D63" package="CD66">
<connects>
<connect gate="G$1" pin="+" pad="P$1"/>
<connect gate="G$1" pin="-" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7517723/5"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="N-MOSFET" urn="urn:adsk.eagle:component:7672749/3" prefix="Q" library_version="66">
<gates>
<gate name="G$1" symbol="N-MOSFET" x="2.54" y="2.54"/>
</gates>
<devices>
<device name="SOT23" package="SOT23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7672745/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TDSON-8" package="TDSON-8">
<connects>
<connect gate="G$1" pin="D" pad="P$5"/>
<connect gate="G$1" pin="G" pad="P$4"/>
<connect gate="G$1" pin="S" pad="P$1 P$2 P$3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8397191/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D2PAK-3P" package="D2PAK-3P">
<connects>
<connect gate="G$1" pin="D" pad="P$3"/>
<connect gate="G$1" pin="G" pad="P$1"/>
<connect gate="G$1" pin="S" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8592081/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D2PAK-7P" package="D2PAK-7P">
<connects>
<connect gate="G$1" pin="D" pad="P$7"/>
<connect gate="G$1" pin="G" pad="P$1"/>
<connect gate="G$1" pin="S" pad="P$2 P$3 P$4 P$5 P$6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8592067/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" urn="urn:adsk.eagle:component:7671073/3" prefix="D" library_version="69">
<gates>
<gate name="G$1" symbol="LED" x="0" y="-2.54"/>
</gates>
<devices>
<device name="1608B" package="LED1608B">
<connects>
<connect gate="G$1" pin="A" pad="P$1"/>
<connect gate="G$1" pin="K" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7669187/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1608G" package="LED1608G">
<connects>
<connect gate="G$1" pin="A" pad="P$1"/>
<connect gate="G$1" pin="K" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7669191/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1608R" package="LED1608R">
<connects>
<connect gate="G$1" pin="A" pad="P$1"/>
<connect gate="G$1" pin="K" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7669190/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2012B" package="LED2012B">
<connects>
<connect gate="G$1" pin="A" pad="P$1"/>
<connect gate="G$1" pin="K" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7669189/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2012G" package="LED2012G">
<connects>
<connect gate="G$1" pin="A" pad="P$1"/>
<connect gate="G$1" pin="K" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7669193/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2012R" package="LED2012R">
<connects>
<connect gate="G$1" pin="A" pad="P$1"/>
<connect gate="G$1" pin="K" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7669192/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3216B" package="LED3216B">
<connects>
<connect gate="G$1" pin="A" pad="P$1"/>
<connect gate="G$1" pin="K" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7669188/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3216G" package="LED3216G">
<connects>
<connect gate="G$1" pin="A" pad="P$1"/>
<connect gate="G$1" pin="K" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7669195/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3216R" package="LED3216R">
<connects>
<connect gate="G$1" pin="A" pad="P$1"/>
<connect gate="G$1" pin="K" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7669194/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-jst-zh" urn="urn:adsk.eagle:library:8322185">
<packages>
<package name="B6B-ZR" urn="urn:adsk.eagle:footprint:8322197/1" library_version="7">
<pad name="1" x="0" y="0" drill="0.7" shape="square" rot="R90"/>
<pad name="2" x="1.5" y="0" drill="0.7" rot="R90"/>
<pad name="3" x="3" y="0" drill="0.7" rot="R90"/>
<pad name="4" x="4.5" y="0" drill="0.7" rot="R90"/>
<pad name="5" x="6" y="0" drill="0.7" rot="R90"/>
<pad name="6" x="7.5" y="0" drill="0.7" rot="R90"/>
<text x="-1.45" y="1.5" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<wire x1="-1.5" y1="1.3" x2="-0.25" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-0.25" y1="1.3" x2="7.75" y2="1.3" width="0.1524" layer="21"/>
<wire x1="7.75" y1="1.3" x2="9" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="1.3" x2="-1.5" y2="0.8" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="0.8" x2="-1.5" y2="0.25" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="0.25" x2="-1.5" y2="-2.2" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="-2.2" x2="9" y2="-2.2" width="0.1524" layer="21"/>
<wire x1="9" y1="-2.2" x2="9" y2="0.25" width="0.1524" layer="21"/>
<wire x1="9" y1="0.25" x2="9" y2="0.8" width="0.1524" layer="21"/>
<wire x1="9" y1="0.8" x2="9" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="0.8" x2="-0.25" y2="0.8" width="0.1524" layer="21"/>
<wire x1="-0.25" y1="0.8" x2="-0.25" y2="1.3" width="0.1524" layer="21"/>
<wire x1="9" y1="0.8" x2="7.75" y2="0.8" width="0.1524" layer="21"/>
<wire x1="7.75" y1="0.8" x2="7.75" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="0.25" x2="-1" y2="0.25" width="0.1524" layer="21"/>
<wire x1="-1" y1="0.25" x2="-1" y2="-1.7" width="0.1524" layer="21"/>
<wire x1="-1" y1="-1.7" x2="8.5" y2="-1.7" width="0.1524" layer="21"/>
<wire x1="8.5" y1="-1.7" x2="8.5" y2="0.25" width="0.1524" layer="21"/>
<wire x1="8.5" y1="0.25" x2="9" y2="0.25" width="0.1524" layer="21"/>
</package>
<package name="S6B-ZR" urn="urn:adsk.eagle:footprint:8322189/3" library_version="7">
<pad name="1" x="0" y="0" drill="0.7" shape="square" rot="R90"/>
<pad name="2" x="-1.5" y="0" drill="0.7" rot="R90"/>
<pad name="3" x="-3" y="0" drill="0.7" rot="R90"/>
<pad name="4" x="-4.5" y="0" drill="0.7" rot="R90"/>
<pad name="5" x="-6" y="0" drill="0.7" rot="R90"/>
<pad name="6" x="-7.5" y="0" drill="0.7" rot="R90"/>
<text x="-8.76" y="4.92" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<wire x1="-9" y1="4.6" x2="-9" y2="-1.4" width="0.1524" layer="21"/>
<wire x1="-9" y1="4.6" x2="1.5" y2="4.6" width="0.1524" layer="21"/>
<wire x1="1.5" y1="4.6" x2="1.5" y2="-1.4" width="0.1524" layer="21"/>
<wire x1="-9" y1="-1.4" x2="-8.5" y2="-1.4" width="0.1524" layer="21"/>
<wire x1="-8.5" y1="-1.4" x2="-8.5" y2="1" width="0.1524" layer="21"/>
<wire x1="-8.5" y1="1" x2="1" y2="1" width="0.1524" layer="21"/>
<wire x1="1" y1="1" x2="1" y2="-1.4" width="0.1524" layer="21"/>
<wire x1="1" y1="-1.4" x2="1.5" y2="-1.4" width="0.1524" layer="21"/>
<wire x1="0" y1="1.25" x2="-0.25" y2="1.75" width="0.127" layer="21"/>
<wire x1="-0.25" y1="1.75" x2="0.25" y2="1.75" width="0.127" layer="21"/>
<wire x1="0.25" y1="1.75" x2="0" y2="1.25" width="0.127" layer="21"/>
<wire x1="-8.5" y1="3.5" x2="-8" y2="3.5" width="0.127" layer="21"/>
<wire x1="-8" y1="3.5" x2="-8" y2="1.5" width="0.127" layer="21"/>
<wire x1="-8" y1="1.5" x2="-8.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-8.5" y1="1.5" x2="-8.5" y2="3.5" width="0.127" layer="21"/>
<wire x1="0.5" y1="3.5" x2="1" y2="3.5" width="0.127" layer="21"/>
<wire x1="1" y1="3.5" x2="1" y2="1.5" width="0.127" layer="21"/>
<wire x1="1" y1="1.5" x2="0.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="0.5" y1="1.5" x2="0.5" y2="3.5" width="0.127" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="B6B-ZR" urn="urn:adsk.eagle:package:8322221/2" type="model" library_version="7">
<packageinstances>
<packageinstance name="B6B-ZR"/>
</packageinstances>
</package3d>
<package3d name="S6B-ZR" urn="urn:adsk.eagle:package:8322213/4" type="model" library_version="7">
<packageinstances>
<packageinstance name="S6B-ZR"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="CONNECTOR-6P" urn="urn:adsk.eagle:symbol:8322205/1" library_version="7">
<description>Connector 3P</description>
<pin name="1" x="0" y="0" visible="pin" length="middle"/>
<pin name="2" x="0" y="-2.54" visible="pin" length="middle"/>
<pin name="3" x="0" y="-5.08" visible="pin" length="middle"/>
<pin name="4" x="0" y="-7.62" visible="pin" length="middle"/>
<pin name="5" x="0" y="-10.16" visible="pin" length="middle"/>
<pin name="6" x="0" y="-12.7" visible="pin" length="middle"/>
<text x="5.08" y="3.302" size="1.27" layer="95">&gt;NAME</text>
<text x="5.08" y="-17.018" size="1.27" layer="96">&gt;VALUE</text>
<wire x1="2.54" y1="2.54" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="2.54" x2="10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="2.54" y2="-15.24" width="0.254" layer="94"/>
<wire x1="2.54" y1="-15.24" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<circle x="5.08" y="0" radius="0.915809375" width="0.254" layer="94"/>
<circle x="5.08" y="-2.54" radius="0.915809375" width="0.254" layer="94"/>
<circle x="5.08" y="-5.08" radius="0.915809375" width="0.254" layer="94"/>
<circle x="5.08" y="-7.62" radius="0.915809375" width="0.254" layer="94"/>
<circle x="5.08" y="-10.16" radius="0.915809375" width="0.254" layer="94"/>
<circle x="5.08" y="-12.7" radius="0.915809375" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ZH-6P" urn="urn:adsk.eagle:component:8322229/5" prefix="CN" library_version="7">
<gates>
<gate name="G$1" symbol="CONNECTOR-6P" x="0" y="0"/>
</gates>
<devices>
<device name="B6B" package="B6B-ZR">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8322221/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="S6B" package="S6B-ZR">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8322213/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="grayhill" urn="urn:adsk.eagle:library:8431917">
<packages>
<package name="97C02ST" urn="urn:adsk.eagle:footprint:8431921/2" library_version="3">
<description>&lt;b&gt;DIL/CODE SWITCH&lt;/b&gt;&lt;p&gt;
Mors&lt;p&gt;
distributor Buerklin, 17G550</description>
<wire x1="-1.8796" y1="-2.286" x2="1.8796" y2="-2.286" width="0.254" layer="21"/>
<wire x1="1.8796" y1="2.286" x2="-1.8796" y2="2.286" width="0.254" layer="21"/>
<wire x1="-1.8796" y1="2.286" x2="-1.8796" y2="-2.286" width="0.254" layer="21"/>
<wire x1="1.8796" y1="-2.286" x2="1.8796" y2="2.286" width="0.254" layer="21"/>
<wire x1="-0.9525" y1="1.27" x2="-0.9525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.9525" y1="1.27" x2="-0.3175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.3175" y1="-1.27" x2="-0.3175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.3175" y1="-1.27" x2="-0.9525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.3175" y1="1.27" x2="0.3175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.3175" y1="1.27" x2="0.9525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.9525" y1="-1.27" x2="0.9525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.9525" y1="-1.27" x2="0.3175" y2="-1.27" width="0.1524" layer="21"/>
<text x="-2.54" y="-2.54" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-1.143" y="0.889" size="0.6096" layer="25" ratio="14" rot="R90">ON</text>
<smd name="P$1" x="-0.635" y="-3.0734" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$2" x="0.635" y="-3.0734" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$3" x="0.635" y="3.0734" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$4" x="-0.635" y="3.0734" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<rectangle x1="-0.9525" y1="-1.27" x2="-0.3175" y2="0" layer="21"/>
<rectangle x1="0.3175" y1="-1.27" x2="0.9525" y2="0" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="97C02ST" urn="urn:adsk.eagle:package:8431923/3" type="model" library_version="3">
<description>&lt;b&gt;DIL/CODE SWITCH&lt;/b&gt;&lt;p&gt;
Mors&lt;p&gt;
distributor Buerklin, 17G550</description>
<packageinstances>
<packageinstance name="97C02ST"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="DIP02" urn="urn:adsk.eagle:symbol:8431920/1" library_version="3">
<wire x1="-1.905" y1="5.08" x2="-1.905" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.905" y1="5.08" x2="4.445" y2="5.08" width="0.4064" layer="94"/>
<wire x1="4.445" y1="-5.08" x2="4.445" y2="5.08" width="0.4064" layer="94"/>
<wire x1="4.445" y1="-5.08" x2="-1.905" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="0.508" y1="-2.54" x2="-0.508" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="2.54" x2="0.508" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0.508" y1="2.54" x2="0.508" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="-2.54" x2="-0.508" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-2.54" x2="2.032" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.032" y1="2.54" x2="3.048" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.048" y1="2.54" x2="3.048" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.032" y1="-2.54" x2="2.032" y2="2.54" width="0.1524" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="6.985" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<text x="-0.254" y="-4.064" size="0.9906" layer="94" ratio="14">1</text>
<text x="2.159" y="-4.064" size="0.9906" layer="94" ratio="14">2</text>
<text x="-0.762" y="3.048" size="0.9906" layer="94" ratio="14">ON</text>
<rectangle x1="-0.254" y1="-2.286" x2="0.254" y2="0" layer="94"/>
<rectangle x1="2.286" y1="-2.286" x2="2.794" y2="0" layer="94"/>
<pin name="3" x="2.54" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="4" x="0" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="1" x="0" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="2.54" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="97C02ST" urn="urn:adsk.eagle:component:8431925/3" prefix="SW" library_version="3">
<gates>
<gate name="G$1" symbol="DIP02" x="0" y="0"/>
</gates>
<devices>
<device name="" package="97C02ST">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8431923/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="TDK-InvenSense" urn="urn:adsk.eagle:library:8545760">
<packages>
<package name="ICM-20689" urn="urn:adsk.eagle:footprint:7564500/2" library_version="2">
<smd name="P$25" x="0" y="0" dx="2.45" dy="2.45" layer="1"/>
<smd name="P$1" x="-2" y="1.25" dx="0.95" dy="0.3" layer="1"/>
<smd name="P$2" x="-2" y="0.75" dx="0.95" dy="0.3" layer="1"/>
<smd name="P$3" x="-2" y="0.25" dx="0.95" dy="0.3" layer="1"/>
<smd name="P$4" x="-2" y="-0.25" dx="0.95" dy="0.3" layer="1"/>
<smd name="P$5" x="-2" y="-0.75" dx="0.95" dy="0.3" layer="1"/>
<smd name="P$6" x="-2" y="-1.25" dx="0.95" dy="0.3" layer="1"/>
<smd name="P$7" x="-1.25" y="-2" dx="0.95" dy="0.3" layer="1" rot="R90"/>
<smd name="P$8" x="-0.75" y="-2" dx="0.95" dy="0.3" layer="1" rot="R90"/>
<smd name="P$9" x="-0.25" y="-2" dx="0.95" dy="0.3" layer="1" rot="R90"/>
<smd name="P$10" x="0.25" y="-2" dx="0.95" dy="0.3" layer="1" rot="R90"/>
<smd name="P$11" x="0.75" y="-2" dx="0.95" dy="0.3" layer="1" rot="R90"/>
<smd name="P$12" x="1.25" y="-2" dx="0.95" dy="0.3" layer="1" rot="R90"/>
<smd name="P$13" x="2" y="-1.25" dx="0.95" dy="0.3" layer="1" rot="R180"/>
<smd name="P$14" x="2" y="-0.75" dx="0.95" dy="0.3" layer="1" rot="R180"/>
<smd name="P$15" x="2" y="-0.25" dx="0.95" dy="0.3" layer="1" rot="R180"/>
<smd name="P$16" x="2" y="0.25" dx="0.95" dy="0.3" layer="1" rot="R180"/>
<smd name="P$17" x="2" y="0.75" dx="0.95" dy="0.3" layer="1" rot="R180"/>
<smd name="P$18" x="2" y="1.25" dx="0.95" dy="0.3" layer="1" rot="R180"/>
<smd name="P$19" x="1.25" y="2" dx="0.95" dy="0.3" layer="1" rot="R270"/>
<smd name="P$20" x="0.75" y="2" dx="0.95" dy="0.3" layer="1" rot="R270"/>
<smd name="P$21" x="0.25" y="2" dx="0.95" dy="0.3" layer="1" rot="R270"/>
<smd name="P$22" x="-0.25" y="2" dx="0.95" dy="0.3" layer="1" rot="R270"/>
<smd name="P$23" x="-0.75" y="2" dx="0.95" dy="0.3" layer="1" rot="R270"/>
<smd name="P$24" x="-1.25" y="2" dx="0.95" dy="0.3" layer="1" rot="R270"/>
<wire x1="-2" y1="1.5" x2="-2" y2="-2" width="0.127" layer="21"/>
<wire x1="-2" y1="-2" x2="2" y2="-2" width="0.127" layer="21"/>
<wire x1="2" y1="-2" x2="2" y2="2" width="0.127" layer="21"/>
<wire x1="2" y1="2" x2="-1.5" y2="2" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2" x2="-2" y2="1.5" width="0.127" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="QFN50P400X400X85-25T250" urn="urn:adsk.eagle:package:7564501/3" type="model" library_version="2">
<packageinstances>
<packageinstance name="ICM-20689"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="ICM-20689" urn="urn:adsk.eagle:symbol:8546011/1" library_version="2">
<wire x1="-15.24" y1="2.54" x2="-15.24" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-30.48" x2="15.24" y2="-30.48" width="0.254" layer="94"/>
<wire x1="15.24" y1="-30.48" x2="15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="2.54" x2="1.778" y2="2.54" width="0.254" layer="94"/>
<wire x1="-15.24" y1="2.54" x2="-1.778" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.778" y1="2.54" x2="1.778" y2="2.54" width="0.254" layer="94" curve="171.200618"/>
<text x="-5.08" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<pin name="NC1" x="-17.78" y="0" length="short" direction="pas"/>
<pin name="NC2" x="-17.78" y="-2.54" length="short" direction="pas"/>
<pin name="NC3" x="-17.78" y="-5.08" length="short" direction="pas"/>
<pin name="NC4" x="-17.78" y="-7.62" length="short" direction="pas"/>
<pin name="NC5" x="-17.78" y="-10.16" length="short" direction="pas"/>
<pin name="NC6" x="-17.78" y="-12.7" length="short" direction="pas"/>
<pin name="NC7" x="-17.78" y="-15.24" length="short" direction="pas"/>
<pin name="VDDIO" x="-17.78" y="-17.78" length="short" direction="pas"/>
<pin name="ADO/SDO" x="-17.78" y="-20.32" length="short" direction="pas"/>
<pin name="REGOUT" x="-17.78" y="-22.86" length="short" direction="pas"/>
<pin name="FSYNC" x="-17.78" y="-25.4" length="short" direction="pas"/>
<pin name="INT" x="-17.78" y="-27.94" length="short" direction="pas"/>
<pin name="VDD" x="17.78" y="-27.94" length="short" direction="pas" rot="R180"/>
<pin name="NC8" x="17.78" y="-25.4" length="short" direction="pas" rot="R180"/>
<pin name="NC9" x="17.78" y="-22.86" length="short" direction="pas" rot="R180"/>
<pin name="NC10" x="17.78" y="-20.32" length="short" direction="pas" rot="R180"/>
<pin name="NC11" x="17.78" y="-17.78" length="short" direction="pas" rot="R180"/>
<pin name="GND1" x="17.78" y="-15.24" length="short" direction="pas" rot="R180"/>
<pin name="NC12" x="17.78" y="-12.7" length="short" direction="pas" rot="R180"/>
<pin name="NC13" x="17.78" y="-10.16" length="short" direction="pas" rot="R180"/>
<pin name="NC14" x="17.78" y="-7.62" length="short" direction="pas" rot="R180"/>
<pin name="NCS" x="17.78" y="-5.08" length="short" direction="pas" rot="R180"/>
<pin name="SCL/SCLK" x="17.78" y="-2.54" length="short" direction="pas" rot="R180"/>
<pin name="SDA/SDI" x="17.78" y="0" length="short" direction="pas" rot="R180"/>
<pin name="GND2" x="0" y="-33.02" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ICM-20689" urn="urn:adsk.eagle:component:8546012/1" library_version="2">
<gates>
<gate name="G$1" symbol="ICM-20689" x="17.78" y="0"/>
</gates>
<devices>
<device name="QFN" package="ICM-20689">
<connects>
<connect gate="G$1" pin="ADO/SDO" pad="P$9"/>
<connect gate="G$1" pin="FSYNC" pad="P$11"/>
<connect gate="G$1" pin="GND1" pad="P$18"/>
<connect gate="G$1" pin="GND2" pad="P$25"/>
<connect gate="G$1" pin="INT" pad="P$12"/>
<connect gate="G$1" pin="NC1" pad="P$1"/>
<connect gate="G$1" pin="NC10" pad="P$16"/>
<connect gate="G$1" pin="NC11" pad="P$17"/>
<connect gate="G$1" pin="NC12" pad="P$19"/>
<connect gate="G$1" pin="NC13" pad="P$20"/>
<connect gate="G$1" pin="NC14" pad="P$21"/>
<connect gate="G$1" pin="NC2" pad="P$2"/>
<connect gate="G$1" pin="NC3" pad="P$3"/>
<connect gate="G$1" pin="NC4" pad="P$4"/>
<connect gate="G$1" pin="NC5" pad="P$5"/>
<connect gate="G$1" pin="NC6" pad="P$6"/>
<connect gate="G$1" pin="NC7" pad="P$7"/>
<connect gate="G$1" pin="NC8" pad="P$14"/>
<connect gate="G$1" pin="NC9" pad="P$15"/>
<connect gate="G$1" pin="NCS" pad="P$22"/>
<connect gate="G$1" pin="REGOUT" pad="P$10"/>
<connect gate="G$1" pin="SCL/SCLK" pad="P$23"/>
<connect gate="G$1" pin="SDA/SDI" pad="P$24"/>
<connect gate="G$1" pin="VDD" pad="P$13"/>
<connect gate="G$1" pin="VDDIO" pad="P$8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7564501/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinheads_2" urn="urn:adsk.eagle:library:7523781">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de, Modified by Cougar@CasaDelGato.Com&lt;/author&gt;</description>
<packages>
<package name="2X02" urn="urn:adsk.eagle:footprint:7523843/2" library_version="7">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2.54</description>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="square"/>
<pad name="2" x="0" y="2.54" drill="1.016" shape="octagon"/>
<pad name="3" x="2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" shape="octagon"/>
<text x="-1.27" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
<package name="2X02/90" urn="urn:adsk.eagle:footprint:7523904/2" library_version="7">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2.54 90°</description>
<wire x1="-1.27" y1="4.445" x2="1.27" y2="4.445" width="0.1524" layer="21"/>
<wire x1="1.27" y1="4.445" x2="1.27" y2="6.985" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="-1.27" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0" y1="13.335" x2="0" y2="7.62" width="0.762" layer="21"/>
<wire x1="1.27" y1="4.445" x2="3.81" y2="4.445" width="0.1524" layer="21"/>
<wire x1="3.81" y1="4.445" x2="3.81" y2="6.985" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="1.27" y2="6.985" width="0.1524" layer="21"/>
<wire x1="2.54" y1="13.335" x2="2.54" y2="7.62" width="0.762" layer="21"/>
<pad name="2" x="0" y="2.54" drill="1.016" shape="octagon"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" shape="octagon"/>
<pad name="1" x="0" y="0" drill="1.016" shape="square"/>
<pad name="3" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-1.905" y="2.54" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="5.715" y="2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.381" y1="6.985" x2="0.381" y2="7.493" layer="21"/>
<rectangle x1="2.159" y1="6.985" x2="2.921" y2="7.493" layer="21"/>
<rectangle x1="-0.381" y1="3.429" x2="0.381" y2="4.445" layer="21"/>
<rectangle x1="2.159" y1="3.429" x2="2.921" y2="4.445" layer="21"/>
<rectangle x1="-0.381" y1="0.889" x2="0.381" y2="1.651" layer="21"/>
<rectangle x1="-0.381" y1="1.651" x2="0.381" y2="3.429" layer="51"/>
<rectangle x1="2.159" y1="1.651" x2="2.921" y2="3.429" layer="51"/>
<rectangle x1="2.159" y1="0.889" x2="2.921" y2="1.651" layer="21"/>
</package>
<package name="2X02M" urn="urn:adsk.eagle:footprint:7524025/2" library_version="7">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2</description>
<wire x1="3.25" y1="3.25" x2="3.25" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="3.25" y1="-1.25" x2="-1.25" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="-1.25" y1="-1.25" x2="-1.25" y2="3.25" width="0.2032" layer="21"/>
<wire x1="-1.25" y1="3.25" x2="3.25" y2="3.25" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.9144" shape="square"/>
<pad name="2" x="0" y="2" drill="0.9144"/>
<pad name="3" x="2" y="0" drill="0.9144"/>
<pad name="4" x="2" y="2" drill="0.9144"/>
<text x="-2" y="-1" size="1.016" layer="25" ratio="14" rot="R90">&gt;NAME</text>
<text x="5" y="-1" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.25" y1="1.75" x2="0.25" y2="2.25" layer="51"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="51"/>
<rectangle x1="1.75" y1="-0.25" x2="2.25" y2="0.25" layer="51"/>
<rectangle x1="1.75" y1="1.75" x2="2.25" y2="2.25" layer="51"/>
</package>
<package name="2X02SMD" urn="urn:adsk.eagle:footprint:7524085/2" library_version="7">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2.54 SMD</description>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<smd name="1" x="0" y="-1.27" dx="3.302" dy="1.016" layer="1" rot="R270"/>
<smd name="2" x="0" y="3.81" dx="3.302" dy="1.016" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="-1.27" dx="3.302" dy="1.016" layer="1" rot="R270"/>
<smd name="4" x="2.54" y="3.81" dx="3.302" dy="1.016" layer="1" rot="R270"/>
<text x="-1.3462" y="4.3688" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
<package name="2X02SMD/90" urn="urn:adsk.eagle:footprint:7524145/2" library_version="7">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2.54 90°</description>
<wire x1="-1.27" y1="6.985" x2="1.27" y2="6.985" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="9.525" width="0.1524" layer="21"/>
<wire x1="1.27" y1="9.525" x2="-1.27" y2="9.525" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="9.525" x2="-1.27" y2="6.985" width="0.1524" layer="21"/>
<wire x1="0" y1="17.4625" x2="0" y2="10.16" width="0.762" layer="21"/>
<wire x1="0" y1="6.35" x2="0" y2="0" width="0.4064" layer="21"/>
<wire x1="2.54" y1="17.4625" x2="2.54" y2="10.16" width="0.762" layer="21"/>
<wire x1="3.81" y1="9.525" x2="1.27" y2="9.525" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="9.525" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="3.81" y2="6.985" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="0" width="0.4064" layer="21"/>
<smd name="2" x="0" y="3.81" dx="1.778" dy="1.016" layer="1" rot="R90"/>
<smd name="1" x="0" y="0" dx="1.778" dy="1.016" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="0" dx="1.778" dy="1.016" layer="1" rot="R90"/>
<smd name="4" x="2.54" y="3.81" dx="1.778" dy="1.016" layer="1" rot="R90"/>
<text x="-1.905" y="5.08" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="5.715" y="5.08" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.381" y1="9.525" x2="0.381" y2="10.033" layer="21"/>
<rectangle x1="-0.381" y1="5.969" x2="0.381" y2="6.985" layer="21"/>
<rectangle x1="2.159" y1="9.525" x2="2.921" y2="10.033" layer="21"/>
<rectangle x1="2.159" y1="5.969" x2="2.921" y2="6.985" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="HDRV4W64P254_2X2_508X508X838C" urn="urn:adsk.eagle:package:7524206/3" type="model" library_version="7">
<description>Double-row, 4-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) pitch, 5.84 mm mating length, 5.08 X 5.08 X 8.38 mm body
&lt;p&gt;Double-row (2X2), 4-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 5.08 X 5.08 X 8.38 mm, pin pattern - zigzag from bottom left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="2X02"/>
</packageinstances>
</package3d>
<package3d name="HDRRA4W64P254_2X2_508X254X556C" urn="urn:adsk.eagle:package:7524267/3" type="model" library_version="7">
<description>Double-row, 4-pin Pin Header (Male) Right Angle, 2.54 mm (0.10 in) pitch, 5.84 mm mating length, 5.08 X 2.54 X 5.56 mm body
&lt;p&gt;Double-row (2X2), 4-pin Pin Header (Male) Right Angle package with 2.54 mm (0.10 in) pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with body size 5.08 X 2.54 X 5.56 mm, pin pattern - zigzag from bottom left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="2X02/90"/>
</packageinstances>
</package3d>
<package3d name="HDRV4W50P200_2X2_400X400X590C" urn="urn:adsk.eagle:package:7524387/3" type="model" library_version="7">
<description>Double-row, 4-pin Pin Header (Male) Straight, 2.00 mm (0.08 in) pitch, 3.90 mm mating length, 4.00 X 4.00 X 5.90 mm body
&lt;p&gt;Double-row (2X2), 4-pin Pin Header (Male) Straight package with 2.00 mm (0.08 in) pitch, 0.50 mm lead width, 2.80 mm tail length and 3.90 mm mating length with overall size 4.00 X 4.00 X 5.90 mm, pin pattern - zigzag from bottom left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="2X02M"/>
</packageinstances>
</package3d>
<package3d name="2X02SMD" urn="urn:adsk.eagle:package:7524447/2" type="box" library_version="7">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2.54 SMD</description>
<packageinstances>
<packageinstance name="2X02SMD"/>
</packageinstances>
</package3d>
<package3d name="2X02SMD/90" urn="urn:adsk.eagle:package:7524507/2" type="box" library_version="7">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2.54 90°</description>
<packageinstances>
<packageinstance name="2X02SMD/90"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PINH2X02" urn="urn:adsk.eagle:symbol:7523810/1" library_version="7">
<wire x1="-8.89" y1="-2.54" x2="6.35" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-2.54" x2="6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="6.35" y1="5.08" x2="-8.89" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-8.89" y1="5.08" x2="-8.89" y2="-2.54" width="0.4064" layer="94"/>
<text x="-8.89" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-8.89" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-5.08" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="2.54" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-2X02" urn="urn:adsk.eagle:component:7524565/3" prefix="JP" uservalue="yes" library_version="7">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X02" x="0" y="0"/>
</gates>
<devices>
<device name="_2.54" package="2X02">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7524206/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2.54-90°" package="2X02/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7524267/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2.00" package="2X02M">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7524387/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2.54-SMD" package="2X02SMD">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7524447/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2.54-SMD-90°" package="2X02SMD/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7524507/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ajisaka-manufacturing" urn="urn:adsk.eagle:library:8516697">
<packages>
<package name="LOGO-8MM" urn="urn:adsk.eagle:footprint:8516698/1" library_version="2">
<wire x1="-2.24405625" y1="-2.045390625" x2="-2.25195" y2="-2.049634375" width="0.1" layer="21"/>
<wire x1="-2.25195" y1="-2.049634375" x2="-2.325459375" y2="-2.072334375" width="0.1" layer="21"/>
<wire x1="-2.325459375" y1="-2.072334375" x2="-2.377990625" y2="-2.072334375" width="0.1" layer="21"/>
<wire x1="-2.377990625" y1="-2.072334375" x2="-2.45405" y2="-2.04645" width="0.1" layer="21"/>
<wire x1="-2.45405" y1="-2.04645" x2="-2.46228125" y2="-2.041540625" width="0.1" layer="21"/>
<wire x1="-2.46228125" y1="-2.041540625" x2="-2.46630625" y2="-2.039184375" width="0.1" layer="21"/>
<wire x1="-2.46630625" y1="-2.039184375" x2="-2.50306875" y2="-2.01075" width="0.1" layer="21"/>
<wire x1="-2.50306875" y1="-2.01075" x2="-2.516503125" y2="-1.997315625" width="0.1" layer="21"/>
<wire x1="-2.516503125" y1="-1.997315625" x2="-2.553775" y2="-1.937125" width="0.1" layer="21"/>
<wire x1="-2.553775" y1="-1.937125" x2="-2.5710375" y2="-1.87235625" width="0.1" layer="21"/>
<wire x1="-2.5710375" y1="-1.87235625" x2="-2.5710375" y2="-1.829190625" width="0.1" layer="21"/>
<wire x1="-2.5710375" y1="-1.829190625" x2="-2.553775" y2="-1.7646375" width="0.1" layer="21"/>
<wire x1="-2.553775" y1="-1.7646375" x2="-2.516503125" y2="-1.704328125" width="0.1" layer="21"/>
<wire x1="-2.516503125" y1="-1.704328125" x2="-2.50636875" y2="-1.6943125" width="0.1" layer="21"/>
<wire x1="-2.50636875" y1="-1.6943125" x2="-2.496371875" y2="-1.684278125" width="0.1" layer="21"/>
<wire x1="-2.496371875" y1="-1.684278125" x2="-2.4360625" y2="-1.646884375" width="0.1" layer="21"/>
<wire x1="-2.4360625" y1="-1.646884375" x2="-2.371490625" y2="-1.62964375" width="0.1" layer="21"/>
<wire x1="-2.371490625" y1="-1.62964375" x2="-2.32834375" y2="-1.62964375" width="0.1" layer="21"/>
<wire x1="-2.32834375" y1="-1.62964375" x2="-2.26355625" y2="-1.646884375" width="0.1" layer="21"/>
<wire x1="-2.26355625" y1="-1.646884375" x2="-2.203365625" y2="-1.684278125" width="0.1" layer="21"/>
<wire x1="-2.203365625" y1="-1.684278125" x2="-2.19323125" y2="-1.6943125" width="0.1" layer="21"/>
<wire x1="-2.19323125" y1="-1.6943125" x2="-2.183215625" y2="-1.704328125" width="0.1" layer="21"/>
<wire x1="-2.183215625" y1="-1.704328125" x2="-2.14570625" y2="-1.7646375" width="0.1" layer="21"/>
<wire x1="-2.14570625" y1="-1.7646375" x2="-2.128365625" y2="-1.829190625" width="0.1" layer="21"/>
<wire x1="-2.128365625" y1="-1.829190625" x2="-2.128365625" y2="-1.87235625" width="0.1" layer="21"/>
<wire x1="-2.128365625" y1="-1.87235625" x2="-2.14570625" y2="-1.937125" width="0.1" layer="21"/>
<wire x1="-2.14570625" y1="-1.937125" x2="-2.183215625" y2="-1.997315625" width="0.1" layer="21"/>
<wire x1="-2.183215625" y1="-1.997315625" x2="-2.19323125" y2="-2.00745" width="0.1" layer="21"/>
<wire x1="-2.19323125" y1="-2.00745" x2="-2.1970625" y2="-2.01118125" width="0.1" layer="21"/>
<wire x1="-2.1970625" y1="-2.01118125" x2="-2.23948125" y2="-2.04281875" width="0.1" layer="21"/>
<wire x1="-2.065384375" y1="0.122528125" x2="-2.07070625" y2="0.12783125" width="0.1" layer="21"/>
<wire x1="-2.07070625" y1="0.12783125" x2="-2.11173125" y2="0.190065625" width="0.1" layer="21"/>
<wire x1="-2.11173125" y1="0.190065625" x2="-2.1145" y2="0.196978125" width="0.1" layer="21"/>
<wire x1="-2.1145" y1="0.196978125" x2="-2.117034375" y2="0.2034" width="0.1" layer="21"/>
<wire x1="-2.117034375" y1="0.2034" x2="-2.1299375" y2="0.26029375" width="0.1" layer="21"/>
<wire x1="-2.1299375" y1="0.26029375" x2="-2.1299375" y2="0.299925" width="0.1" layer="21"/>
<wire x1="-2.1299375" y1="0.299925" x2="-2.11406875" y2="0.362021875" width="0.1" layer="21"/>
<wire x1="-2.11406875" y1="0.362021875" x2="-2.079346875" y2="0.420621875" width="0.1" layer="21"/>
<wire x1="-2.079346875" y1="0.420621875" x2="-2.0699625" y2="0.430540625" width="0.1" layer="21"/>
<wire x1="-2.0699625" y1="0.430540625" x2="-2.069646875" y2="0.430953125" width="0.1" layer="21"/>
<wire x1="-2.069646875" y1="0.430953125" x2="-2.065815625" y2="0.43535" width="0.1" layer="21"/>
<wire x1="-2.065815625" y1="0.43535" x2="-2.065384375" y2="0.435646875" width="0.1" layer="21"/>
<wire x1="-2.065384375" y1="0.435646875" x2="-2.05536875" y2="0.445759375" width="0.1" layer="21"/>
<wire x1="-2.05536875" y1="0.445759375" x2="-1.9949625" y2="0.48315" width="0.1" layer="21"/>
<wire x1="-1.9949625" y1="0.48315" x2="-1.9303875" y2="0.50053125" width="0.1" layer="21"/>
<wire x1="-1.9303875" y1="0.50053125" x2="-1.88724375" y2="0.50053125" width="0.1" layer="21"/>
<wire x1="-1.88724375" y1="0.50053125" x2="-1.822575" y2="0.48315" width="0.1" layer="21"/>
<wire x1="-1.822575" y1="0.48315" x2="-1.76238125" y2="0.445759375" width="0.1" layer="21"/>
<wire x1="-1.76238125" y1="0.445759375" x2="-1.752228125" y2="0.435646875" width="0.1" layer="21"/>
<wire x1="-1.752228125" y1="0.435646875" x2="-1.7422125" y2="0.42563125" width="0.1" layer="21"/>
<wire x1="-1.7422125" y1="0.42563125" x2="-1.704721875" y2="0.36531875" width="0.1" layer="21"/>
<wire x1="-1.704721875" y1="0.36531875" x2="-1.6873625" y2="0.30076875" width="0.1" layer="21"/>
<wire x1="-1.6873625" y1="0.30076875" x2="-1.6873625" y2="0.257621875" width="0.1" layer="21"/>
<wire x1="-1.6873625" y1="0.257621875" x2="-1.704721875" y2="0.19305" width="0.1" layer="21"/>
<wire x1="-1.704721875" y1="0.19305" x2="-1.7422125" y2="0.132640625" width="0.1" layer="21"/>
<wire x1="-1.7422125" y1="0.132640625" x2="-1.752228125" y2="0.122528125" width="0.1" layer="21"/>
<wire x1="-1.752228125" y1="0.122528125" x2="-1.76238125" y2="0.1125125" width="0.1" layer="21"/>
<wire x1="-1.76238125" y1="0.1125125" x2="-1.822575" y2="0.0752" width="0.1" layer="21"/>
<wire x1="-1.822575" y1="0.0752" x2="-1.893409375" y2="0.057740625" width="0.1" layer="21"/>
<wire x1="-1.893409375" y1="0.057740625" x2="-1.908765625" y2="0.057621875" width="0.1" layer="21"/>
<wire x1="-1.908765625" y1="0.057621875" x2="-1.924084375" y2="0.057740625" width="0.1" layer="21"/>
<wire x1="-1.924084375" y1="0.057740625" x2="-1.9949625" y2="0.0752" width="0.1" layer="21"/>
<wire x1="-1.9949625" y1="0.0752" x2="-2.05536875" y2="0.1125125" width="0.1" layer="21"/>
<wire x1="-0.208715625" y1="-1.039896875" x2="-0.2140375" y2="-1.037246875" width="0.1" layer="21"/>
<wire x1="-0.2140375" y1="-1.037246875" x2="-0.26154375" y2="-1.00281875" width="0.1" layer="21"/>
<wire x1="-0.26154375" y1="-1.00281875" x2="-0.265709375" y2="-0.99879375" width="0.1" layer="21"/>
<wire x1="-0.265709375" y1="-0.99879375" x2="-0.275840625" y2="-0.988659375" width="0.1" layer="21"/>
<wire x1="-0.275840625" y1="-0.988659375" x2="-0.31335" y2="-0.928253125" width="0.1" layer="21"/>
<wire x1="-0.31335" y1="-0.928253125" x2="-0.33059375" y2="-0.8636625" width="0.1" layer="21"/>
<wire x1="-0.33059375" y1="-0.8636625" x2="-0.33059375" y2="-0.83385" width="0.1" layer="21"/>
<wire x1="-0.33059375" y1="-0.83385" x2="-0.328571875" y2="-0.811559375" width="0.1" layer="21"/>
<wire x1="-0.328571875" y1="-0.811559375" x2="-0.328159375" y2="-0.808928125" width="0.1" layer="21"/>
<wire x1="-0.328159375" y1="-0.808928125" x2="-0.326665625" y2="-0.7983625" width="0.1" layer="21"/>
<wire x1="-0.326665625" y1="-0.7983625" x2="-0.310346875" y2="-0.7494625" width="0.1" layer="21"/>
<wire x1="-0.310346875" y1="-0.7494625" x2="-0.28403125" y2="-0.70608125" width="0.1" layer="21"/>
<wire x1="-0.28403125" y1="-0.70608125" x2="-0.27743125" y2="-0.698421875" width="0.1" layer="21"/>
<wire x1="-0.27743125" y1="-0.698421875" x2="-0.27646875" y2="-0.69724375" width="0.1" layer="21"/>
<wire x1="-0.27646875" y1="-0.69724375" x2="-0.2667875" y2="-0.68658125" width="0.1" layer="21"/>
<wire x1="-0.2667875" y1="-0.68658125" x2="-0.265709375" y2="-0.6856375" width="0.1" layer="21"/>
<wire x1="-0.265709375" y1="-0.6856375" x2="-0.255690625" y2="-0.675621875" width="0.1" layer="21"/>
<wire x1="-0.255690625" y1="-0.675621875" x2="-0.19538125" y2="-0.63823125" width="0.1" layer="21"/>
<wire x1="-0.19538125" y1="-0.63823125" x2="-0.1307125" y2="-0.620965625" width="0.1" layer="21"/>
<wire x1="-0.1307125" y1="-0.620965625" x2="-0.087565625" y2="-0.620965625" width="0.1" layer="21"/>
<wire x1="-0.087565625" y1="-0.620965625" x2="-0.02299375" y2="-0.63823125" width="0.1" layer="21"/>
<wire x1="-0.02299375" y1="-0.63823125" x2="0.037315625" y2="-0.675621875" width="0.1" layer="21"/>
<wire x1="0.037315625" y1="-0.675621875" x2="0.047428125" y2="-0.6856375" width="0.1" layer="21"/>
<wire x1="0.047428125" y1="-0.6856375" x2="0.048390625" y2="-0.68658125" width="0.1" layer="21"/>
<wire x1="0.048390625" y1="-0.68658125" x2="0.057975" y2="-0.69724375" width="0.1" layer="21"/>
<wire x1="0.057975" y1="-0.69724375" x2="0.0589375" y2="-0.698421875" width="0.1" layer="21"/>
<wire x1="0.0589375" y1="-0.698421875" x2="0.0672625" y2="-0.708103125" width="0.1" layer="21"/>
<wire x1="0.0672625" y1="-0.708103125" x2="0.0979375" y2="-0.764153125" width="0.1" layer="21"/>
<wire x1="0.0979375" y1="-0.764153125" x2="0.1121" y2="-0.822675" width="0.1" layer="21"/>
<wire x1="0.1121" y1="-0.822675" x2="0.1121" y2="-0.8544125" width="0.1" layer="21"/>
<wire x1="0.1121" y1="-0.8544125" x2="0.107521875" y2="-0.88734375" width="0.1" layer="21"/>
<wire x1="0.107521875" y1="-0.88734375" x2="0.106678125" y2="-0.891271875" width="0.1" layer="21"/>
<wire x1="0.106678125" y1="-0.891271875" x2="0.104321875" y2="-0.901603125" width="0.1" layer="21"/>
<wire x1="0.104321875" y1="-0.901603125" x2="0.084309375" y2="-0.949559375" width="0.1" layer="21"/>
<wire x1="0.084309375" y1="-0.949559375" x2="0.054578125" y2="-0.991428125" width="0.1" layer="21"/>
<wire x1="0.054578125" y1="-0.991428125" x2="0.047428125" y2="-0.99879375" width="0.1" layer="21"/>
<wire x1="0.047428125" y1="-0.99879375" x2="0.037315625" y2="-1.008790625" width="0.1" layer="21"/>
<wire x1="0.037315625" y1="-1.008790625" x2="-0.02299375" y2="-1.0462" width="0.1" layer="21"/>
<wire x1="-0.02299375" y1="-1.0462" x2="-0.087565625" y2="-1.063678125" width="0.1" layer="21"/>
<wire x1="-0.087565625" y1="-1.063678125" x2="-0.13041875" y2="-1.063678125" width="0.1" layer="21"/>
<wire x1="-0.13041875" y1="-1.063678125" x2="-0.18750625" y2="-1.049384375" width="0.1" layer="21"/>
<wire x1="-0.18750625" y1="-1.049384375" x2="-0.194028125" y2="-1.0467125" width="0.1" layer="21"/>
<wire x1="-0.194028125" y1="-1.0467125" x2="-0.1952625" y2="-1.0462" width="0.1" layer="21"/>
<wire x1="-0.1952625" y1="-1.0462" x2="-0.2075375" y2="-1.040428125" width="0.1" layer="21"/>
<wire x1="1.64585" y1="2.88311875" x2="1.64903125" y2="2.8799375" width="0.1" layer="21"/>
<wire x1="1.64903125" y1="2.8799375" x2="1.6769375" y2="2.843803125" width="0.1" layer="21"/>
<wire x1="1.6769375" y1="2.843803125" x2="1.679275" y2="2.839759375" width="0.1" layer="21"/>
<wire x1="1.679275" y1="2.839759375" x2="1.684303125" y2="2.83155" width="0.1" layer="21"/>
<wire x1="1.684303125" y1="2.83155" x2="1.710696875" y2="2.752915625" width="0.1" layer="21"/>
<wire x1="1.710696875" y1="2.752915625" x2="1.710696875" y2="2.7107125" width="0.1" layer="21"/>
<wire x1="1.710696875" y1="2.7107125" x2="1.701446875" y2="2.66279375" width="0.1" layer="21"/>
<wire x1="1.701446875" y1="2.66279375" x2="1.68066875" y2="2.615053125" width="0.1" layer="21"/>
<wire x1="1.68066875" y1="2.615053125" x2="1.675034375" y2="2.6063125" width="0.1" layer="21"/>
<wire x1="1.675034375" y1="2.6063125" x2="1.672890625" y2="2.6031125" width="0.1" layer="21"/>
<wire x1="1.672890625" y1="2.6031125" x2="1.6486" y2="2.57286875" width="0.1" layer="21"/>
<wire x1="1.6486" y1="2.57286875" x2="1.64585" y2="2.569984375" width="0.1" layer="21"/>
<wire x1="1.64585" y1="2.569984375" x2="1.635696875" y2="2.559965625" width="0.1" layer="21"/>
<wire x1="1.635696875" y1="2.559965625" x2="1.57550625" y2="2.522675" width="0.1" layer="21"/>
<wire x1="1.57550625" y1="2.522675" x2="1.504884375" y2="2.50519375" width="0.1" layer="21"/>
<wire x1="1.504884375" y1="2.50519375" x2="1.489528125" y2="2.505078125" width="0.1" layer="21"/>
<wire x1="1.489528125" y1="2.505078125" x2="1.474090625" y2="2.50519375" width="0.1" layer="21"/>
<wire x1="1.474090625" y1="2.50519375" x2="1.403215625" y2="2.522675" width="0.1" layer="21"/>
<wire x1="1.403215625" y1="2.522675" x2="1.342925" y2="2.559965625" width="0.1" layer="21"/>
<wire x1="1.342925" y1="2.559965625" x2="1.332909375" y2="2.569984375" width="0.1" layer="21"/>
<wire x1="1.332909375" y1="2.569984375" x2="1.322796875" y2="2.580096875" width="0.1" layer="21"/>
<wire x1="1.322796875" y1="2.580096875" x2="1.2852875" y2="2.64050625" width="0.1" layer="21"/>
<wire x1="1.2852875" y1="2.64050625" x2="1.268025" y2="2.705078125" width="0.1" layer="21"/>
<wire x1="1.268025" y1="2.705078125" x2="1.268025" y2="2.748221875" width="0.1" layer="21"/>
<wire x1="1.268025" y1="2.748221875" x2="1.2852875" y2="2.8128125" width="0.1" layer="21"/>
<wire x1="1.2852875" y1="2.8128125" x2="1.322796875" y2="2.873125" width="0.1" layer="21"/>
<wire x1="1.322796875" y1="2.873125" x2="1.332909375" y2="2.88311875" width="0.1" layer="21"/>
<wire x1="1.332909375" y1="2.88311875" x2="1.342925" y2="2.893134375" width="0.1" layer="21"/>
<wire x1="1.342925" y1="2.893134375" x2="1.403215625" y2="2.93064375" width="0.1" layer="21"/>
<wire x1="1.403215625" y1="2.93064375" x2="1.467884375" y2="2.948025" width="0.1" layer="21"/>
<wire x1="1.467884375" y1="2.948025" x2="1.51105" y2="2.948025" width="0.1" layer="21"/>
<wire x1="1.51105" y1="2.948025" x2="1.575603125" y2="2.93064375" width="0.1" layer="21"/>
<wire x1="1.575603125" y1="2.93064375" x2="1.635696875" y2="2.893134375" width="0.1" layer="21"/>
<wire x1="1.999559375" y1="-0.6856375" x2="2.00254375" y2="-0.688640625" width="0.1" layer="21"/>
<wire x1="2.00254375" y1="-0.688640625" x2="2.0286625" y2="-0.72185" width="0.1" layer="21"/>
<wire x1="2.0286625" y1="-0.72185" x2="2.030884375" y2="-0.725484375" width="0.1" layer="21"/>
<wire x1="2.030884375" y1="-0.725484375" x2="2.03620625" y2="-0.733909375" width="0.1" layer="21"/>
<wire x1="2.03620625" y1="-0.733909375" x2="2.064328125" y2="-0.83139375" width="0.1" layer="21"/>
<wire x1="2.064328125" y1="-0.83139375" x2="2.064446875" y2="-0.8421375" width="0.1" layer="21"/>
<wire x1="2.064446875" y1="-0.8421375" x2="2.064328125" y2="-0.85291875" width="0.1" layer="21"/>
<wire x1="2.064328125" y1="-0.85291875" x2="2.03620625" y2="-0.950403125" width="0.1" layer="21"/>
<wire x1="2.03620625" y1="-0.950403125" x2="2.030884375" y2="-0.958928125" width="0.1" layer="21"/>
<wire x1="2.030884375" y1="-0.958928125" x2="2.0286625" y2="-0.962540625" width="0.1" layer="21"/>
<wire x1="2.0286625" y1="-0.962540625" x2="2.00254375" y2="-0.995671875" width="0.1" layer="21"/>
<wire x1="2.00254375" y1="-0.995671875" x2="1.999559375" y2="-0.99879375" width="0.1" layer="21"/>
<wire x1="1.999559375" y1="-0.99879375" x2="1.989425" y2="-1.008790625" width="0.1" layer="21"/>
<wire x1="1.989425" y1="-1.008790625" x2="1.92901875" y2="-1.0462" width="0.1" layer="21"/>
<wire x1="1.92901875" y1="-1.0462" x2="1.864465625" y2="-1.063678125" width="0.1" layer="21"/>
<wire x1="1.864465625" y1="-1.063678125" x2="1.8213" y2="-1.063678125" width="0.1" layer="21"/>
<wire x1="1.8213" y1="-1.063678125" x2="1.75675" y2="-1.0462" width="0.1" layer="21"/>
<wire x1="1.75675" y1="-1.0462" x2="1.6964375" y2="-1.008790625" width="0.1" layer="21"/>
<wire x1="1.6964375" y1="-1.008790625" x2="1.686421875" y2="-0.99879375" width="0.1" layer="21"/>
<wire x1="1.686421875" y1="-0.99879375" x2="1.676309375" y2="-0.988659375" width="0.1" layer="21"/>
<wire x1="1.676309375" y1="-0.988659375" x2="1.6388" y2="-0.928253125" width="0.1" layer="21"/>
<wire x1="1.6388" y1="-0.928253125" x2="1.6215375" y2="-0.8636625" width="0.1" layer="21"/>
<wire x1="1.6215375" y1="-0.8636625" x2="1.6215375" y2="-0.820515625" width="0.1" layer="21"/>
<wire x1="1.6215375" y1="-0.820515625" x2="1.6388" y2="-0.75594375" width="0.1" layer="21"/>
<wire x1="1.6388" y1="-0.75594375" x2="1.676309375" y2="-0.695634375" width="0.1" layer="21"/>
<wire x1="1.676309375" y1="-0.695634375" x2="1.686421875" y2="-0.6856375" width="0.1" layer="21"/>
<wire x1="1.686421875" y1="-0.6856375" x2="1.6964375" y2="-0.675621875" width="0.1" layer="21"/>
<wire x1="1.6964375" y1="-0.675621875" x2="1.75675" y2="-0.63823125" width="0.1" layer="21"/>
<wire x1="1.75675" y1="-0.63823125" x2="1.8213" y2="-0.620965625" width="0.1" layer="21"/>
<wire x1="1.8213" y1="-0.620965625" x2="1.864465625" y2="-0.620965625" width="0.1" layer="21"/>
<wire x1="1.864465625" y1="-0.620965625" x2="1.92901875" y2="-0.63823125" width="0.1" layer="21"/>
<wire x1="1.92901875" y1="-0.63823125" x2="1.989425" y2="-0.675621875" width="0.1" layer="21"/>
<wire x1="-3.8173" y1="-2.0990625" x2="-3.87016875" y2="-1.99625625" width="0.1" layer="21"/>
<wire x1="-3.87016875" y1="-1.99625625" x2="-4.0047125" y2="-1.675440625" width="0.1" layer="21"/>
<wire x1="-4.0047125" y1="-1.675440625" x2="-4.142809375" y2="-1.227621875" width="0.1" layer="21"/>
<wire x1="-4.142809375" y1="-1.227621875" x2="-4.228040625" y2="-0.75914375" width="0.1" layer="21"/>
<wire x1="-4.228040625" y1="-0.75914375" x2="-4.25724375" y2="-0.39486875" width="0.1" layer="21"/>
<wire x1="-4.25724375" y1="-0.39486875" x2="-4.25724375" y2="-0.17124375" width="0.1" layer="21"/>
<wire x1="-4.25724375" y1="-0.17124375" x2="-4.2366625" y2="0.135509375" width="0.1" layer="21"/>
<wire x1="-4.2366625" y1="0.135509375" x2="-4.176059375" y2="0.53271875" width="0.1" layer="21"/>
<wire x1="-4.176059375" y1="0.53271875" x2="-4.077490625" y2="0.915965625" width="0.1" layer="21"/>
<wire x1="-4.077490625" y1="0.915965625" x2="-3.942928125" y2="1.283540625" width="0.1" layer="21"/>
<wire x1="-3.942928125" y1="1.283540625" x2="-3.7744875" y2="1.633225" width="0.1" layer="21"/>
<wire x1="-3.7744875" y1="1.633225" x2="-3.574175" y2="1.962975" width="0.1" layer="21"/>
<wire x1="-3.574175" y1="1.962975" x2="-3.343815625" y2="2.2708875" width="0.1" layer="21"/>
<wire x1="-3.343815625" y1="2.2708875" x2="-3.085665625" y2="2.554940625" width="0.1" layer="21"/>
<wire x1="-3.085665625" y1="2.554940625" x2="-2.801615625" y2="2.813109375" width="0.1" layer="21"/>
<wire x1="-2.801615625" y1="2.813109375" x2="-2.493703125" y2="3.0434875" width="0.1" layer="21"/>
<wire x1="-2.493703125" y1="3.0434875" x2="-2.16393125" y2="3.2437625" width="0.1" layer="21"/>
<wire x1="-2.16393125" y1="3.2437625" x2="-1.814246875" y2="3.412221875" width="0.1" layer="21"/>
<wire x1="-1.814246875" y1="3.412221875" x2="-1.44676875" y2="3.54680625" width="0.1" layer="21"/>
<wire x1="-1.44676875" y1="3.54680625" x2="-1.063425" y2="3.64535" width="0.1" layer="21"/>
<wire x1="-1.063425" y1="3.64535" x2="-0.666334375" y2="3.705975" width="0.1" layer="21"/>
<wire x1="-0.666334375" y1="3.705975" x2="-0.35958125" y2="3.72655625" width="0.1" layer="21"/>
<wire x1="-0.35958125" y1="3.72655625" x2="-0.082971875" y2="3.72655625" width="0.1" layer="21"/>
<wire x1="-0.082971875" y1="3.72655625" x2="0.43994375" y2="3.66603125" width="0.1" layer="21"/>
<wire x1="0.43994375" y1="3.66603125" x2="1.097975" y2="3.4912875" width="0.1" layer="21"/>
<wire x1="1.097975" y1="3.4912875" x2="1.7062375" y2="3.212459375" width="0.1" layer="21"/>
<wire x1="1.7062375" y1="3.212459375" x2="2.12615" y2="2.943625" width="0.1" layer="21"/>
<wire x1="2.12615" y1="2.943625" x2="2.25464375" y2="2.839759375" width="0.1" layer="21"/>
<wire x1="2.25464375" y1="2.839759375" x2="1.891509375" y2="2.839759375" width="0.1" layer="21"/>
<wire x1="1.891509375" y1="2.839759375" x2="1.8866375" y2="2.857215625" width="0.1" layer="21"/>
<wire x1="1.8866375" y1="2.857215625" x2="1.849421875" y2="2.938421875" width="0.1" layer="21"/>
<wire x1="1.849421875" y1="2.938421875" x2="1.797225" y2="3.009571875" width="0.1" layer="21"/>
<wire x1="1.797225" y1="3.009571875" x2="1.784871875" y2="3.0219625" width="0.1" layer="21"/>
<wire x1="1.784871875" y1="3.0219625" x2="1.770809375" y2="3.036125" width="0.1" layer="21"/>
<wire x1="1.770809375" y1="3.036125" x2="1.7230875" y2="3.072984375" width="0.1" layer="21"/>
<wire x1="1.7230875" y1="3.072984375" x2="1.651996875" y2="3.11155625" width="0.1" layer="21"/>
<wire x1="1.651996875" y1="3.11155625" x2="1.57348125" y2="3.1360625" width="0.1" layer="21"/>
<wire x1="1.57348125" y1="3.1360625" x2="1.510521875" y2="3.1444875" width="0.1" layer="21"/>
<wire x1="1.510521875" y1="3.1444875" x2="1.468434375" y2="3.1444875" width="0.1" layer="21"/>
<wire x1="1.468434375" y1="3.1444875" x2="1.4052375" y2="3.1360625" width="0.1" layer="21"/>
<wire x1="1.4052375" y1="3.1360625" x2="1.32660625" y2="3.11155625" width="0.1" layer="21"/>
<wire x1="1.32660625" y1="3.11155625" x2="1.255553125" y2="3.072984375" width="0.1" layer="21"/>
<wire x1="1.255553125" y1="3.072984375" x2="1.2078125" y2="3.036125" width="0.1" layer="21"/>
<wire x1="1.2078125" y1="3.036125" x2="1.19385" y2="3.0219625" width="0.1" layer="21"/>
<wire x1="1.19385" y1="3.0219625" x2="1.17980625" y2="3.007884375" width="0.1" layer="21"/>
<wire x1="1.17980625" y1="3.007884375" x2="1.142925" y2="2.960259375" width="0.1" layer="21"/>
<wire x1="1.142925" y1="2.960259375" x2="1.104475" y2="2.8892875" width="0.1" layer="21"/>
<wire x1="1.104475" y1="2.8892875" x2="1.0800625" y2="2.810771875" width="0.1" layer="21"/>
<wire x1="1.0800625" y1="2.810771875" x2="1.071659375" y2="2.7477125" width="0.1" layer="21"/>
<wire x1="1.071659375" y1="2.7477125" x2="1.071659375" y2="2.705628125" width="0.1" layer="21"/>
<wire x1="1.071659375" y1="2.705628125" x2="1.0800625" y2="2.642528125" width="0.1" layer="21"/>
<wire x1="1.0800625" y1="2.642528125" x2="1.104475" y2="2.56399375" width="0.1" layer="21"/>
<wire x1="1.104475" y1="2.56399375" x2="1.142925" y2="2.492940625" width="0.1" layer="21"/>
<wire x1="1.142925" y1="2.492940625" x2="1.179690625" y2="2.44531875" width="0.1" layer="21"/>
<wire x1="1.179690625" y1="2.44531875" x2="1.19385" y2="2.4311375" width="0.1" layer="21"/>
<wire x1="1.19385" y1="2.4311375" x2="1.2078125" y2="2.417096875" width="0.1" layer="21"/>
<wire x1="1.2078125" y1="2.417096875" x2="1.255553125" y2="2.380215625" width="0.1" layer="21"/>
<wire x1="1.255553125" y1="2.380215625" x2="1.32660625" y2="2.341646875" width="0.1" layer="21"/>
<wire x1="1.32660625" y1="2.341646875" x2="1.4052375" y2="2.317275" width="0.1" layer="21"/>
<wire x1="1.4052375" y1="2.317275" x2="1.468434375" y2="2.30873125" width="0.1" layer="21"/>
<wire x1="1.468434375" y1="2.30873125" x2="1.510521875" y2="2.30873125" width="0.1" layer="21"/>
<wire x1="1.510521875" y1="2.30873125" x2="1.57348125" y2="2.317275" width="0.1" layer="21"/>
<wire x1="1.57348125" y1="2.317275" x2="1.651996875" y2="2.341646875" width="0.1" layer="21"/>
<wire x1="1.651996875" y1="2.341646875" x2="1.7230875" y2="2.380215625" width="0.1" layer="21"/>
<wire x1="1.7230875" y1="2.380215625" x2="1.770809375" y2="2.417096875" width="0.1" layer="21"/>
<wire x1="1.770809375" y1="2.417096875" x2="1.784871875" y2="2.4311375" width="0.1" layer="21"/>
<wire x1="1.784871875" y1="2.4311375" x2="1.7968125" y2="2.443196875" width="0.1" layer="21"/>
<wire x1="1.7968125" y1="2.443196875" x2="1.84751875" y2="2.511596875" width="0.1" layer="21"/>
<wire x1="1.84751875" y1="2.511596875" x2="1.88428125" y2="2.58958125" width="0.1" layer="21"/>
<wire x1="1.88428125" y1="2.58958125" x2="1.8893875" y2="2.6063125" width="0.1" layer="21"/>
<wire x1="1.8893875" y1="2.6063125" x2="2.5188625" y2="2.6063125" width="0.1" layer="21"/>
<wire x1="2.5188625" y1="2.6063125" x2="2.538678125" y2="2.587128125" width="0.1" layer="21"/>
<wire x1="2.538678125" y1="2.587128125" x2="2.72899375" y2="2.387875" width="0.1" layer="21"/>
<wire x1="2.72899375" y1="2.387875" x2="2.74718125" y2="2.367115625" width="0.1" layer="21"/>
<wire x1="2.74718125" y1="2.367115625" x2="1.5797875" y2="0.998978125" width="0.1" layer="21"/>
<wire x1="1.5797875" y1="0.998978125" x2="-0.394734375" y2="3.07758125" width="0.1" layer="21"/>
<wire x1="-0.394734375" y1="3.07758125" x2="-0.394734375" y2="0.331975" width="0.1" layer="21"/>
<wire x1="-0.394734375" y1="0.331975" x2="-1.770453125" y2="2.98944375" width="0.1" layer="21"/>
<wire x1="-1.770453125" y1="2.98944375" x2="-3.845128125" y2="-0.792275" width="0.1" layer="21"/>
<wire x1="-3.845128125" y1="-0.792275" x2="-3.84406875" y2="-0.792825" width="0.1" layer="21"/>
<wire x1="-3.84406875" y1="-0.792825" x2="-3.987253125" y2="-1.02008125" width="0.1" layer="21"/>
<wire x1="-3.987253125" y1="-1.02008125" x2="-3.812940625" y2="-1.02008125" width="0.1" layer="21"/>
<wire x1="2.138384375" y1="-0.54679375" x2="2.12434375" y2="-0.532653125" width="0.1" layer="21"/>
<wire x1="2.12434375" y1="-0.532653125" x2="2.076603125" y2="-0.495771875" width="0.1" layer="21"/>
<wire x1="2.076603125" y1="-0.495771875" x2="2.00553125" y2="-0.457203125" width="0.1" layer="21"/>
<wire x1="2.00553125" y1="-0.457203125" x2="1.92699375" y2="-0.4327125" width="0.1" layer="21"/>
<wire x1="1.92699375" y1="-0.4327125" x2="1.8639375" y2="-0.42426875" width="0.1" layer="21"/>
<wire x1="1.8639375" y1="-0.42426875" x2="1.82185" y2="-0.42426875" width="0.1" layer="21"/>
<wire x1="1.82185" y1="-0.42426875" x2="1.758771875" y2="-0.4327125" width="0.1" layer="21"/>
<wire x1="1.758771875" y1="-0.4327125" x2="1.68035625" y2="-0.457203125" width="0.1" layer="21"/>
<wire x1="1.68035625" y1="-0.457203125" x2="1.609284375" y2="-0.495771875" width="0.1" layer="21"/>
<wire x1="1.609284375" y1="-0.495771875" x2="1.561659375" y2="-0.532653125" width="0.1" layer="21"/>
<wire x1="1.561659375" y1="-0.532653125" x2="1.547596875" y2="-0.54679375" width="0.1" layer="21"/>
<wire x1="1.547596875" y1="-0.54679375" x2="1.53341875" y2="-0.56075625" width="0.1" layer="21"/>
<wire x1="1.53341875" y1="-0.56075625" x2="1.496559375" y2="-0.608496875" width="0.1" layer="21"/>
<wire x1="1.496559375" y1="-0.608496875" x2="1.4579875" y2="-0.679471875" width="0.1" layer="21"/>
<wire x1="1.4579875" y1="-0.679471875" x2="1.433578125" y2="-0.757965625" width="0.1" layer="21"/>
<wire x1="1.433578125" y1="-0.757965625" x2="1.425171875" y2="-0.821065625" width="0.1" layer="21"/>
<wire x1="1.425171875" y1="-0.821065625" x2="1.425171875" y2="-0.86315" width="0.1" layer="21"/>
<wire x1="1.425171875" y1="-0.86315" x2="1.433578125" y2="-0.926209375" width="0.1" layer="21"/>
<wire x1="1.433578125" y1="-0.926209375" x2="1.4579875" y2="-1.004725" width="0.1" layer="21"/>
<wire x1="1.4579875" y1="-1.004725" x2="1.496559375" y2="-1.075815625" width="0.1" layer="21"/>
<wire x1="1.496559375" y1="-1.075815625" x2="1.53341875" y2="-1.1235375" width="0.1" layer="21"/>
<wire x1="1.53341875" y1="-1.1235375" x2="1.547596875" y2="-1.1376" width="0.1" layer="21"/>
<wire x1="1.547596875" y1="-1.1376" x2="1.5615625" y2="-1.151659375" width="0.1" layer="21"/>
<wire x1="1.5615625" y1="-1.151659375" x2="1.609284375" y2="-1.188540625" width="0.1" layer="21"/>
<wire x1="1.609284375" y1="-1.188540625" x2="1.68035625" y2="-1.2271125" width="0.1" layer="21"/>
<wire x1="1.68035625" y1="-1.2271125" x2="1.758771875" y2="-1.251503125" width="0.1" layer="21"/>
<wire x1="1.758771875" y1="-1.251503125" x2="1.82185" y2="-1.260025" width="0.1" layer="21"/>
<wire x1="1.82185" y1="-1.260025" x2="1.8639375" y2="-1.260025" width="0.1" layer="21"/>
<wire x1="1.8639375" y1="-1.260025" x2="1.92699375" y2="-1.251503125" width="0.1" layer="21"/>
<wire x1="1.92699375" y1="-1.251503125" x2="2.00553125" y2="-1.2271125" width="0.1" layer="21"/>
<wire x1="2.00553125" y1="-1.2271125" x2="2.076603125" y2="-1.188540625" width="0.1" layer="21"/>
<wire x1="2.076603125" y1="-1.188540625" x2="2.12434375" y2="-1.151659375" width="0.1" layer="21"/>
<wire x1="2.12434375" y1="-1.151659375" x2="2.138384375" y2="-1.1376" width="0.1" layer="21"/>
<wire x1="2.138384375" y1="-1.1376" x2="2.150521875" y2="-1.12534375" width="0.1" layer="21"/>
<wire x1="2.150521875" y1="-1.12534375" x2="2.20199375" y2="-1.05556875" width="0.1" layer="21"/>
<wire x1="2.20199375" y1="-1.05556875" x2="2.238971875" y2="-0.975975" width="0.1" layer="21"/>
<wire x1="2.238971875" y1="-0.975975" x2="2.24398125" y2="-0.958928125" width="0.1" layer="21"/>
<wire x1="2.24398125" y1="-0.958928125" x2="3.38786875" y2="-0.958928125" width="0.1" layer="21"/>
<wire x1="3.38786875" y1="-0.958928125" x2="3.38786875" y2="-0.8421375" width="0.1" layer="21"/>
<wire x1="3.38786875" y1="-0.8421375" x2="3.394684375" y2="-0.8421375" width="0.1" layer="21"/>
<wire x1="3.394684375" y1="-0.8421375" x2="3.394684375" y2="1.360896875" width="0.1" layer="21"/>
<wire x1="3.394684375" y1="1.360896875" x2="3.43665" y2="1.267140625" width="0.1" layer="21"/>
<wire x1="3.43665" y1="1.267140625" x2="3.543425" y2="0.977021875" width="0.1" layer="21"/>
<wire x1="3.543425" y1="0.977021875" x2="3.652634375" y2="0.57501875" width="0.1" layer="21"/>
<wire x1="3.652634375" y1="0.57501875" x2="3.719759375" y2="0.157465625" width="0.1" layer="21"/>
<wire x1="3.719759375" y1="0.157465625" x2="3.74275625" y2="-0.16570625" width="0.1" layer="21"/>
<wire x1="3.74275625" y1="-0.16570625" x2="3.74275625" y2="-0.4588125" width="0.1" layer="21"/>
<wire x1="3.74275625" y1="-0.4588125" x2="3.674159375" y2="-1.01495625" width="0.1" layer="21"/>
<wire x1="3.674159375" y1="-1.01495625" x2="3.51954375" y2="-1.599596875" width="0.1" layer="21"/>
<wire x1="3.51954375" y1="-1.599596875" x2="3.476515625" y2="-1.7112625" width="0.1" layer="21"/>
<wire x1="3.476515625" y1="-1.7112625" x2="3.415871875" y2="-1.72221875" width="0.1" layer="21"/>
<wire x1="3.415871875" y1="-1.72221875" x2="2.82805" y2="-1.85358125" width="0.1" layer="21"/>
<wire x1="2.82805" y1="-1.85358125" x2="2.771159375" y2="-1.86860625" width="0.1" layer="21"/>
<wire x1="2.771159375" y1="-1.86860625" x2="2.726715625" y2="-1.8801125" width="0.1" layer="21"/>
<wire x1="2.726715625" y1="-1.8801125" x2="2.294903125" y2="-2.00829375" width="0.1" layer="21"/>
<wire x1="2.294903125" y1="-2.00829375" x2="2.252915625" y2="-2.0221375" width="0.1" layer="21"/>
<wire x1="2.252915625" y1="-2.0221375" x2="2.23194375" y2="-2.028971875" width="0.1" layer="21"/>
<wire x1="2.23194375" y1="-2.028971875" x2="2.024815625" y2="-2.100890625" width="0.1" layer="21"/>
<wire x1="2.024815625" y1="-2.100890625" x2="2.00446875" y2="-2.108334375" width="0.1" layer="21"/>
<wire x1="2.00446875" y1="-2.108334375" x2="1.99466875" y2="-2.111965625" width="0.1" layer="21"/>
<wire x1="1.99466875" y1="-2.111965625" x2="1.897596875" y2="-2.14808125" width="0.1" layer="21"/>
<wire x1="1.897596875" y1="-2.14808125" x2="1.88799375" y2="-2.1517125" width="0.1" layer="21"/>
<wire x1="1.88799375" y1="-2.1517125" x2="1.87809375" y2="-2.155228125" width="0.1" layer="21"/>
<wire x1="1.87809375" y1="-2.155228125" x2="1.780609375" y2="-2.1905" width="0.1" layer="21"/>
<wire x1="1.780609375" y1="-2.1905" x2="1.77100625" y2="-2.194015625" width="0.1" layer="21"/>
<wire x1="1.77100625" y1="-2.194015625" x2="1.751621875" y2="-2.201046875" width="0.1" layer="21"/>
<wire x1="1.751621875" y1="-2.201046875" x2="1.560165625" y2="-2.27091875" width="0.1" layer="21"/>
<wire x1="1.560165625" y1="-2.27091875" x2="1.5413125" y2="-2.27796875" width="0.1" layer="21"/>
<wire x1="1.5413125" y1="-2.27796875" x2="1.531828125" y2="-2.28126875" width="0.1" layer="21"/>
<wire x1="1.531828125" y1="-2.28126875" x2="1.438053125" y2="-2.3153625" width="0.1" layer="21"/>
<wire x1="1.438053125" y1="-2.3153625" x2="1.428884375" y2="-2.318365625" width="0.1" layer="21"/>
<wire x1="1.428884375" y1="-2.318365625" x2="1.41975" y2="-2.3212125" width="0.1" layer="21"/>
<wire x1="1.41975" y1="-2.3212125" x2="1.329059375" y2="-2.349571875" width="0.1" layer="21"/>
<wire x1="1.329059375" y1="-2.349571875" x2="1.32010625" y2="-2.352459375" width="0.1" layer="21"/>
<wire x1="1.32010625" y1="-2.352459375" x2="1.283890625" y2="-2.363634375" width="0.1" layer="21"/>
<wire x1="1.283890625" y1="-2.363634375" x2="1.1067125" y2="-2.4203125" width="0.1" layer="21"/>
<wire x1="1.1067125" y1="-2.4203125" x2="0.931871875" y2="-2.471234375" width="0.1" layer="21"/>
<wire x1="0.931871875" y1="-2.471234375" x2="0.8970125" y2="-2.479559375" width="0.1" layer="21"/>
<wire x1="0.8970125" y1="-2.479559375" x2="0.879671875" y2="-2.4841375" width="0.1" layer="21"/>
<wire x1="0.879671875" y1="-2.4841375" x2="0.7091875" y2="-2.5296375" width="0.1" layer="21"/>
<wire x1="0.7091875" y1="-2.5296375" x2="0.6924375" y2="-2.534115625" width="0.1" layer="21"/>
<wire x1="0.6924375" y1="-2.534115625" x2="0.684053125" y2="-2.536571875" width="0.1" layer="21"/>
<wire x1="0.684053125" y1="-2.536571875" x2="0.600609375" y2="-2.554875" width="0.1" layer="21"/>
<wire x1="0.600609375" y1="-2.554875" x2="0.5924" y2="-2.5568" width="0.1" layer="21"/>
<wire x1="0.5924" y1="-2.5568" x2="0.5841125" y2="-2.5584875" width="0.1" layer="21"/>
<wire x1="0.5841125" y1="-2.5584875" x2="0.5020625" y2="-2.575553125" width="0.1" layer="21"/>
<wire x1="0.5020625" y1="-2.575553125" x2="0.49395" y2="-2.5772625" width="0.1" layer="21"/>
<wire x1="0.49395" y1="-2.5772625" x2="0.47765" y2="-2.58044375" width="0.1" layer="21"/>
<wire x1="0.47765" y1="-2.58044375" x2="0.31783125" y2="-2.61359375" width="0.1" layer="21"/>
<wire x1="0.31783125" y1="-2.61359375" x2="0.30218125" y2="-2.61624375" width="0.1" layer="21"/>
<wire x1="0.30218125" y1="-2.61624375" x2="0.286409375" y2="-2.618484375" width="0.1" layer="21"/>
<wire x1="0.286409375" y1="-2.618484375" x2="0.13244375" y2="-2.641284375" width="0.1" layer="21"/>
<wire x1="0.13244375" y1="-2.641284375" x2="0.11751875" y2="-2.643521875" width="0.1" layer="21"/>
<wire x1="0.11751875" y1="-2.643521875" x2="0.031778125" y2="-2.656425" width="0.1" layer="21"/>
<wire x1="0.031778125" y1="-2.656425" x2="-0.228003125" y2="-2.679225" width="0.1" layer="21"/>
<wire x1="-0.228003125" y1="-2.679225" x2="-0.5373875" y2="-2.68199375" width="0.1" layer="21"/>
<wire x1="-0.5373875" y1="-2.68199375" x2="-0.806003125" y2="-2.65864375" width="0.1" layer="21"/>
<wire x1="-0.806003125" y1="-2.65864375" x2="-0.9744625" y2="-2.629875" width="0.1" layer="21"/>
<wire x1="-0.9744625" y1="-2.629875" x2="-1.0294125" y2="-2.61624375" width="0.1" layer="21"/>
<wire x1="-1.0294125" y1="-2.61624375" x2="-1.0625625" y2="-2.6072875" width="0.1" layer="21"/>
<wire x1="-1.0625625" y1="-2.6072875" x2="-1.20278125" y2="-2.559665625" width="0.1" layer="21"/>
<wire x1="-1.20278125" y1="-2.559665625" x2="-1.3094375" y2="-2.513221875" width="0.1" layer="21"/>
<wire x1="-1.3094375" y1="-2.513221875" x2="-1.325640625" y2="-2.50489375" width="0.1" layer="21"/>
<wire x1="-1.325640625" y1="-2.50489375" x2="-1.3406625" y2="-2.49660625" width="0.1" layer="21"/>
<wire x1="-1.3406625" y1="-2.49660625" x2="-1.39394375" y2="-2.463790625" width="0.1" layer="21"/>
<wire x1="-1.39394375" y1="-2.463790625" x2="-1.4188625" y2="-2.445978125" width="0.1" layer="21"/>
<wire x1="-1.4188625" y1="-2.445978125" x2="-1.419709375" y2="-2.445253125" width="0.1" layer="21"/>
<wire x1="-1.419709375" y1="-2.445253125" x2="-1.41898125" y2="-2.446096875" width="0.1" layer="21"/>
<wire x1="-1.41898125" y1="-2.446096875" x2="-1.39786875" y2="-2.4685625" width="0.1" layer="21"/>
<wire x1="-1.39786875" y1="-2.4685625" x2="-1.35099375" y2="-2.51153125" width="0.1" layer="21"/>
<wire x1="-1.35099375" y1="-2.51153125" x2="-1.3373625" y2="-2.52270625" width="0.1" layer="21"/>
<wire x1="-1.3373625" y1="-2.52270625" x2="-1.322634375" y2="-2.533996875" width="0.1" layer="21"/>
<wire x1="-1.322634375" y1="-2.533996875" x2="-1.223128125" y2="-2.600259375" width="0.1" layer="21"/>
<wire x1="-1.223128125" y1="-2.600259375" x2="-1.087815625" y2="-2.67356875" width="0.1" layer="21"/>
<wire x1="-1.087815625" y1="-2.67356875" x2="-1.05511875" y2="-2.68849375" width="0.1" layer="21"/>
<wire x1="-1.05511875" y1="-2.68849375" x2="-1.046909375" y2="-2.692009375" width="0.1" layer="21"/>
<wire x1="-1.046909375" y1="-2.692009375" x2="-0.95910625" y2="-2.72631875" width="0.1" layer="21"/>
<wire x1="-0.95910625" y1="-2.72631875" x2="-0.9497375" y2="-2.72993125" width="0.1" layer="21"/>
<wire x1="-0.9497375" y1="-2.72993125" x2="-0.9450625" y2="-2.731640625" width="0.1" layer="21"/>
<wire x1="-0.9450625" y1="-2.731640625" x2="-0.89720625" y2="-2.75008125" width="0.1" layer="21"/>
<wire x1="-0.89720625" y1="-2.75008125" x2="-0.892196875" y2="-2.75176875" width="0.1" layer="21"/>
<wire x1="-0.892196875" y1="-2.75176875" x2="-0.88730625" y2="-2.7531625" width="0.1" layer="21"/>
<wire x1="-0.88730625" y1="-2.7531625" x2="-0.836365625" y2="-2.7681875" width="0.1" layer="21"/>
<wire x1="-0.836365625" y1="-2.7681875" x2="-0.8311625" y2="-2.769778125" width="0.1" layer="21"/>
<wire x1="-0.8311625" y1="-2.769778125" x2="-0.8207125" y2="-2.772665625" width="0.1" layer="21"/>
<wire x1="-0.8207125" y1="-2.772665625" x2="-0.710971875" y2="-2.8040875" width="0.1" layer="21"/>
<wire x1="-0.710971875" y1="-2.8040875" x2="-0.699346875" y2="-2.806875" width="0.1" layer="21"/>
<wire x1="-0.699346875" y1="-2.806875" x2="-0.6878375" y2="-2.8092125" width="0.1" layer="21"/>
<wire x1="-0.6878375" y1="-2.8092125" x2="-0.5673375" y2="-2.83381875" width="0.1" layer="21"/>
<wire x1="-0.5673375" y1="-2.83381875" x2="-0.55476875" y2="-2.836371875" width="0.1" layer="21"/>
<wire x1="-0.55476875" y1="-2.836371875" x2="-0.542296875" y2="-2.839359375" width="0.1" layer="21"/>
<wire x1="-0.542296875" y1="-2.839359375" x2="-0.411896875" y2="-2.858309375" width="0.1" layer="21"/>
<wire x1="-0.411896875" y1="-2.858309375" x2="-0.39826875" y2="-2.8600375" width="0.1" layer="21"/>
<wire x1="-0.39826875" y1="-2.8600375" x2="-0.384834375" y2="-2.86153125" width="0.1" layer="21"/>
<wire x1="-0.384834375" y1="-2.86153125" x2="-0.245046875" y2="-2.877809375" width="0.1" layer="21"/>
<wire x1="-0.245046875" y1="-2.877809375" x2="-0.230534375" y2="-2.878871875" width="0.1" layer="21"/>
<wire x1="-0.230534375" y1="-2.878871875" x2="-0.216159375" y2="-2.8795375" width="0.1" layer="21"/>
<wire x1="-0.216159375" y1="-2.8795375" x2="-0.067946875" y2="-2.886353125" width="0.1" layer="21"/>
<wire x1="-0.067946875" y1="-2.886353125" x2="-0.052728125" y2="-2.88708125" width="0.1" layer="21"/>
<wire x1="-0.052728125" y1="-2.88708125" x2="-0.04506875" y2="-2.8874125" width="0.1" layer="21"/>
<wire x1="-0.04506875" y1="-2.8874125" x2="0.03240625" y2="-2.8907125" width="0.1" layer="21"/>
<wire x1="0.03240625" y1="-2.8907125" x2="0.04028125" y2="-2.890928125" width="0.1" layer="21"/>
<wire x1="0.04028125" y1="-2.890928125" x2="0.04805625" y2="-2.8907125" width="0.1" layer="21"/>
<wire x1="0.04805625" y1="-2.8907125" x2="0.127240625" y2="-2.888590625" width="0.1" layer="21"/>
<wire x1="0.127240625" y1="-2.888590625" x2="0.135234375" y2="-2.888475" width="0.1" layer="21"/>
<wire x1="0.135234375" y1="-2.888475" x2="0.15121875" y2="-2.887925" width="0.1" layer="21"/>
<wire x1="0.15121875" y1="-2.887925" x2="0.31559375" y2="-2.883346875" width="0.1" layer="21"/>
<wire x1="0.31559375" y1="-2.883346875" x2="0.332421875" y2="-2.8829375" width="0.1" layer="21"/>
<wire x1="0.332421875" y1="-2.8829375" x2="0.349175" y2="-2.882071875" width="0.1" layer="21"/>
<wire x1="0.349175" y1="-2.882071875" x2="0.519540625" y2="-2.866734375" width="0.1" layer="21"/>
<wire x1="0.519540625" y1="-2.866734375" x2="0.536996875" y2="-2.865240625" width="0.1" layer="21"/>
<wire x1="0.536996875" y1="-2.865240625" x2="0.5456375" y2="-2.864396875" width="0.1" layer="21"/>
<wire x1="0.5456375" y1="-2.864396875" x2="0.633640625" y2="-2.855875" width="0.1" layer="21"/>
<wire x1="0.633640625" y1="-2.855875" x2="0.642575" y2="-2.855128125" width="0.1" layer="21"/>
<wire x1="0.642575" y1="-2.855128125" x2="0.651453125" y2="-2.854046875" width="0.1" layer="21"/>
<wire x1="0.651453125" y1="-2.854046875" x2="0.740828125" y2="-2.84415" width="0.1" layer="21"/>
<wire x1="0.740828125" y1="-2.84415" x2="0.74978125" y2="-2.8425375" width="0.1" layer="21"/>
<wire x1="0.74978125" y1="-2.8425375" x2="0.76755625" y2="-2.839671875" width="0.1" layer="21"/>
<wire x1="0.76755625" y1="-2.839671875" x2="0.949978125" y2="-2.810390625" width="0.1" layer="21"/>
<wire x1="0.949978125" y1="-2.810390625" x2="0.9685375" y2="-2.8073875" width="0.1" layer="21"/>
<wire x1="0.9685375" y1="-2.8073875" x2="1.0057125" y2="-2.80228125" width="0.1" layer="21"/>
<wire x1="1.0057125" y1="-2.80228125" x2="1.38809375" y2="-2.7225875" width="0.1" layer="21"/>
<wire x1="1.38809375" y1="-2.7225875" x2="1.427625" y2="-2.71386875" width="0.1" layer="21"/>
<wire x1="1.427625" y1="-2.71386875" x2="1.437425" y2="-2.711590625" width="0.1" layer="21"/>
<wire x1="1.437425" y1="-2.711590625" x2="1.5367375" y2="-2.688828125" width="0.1" layer="21"/>
<wire x1="1.5367375" y1="-2.688828125" x2="1.546753125" y2="-2.686590625" width="0.1" layer="21"/>
<wire x1="1.546753125" y1="-2.686590625" x2="1.556653125" y2="-2.684234375" width="0.1" layer="21"/>
<wire x1="1.556653125" y1="-2.684234375" x2="1.653175" y2="-2.657584375" width="0.1" layer="21"/>
<wire x1="1.653175" y1="-2.657584375" x2="1.662975" y2="-2.6549125" width="0.1" layer="21"/>
<wire x1="1.662975" y1="-2.6549125" x2="1.682278125" y2="-2.649375" width="0.1" layer="21"/>
<wire x1="1.682278125" y1="-2.649375" x2="1.878428125" y2="-2.593759375" width="0.1" layer="21"/>
<wire x1="1.878428125" y1="-2.593759375" x2="1.89834375" y2="-2.588121875" width="0.1" layer="21"/>
<wire x1="1.89834375" y1="-2.588121875" x2="1.908140625" y2="-2.58525625" width="0.1" layer="21"/>
<wire x1="1.908140625" y1="-2.58525625" x2="2.008084375" y2="-2.55658125" width="0.1" layer="21"/>
<wire x1="2.008084375" y1="-2.55658125" x2="2.0181" y2="-2.553696875" width="0.1" layer="21"/>
<wire x1="2.0181" y1="-2.553696875" x2="2.0282125" y2="-2.5506125" width="0.1" layer="21"/>
<wire x1="2.0282125" y1="-2.5506125" x2="2.13029375" y2="-2.5199375" width="0.1" layer="21"/>
<wire x1="2.13029375" y1="-2.5199375" x2="2.14050625" y2="-2.516953125" width="0.1" layer="21"/>
<wire x1="2.14050625" y1="-2.516953125" x2="2.160240625" y2="-2.5111" width="0.1" layer="21"/>
<wire x1="2.160240625" y1="-2.5111" x2="2.360653125" y2="-2.4541875" width="0.1" layer="21"/>
<wire x1="2.360653125" y1="-2.4541875" x2="2.381115625" y2="-2.448765625" width="0.1" layer="21"/>
<wire x1="2.381115625" y1="-2.448765625" x2="2.421809375" y2="-2.43776875" width="0.1" layer="21"/>
<wire x1="2.421809375" y1="-2.43776875" x2="2.840109375" y2="-2.33443125" width="0.1" layer="21"/>
<wire x1="2.840109375" y1="-2.33443125" x2="2.8830375" y2="-2.324965625" width="0.1" layer="21"/>
<wire x1="2.8830375" y1="-2.324965625" x2="2.91065" y2="-2.318778125" width="0.1" layer="21"/>
<wire x1="2.91065" y1="-2.318778125" x2="3.190225" y2="-2.2599625" width="0.1" layer="21"/>
<wire x1="3.190225" y1="-2.2599625" x2="3.2185625" y2="-2.254540625" width="0.1" layer="21"/>
<wire x1="3.2185625" y1="-2.254540625" x2="3.1851" y2="-2.31300625" width="0.1" layer="21"/>
<wire x1="3.1851" y1="-2.31300625" x2="3.0009875" y2="-2.594190625" width="0.1" layer="21"/>
<wire x1="3.0009875" y1="-2.594190625" x2="2.795353125" y2="-2.859075" width="0.1" layer="21"/>
<wire x1="2.795353125" y1="-2.859075" x2="2.75124375" y2="-2.909446875" width="0.1" layer="21"/>
<wire x1="2.75124375" y1="-2.909446875" x2="2.73365" y2="-2.914909375" width="0.1" layer="21"/>
<wire x1="2.73365" y1="-2.914909375" x2="2.56075" y2="-2.966578125" width="0.1" layer="21"/>
<wire x1="2.56075" y1="-2.966578125" x2="2.544" y2="-2.97081875" width="0.1" layer="21"/>
<wire x1="2.544" y1="-2.97081875" x2="2.50821875" y2="-2.980421875" width="0.1" layer="21"/>
<wire x1="2.50821875" y1="-2.980421875" x2="2.1578875" y2="-3.077396875" width="0.1" layer="21"/>
<wire x1="2.1578875" y1="-3.077396875" x2="2.1233625" y2="-3.08645" width="0.1" layer="21"/>
<wire x1="2.1233625" y1="-3.08645" x2="1.98485" y2="-3.12235" width="0.1" layer="21"/>
<wire x1="1.98485" y1="-3.12235" x2="1.331103125" y2="-3.2628625" width="0.1" layer="21"/>
<wire x1="1.331103125" y1="-3.2628625" x2="0.73870625" y2="-3.35055" width="0.1" layer="21"/>
<wire x1="0.73870625" y1="-3.35055" x2="0.631065625" y2="-3.36005625" width="0.1" layer="21"/>
<wire x1="0.631065625" y1="-3.36005625" x2="0.554378125" y2="-3.366965625" width="0.1" layer="21"/>
<wire x1="0.554378125" y1="-3.366965625" x2="0.32336875" y2="-3.379653125" width="0.1" layer="21"/>
<wire x1="0.32336875" y1="-3.379653125" x2="0.04805625" y2="-3.38179375" width="0.1" layer="21"/>
<wire x1="0.04805625" y1="-3.38179375" x2="-0.1917875" y2="-3.36961875" width="0.1" layer="21"/>
<wire x1="-0.1917875" y1="-3.36961875" x2="-0.34286875" y2="-3.354084375" width="0.1" layer="21"/>
<wire x1="-0.34286875" y1="-3.354084375" x2="-0.392828125" y2="-3.34640625" width="0.1" layer="21"/>
<wire x1="-0.392828125" y1="-3.34640625" x2="-0.4004875" y2="-3.3455625" width="0.1" layer="21"/>
<wire x1="-0.4004875" y1="-3.3455625" x2="-0.471028125" y2="-3.332875" width="0.1" layer="21"/>
<wire x1="-0.471028125" y1="-3.332875" x2="-0.477628125" y2="-3.331715625" width="0.1" layer="21"/>
<wire x1="-0.477628125" y1="-3.331715625" x2="-0.48434375" y2="-3.330321875" width="0.1" layer="21"/>
<wire x1="-0.48434375" y1="-3.330321875" x2="-0.546678125" y2="-3.31796875" width="0.1" layer="21"/>
<wire x1="-0.546678125" y1="-3.31796875" x2="-0.5523125" y2="-3.316671875" width="0.1" layer="21"/>
<wire x1="-0.5523125" y1="-3.316671875" x2="-0.56360625" y2="-3.313590625" width="0.1" layer="21"/>
<wire x1="-0.56360625" y1="-3.313590625" x2="-0.659953125" y2="-3.2882375" width="0.1" layer="21"/>
<wire x1="-0.659953125" y1="-3.2882375" x2="-0.66749375" y2="-3.285565625" width="0.1" layer="21"/>
<wire x1="-0.66749375" y1="-3.285565625" x2="-0.671340625" y2="-3.284309375" width="0.1" layer="21"/>
<wire x1="-0.671340625" y1="-3.284309375" x2="-0.70521875" y2="-3.27364375" width="0.1" layer="21"/>
<wire x1="-0.70521875" y1="-3.27364375" x2="-0.7080875" y2="-3.2728" width="0.1" layer="21"/>
<wire x1="-0.7080875" y1="-3.2728" x2="-0.710971875" y2="-3.271621875" width="0.1" layer="21"/>
<wire x1="-0.710971875" y1="-3.271621875" x2="-0.735146875" y2="-3.262353125" width="0.1" layer="21"/>
<wire x1="-0.735146875" y1="-3.262353125" x2="-0.737071875" y2="-3.26160625" width="0.1" layer="21"/>
<wire x1="-0.737071875" y1="-3.26160625" x2="-0.73909375" y2="-3.260840625" width="0.1" layer="21"/>
<wire x1="-0.73909375" y1="-3.260840625" x2="-0.7535875" y2="-3.254871875" width="0.1" layer="21"/>
<wire x1="-0.7535875" y1="-3.254871875" x2="-0.754434375" y2="-3.254459375" width="0.1" layer="21"/>
<wire x1="-0.754434375" y1="-3.254459375" x2="-0.755296875" y2="-3.254025" width="0.1" layer="21"/>
<wire x1="-0.755296875" y1="-3.254025" x2="-0.759990625" y2="-3.2523375" width="0.1" layer="21"/>
<wire x1="-0.7601875" y1="-3.2523375" x2="-0.759990625" y2="-3.2523375" width="0.1" layer="21"/>
<wire x1="-0.759990625" y1="-3.2523375" x2="-0.755925" y2="-3.255203125" width="0.1" layer="21"/>
<wire x1="-0.755925" y1="-3.255203125" x2="-0.755296875" y2="-3.255853125" width="0.1" layer="21"/>
<wire x1="-0.755296875" y1="-3.255853125" x2="-0.75453125" y2="-3.256384375" width="0.1" layer="21"/>
<wire x1="-0.75453125" y1="-3.256384375" x2="-0.74155" y2="-3.26531875" width="0.1" layer="21"/>
<wire x1="-0.74155" y1="-3.26531875" x2="-0.739725" y2="-3.266496875" width="0.1" layer="21"/>
<wire x1="-0.739725" y1="-3.266496875" x2="-0.738034375" y2="-3.26755625" width="0.1" layer="21"/>
<wire x1="-0.738034375" y1="-3.26755625" x2="-0.716178125" y2="-3.281953125" width="0.1" layer="21"/>
<wire x1="-0.716178125" y1="-3.281953125" x2="-0.71350625" y2="-3.283640625" width="0.1" layer="21"/>
<wire x1="-0.71350625" y1="-3.283640625" x2="-0.710875" y2="-3.285153125" width="0.1" layer="21"/>
<wire x1="-0.710875" y1="-3.285153125" x2="-0.679334375" y2="-3.302828125" width="0.1" layer="21"/>
<wire x1="-0.679334375" y1="-3.302828125" x2="-0.675703125" y2="-3.30495" width="0.1" layer="21"/>
<wire x1="-0.675703125" y1="-3.30495" x2="-0.668671875" y2="-3.30923125" width="0.1" layer="21"/>
<wire x1="-0.668671875" y1="-3.30923125" x2="-0.5776875" y2="-3.354184375" width="0.1" layer="21"/>
<wire x1="-0.5776875" y1="-3.354184375" x2="-0.566825" y2="-3.35950625" width="0.1" layer="21"/>
<wire x1="-0.566825" y1="-3.35950625" x2="-0.561365625" y2="-3.361959375" width="0.1" layer="21"/>
<wire x1="-0.561365625" y1="-3.361959375" x2="-0.501271875" y2="-3.3865875" width="0.1" layer="21"/>
<wire x1="-0.501271875" y1="-3.3865875" x2="-0.494790625" y2="-3.3892375" width="0.1" layer="21"/>
<wire x1="-0.494790625" y1="-3.3892375" x2="-0.488409375" y2="-3.391790625" width="0.1" layer="21"/>
<wire x1="-0.488409375" y1="-3.391790625" x2="-0.419125" y2="-3.4185375" width="0.1" layer="21"/>
<wire x1="-0.419125" y1="-3.4185375" x2="-0.4115625" y2="-3.420875" width="0.1" layer="21"/>
<wire x1="-0.4115625" y1="-3.420875" x2="-0.362446875" y2="-3.43825625" width="0.1" layer="21"/>
<wire x1="-0.362446875" y1="-3.43825625" x2="-0.211703125" y2="-3.481834375" width="0.1" layer="21"/>
<wire x1="-0.211703125" y1="-3.481834375" x2="0.031659375" y2="-3.5363875" width="0.1" layer="21"/>
<wire x1="0.031659375" y1="-3.5363875" x2="0.3151625" y2="-3.57931875" width="0.1" layer="21"/>
<wire x1="0.3151625" y1="-3.57931875" x2="0.55565625" y2="-3.6016875" width="0.1" layer="21"/>
<wire x1="0.55565625" y1="-3.6016875" x2="0.635978125" y2="-3.6055375" width="0.1" layer="21"/>
<wire x1="0.635978125" y1="-3.6055375" x2="0.74094375" y2="-3.610859375" width="0.1" layer="21"/>
<wire x1="0.74094375" y1="-3.610859375" x2="1.3191625" y2="-3.59945" width="0.1" layer="21"/>
<wire x1="1.3191625" y1="-3.59945" x2="1.95694375" y2="-3.533384375" width="0.1" layer="21"/>
<wire x1="1.95694375" y1="-3.533384375" x2="2.091725" y2="-3.511446875" width="0.1" layer="21"/>
<wire x1="2.091725" y1="-3.511446875" x2="1.9681375" y2="-3.60115625" width="0.1" layer="21"/>
<wire x1="1.9681375" y1="-3.60115625" x2="1.56985" y2="-3.832578125" width="0.1" layer="21"/>
<wire x1="1.56985" y1="-3.832578125" x2="0.99899375" y2="-4.071971875" width="0.1" layer="21"/>
<wire x1="0.99899375" y1="-4.071971875" x2="0.3873125" y2="-4.221578125" width="0.1" layer="21"/>
<wire x1="0.3873125" y1="-4.221578125" x2="-0.096209375" y2="-4.273246875" width="0.1" layer="21"/>
<wire x1="-0.096209375" y1="-4.273246875" x2="-0.3905875" y2="-4.273246875" width="0.1" layer="21"/>
<wire x1="-0.3905875" y1="-4.273246875" x2="-0.79045" y2="-4.237975" width="0.1" layer="21"/>
<wire x1="-0.79045" y1="-4.237975" x2="-1.302290625" y2="-4.135384375" width="0.1" layer="21"/>
<wire x1="-1.302290625" y1="-4.135384375" x2="-1.7883625" y2="-3.96969375" width="0.1" layer="21"/>
<wire x1="-1.7883625" y1="-3.96969375" x2="-2.134196875" y2="-3.808715625" width="0.1" layer="21"/>
<wire x1="-2.134196875" y1="-3.808715625" x2="-2.24405625" y2="-3.74575625" width="0.1" layer="21"/>
<wire x1="-2.24405625" y1="-3.74575625" x2="-2.24405625" y2="-2.2553875" width="0.1" layer="21"/>
<wire x1="-2.24405625" y1="-2.2553875" x2="-2.22585" y2="-2.250475" width="0.1" layer="21"/>
<wire x1="-2.22585" y1="-2.250475" x2="-2.1410125" y2="-2.21285" width="0.1" layer="21"/>
<wire x1="-2.1410125" y1="-2.21285" x2="-2.067075" y2="-2.159178125" width="0.1" layer="21"/>
<wire x1="-2.067075" y1="-2.159178125" x2="-2.054190625" y2="-2.146275" width="0.1" layer="21"/>
<wire x1="-2.054190625" y1="-2.146275" x2="-2.04013125" y2="-2.1322125" width="0.1" layer="21"/>
<wire x1="-2.04013125" y1="-2.1322125" x2="-2.003365625" y2="-2.084471875" width="0.1" layer="21"/>
<wire x1="-2.003365625" y1="-2.084471875" x2="-1.96489375" y2="-2.01341875" width="0.1" layer="21"/>
<wire x1="-1.96489375" y1="-2.01341875" x2="-1.940503125" y2="-1.934884375" width="0.1" layer="21"/>
<wire x1="-1.940503125" y1="-1.934884375" x2="-1.93198125" y2="-1.87180625" width="0.1" layer="21"/>
<wire x1="-1.93198125" y1="-1.87180625" x2="-1.93198125" y2="-1.82971875" width="0.1" layer="21"/>
<wire x1="-1.93198125" y1="-1.82971875" x2="-1.940503125" y2="-1.766759375" width="0.1" layer="21"/>
<wire x1="-1.940503125" y1="-1.766759375" x2="-1.96489375" y2="-1.68824375" width="0.1" layer="21"/>
<wire x1="-1.96489375" y1="-1.68824375" x2="-2.003365625" y2="-1.617153125" width="0.1" layer="21"/>
<wire x1="-2.003365625" y1="-1.617153125" x2="-2.04013125" y2="-1.56943125" width="0.1" layer="21"/>
<wire x1="-2.04013125" y1="-1.56943125" x2="-2.054190625" y2="-1.5554875" width="0.1" layer="21"/>
<wire x1="-2.054190625" y1="-1.5554875" x2="-2.068253125" y2="-1.541309375" width="0.1" layer="21"/>
<wire x1="-2.068253125" y1="-1.541309375" x2="-2.115975" y2="-1.504565625" width="0.1" layer="21"/>
<wire x1="-2.115975" y1="-1.504565625" x2="-2.1871625" y2="-1.466071875" width="0.1" layer="21"/>
<wire x1="-2.1871625" y1="-1.466071875" x2="-2.265796875" y2="-1.44168125" width="0.1" layer="21"/>
<wire x1="-2.265796875" y1="-1.44168125" x2="-2.32885625" y2="-1.43325625" width="0.1" layer="21"/>
<wire x1="-2.32885625" y1="-1.43325625" x2="-2.3709625" y2="-1.43325625" width="0.1" layer="21"/>
<wire x1="-2.3709625" y1="-1.43325625" x2="-2.433921875" y2="-1.44168125" width="0.1" layer="21"/>
<wire x1="-2.433921875" y1="-1.44168125" x2="-2.51245625" y2="-1.466071875" width="0.1" layer="21"/>
<wire x1="-2.51245625" y1="-1.466071875" x2="-2.583509375" y2="-1.504565625" width="0.1" layer="21"/>
<wire x1="-2.583509375" y1="-1.504565625" x2="-2.63113125" y2="-1.54140625" width="0.1" layer="21"/>
<wire x1="-2.63113125" y1="-1.54140625" x2="-2.6452125" y2="-1.5554875" width="0.1" layer="21"/>
<wire x1="-2.6452125" y1="-1.5554875" x2="-2.65925625" y2="-1.569528125" width="0.1" layer="21"/>
<wire x1="-2.65925625" y1="-1.569528125" x2="-2.6961375" y2="-1.617153125" width="0.1" layer="21"/>
<wire x1="-2.6961375" y1="-1.617153125" x2="-2.73470625" y2="-1.688125" width="0.1" layer="21"/>
<wire x1="-2.73470625" y1="-1.688125" x2="-2.759115625" y2="-1.7666625" width="0.1" layer="21"/>
<wire x1="-2.759115625" y1="-1.7666625" x2="-2.767521875" y2="-1.82971875" width="0.1" layer="21"/>
<wire x1="-2.767521875" y1="-1.82971875" x2="-2.767403125" y2="-1.85083125" width="0.1" layer="21"/>
<wire x1="-2.767403125" y1="-1.85083125" x2="-2.767521875" y2="-1.87180625" width="0.1" layer="21"/>
<wire x1="-2.767521875" y1="-1.87180625" x2="-2.759115625" y2="-1.934884375" width="0.1" layer="21"/>
<wire x1="-2.759115625" y1="-1.934884375" x2="-2.73470625" y2="-2.01341875" width="0.1" layer="21"/>
<wire x1="-2.73470625" y1="-2.01341875" x2="-2.6961375" y2="-2.084471875" width="0.1" layer="21"/>
<wire x1="-2.6961375" y1="-2.084471875" x2="-2.659371875" y2="-2.13209375" width="0.1" layer="21"/>
<wire x1="-2.659371875" y1="-2.13209375" x2="-2.6452125" y2="-2.146275" width="0.1" layer="21"/>
<wire x1="-2.6452125" y1="-2.146275" x2="-2.63274375" y2="-2.15874375" width="0.1" layer="21"/>
<wire x1="-2.63274375" y1="-2.15874375" x2="-2.56135625" y2="-2.211159375" width="0.1" layer="21"/>
<wire x1="-2.56135625" y1="-2.211159375" x2="-2.4798375" y2="-2.2482375" width="0.1" layer="21"/>
<wire x1="-2.4798375" y1="-2.2482375" x2="-2.46228125" y2="-2.253146875" width="0.1" layer="21"/>
<wire x1="-2.46228125" y1="-2.253146875" x2="-2.46228125" y2="-3.611290625" width="0.1" layer="21"/>
<wire x1="-2.46228125" y1="-3.611290625" x2="-2.545371875" y2="-3.556184375" width="0.1" layer="21"/>
<wire x1="-2.545371875" y1="-3.556184375" x2="-2.783725" y2="-3.374646875" width="0.1" layer="21"/>
<wire x1="-2.783725" y1="-3.374646875" x2="-3.0795" y2="-3.10785625" width="0.1" layer="21"/>
<wire x1="-3.0795" y1="-3.10785625" x2="-3.347565625" y2="-2.813259375" width="0.1" layer="21"/>
<wire x1="-3.347565625" y1="-2.813259375" x2="-3.530165625" y2="-2.57576875" width="0.1" layer="21"/>
<wire x1="-3.530165625" y1="-2.57576875" x2="-3.58546875" y2="-2.492971875" width="0.1" layer="21"/>
<wire x1="-3.58546875" y1="-2.492971875" x2="-3.5794" y2="-1.02008125" width="0.1" layer="21"/>
<wire x1="-3.5794" y1="-1.02008125" x2="-3.4506875" y2="-1.02008125" width="0.1" layer="21"/>
<wire x1="-3.4506875" y1="-1.02008125" x2="-3.4506875" y2="-1.00890625" width="0.1" layer="21"/>
<wire x1="-3.4506875" y1="-1.00890625" x2="-3.437059375" y2="-1.016253125" width="0.1" layer="21"/>
<wire x1="-3.437059375" y1="-1.016253125" x2="-2.7715875" y2="0.196978125" width="0.1" layer="21"/>
<wire x1="-2.7715875" y1="0.196978125" x2="-2.318428125" y2="0.196978125" width="0.1" layer="21"/>
<wire x1="-2.318428125" y1="0.196978125" x2="-2.3142625" y2="0.176415625" width="0.1" layer="21"/>
<wire x1="-2.3142625" y1="0.176415625" x2="-2.276559375" y2="0.0808375" width="0.1" layer="21"/>
<wire x1="-2.276559375" y1="0.0808375" x2="-2.2184875" y2="-0.0020375" width="0.1" layer="21"/>
<wire x1="-2.2184875" y1="-0.0020375" x2="-2.204209375" y2="-0.016315625" width="0.1" layer="21"/>
<wire x1="-2.204209375" y1="-0.016315625" x2="-2.190246875" y2="-0.030396875" width="0.1" layer="21"/>
<wire x1="-2.190246875" y1="-0.030396875" x2="-2.142525" y2="-0.0672375" width="0.1" layer="21"/>
<wire x1="-2.142525" y1="-0.0672375" x2="-2.07135625" y2="-0.105809375" width="0.1" layer="21"/>
<wire x1="-2.07135625" y1="-0.105809375" x2="-1.99281875" y2="-0.13021875" width="0.1" layer="21"/>
<wire x1="-1.99281875" y1="-0.13021875" x2="-1.929840625" y2="-0.138721875" width="0.1" layer="21"/>
<wire x1="-1.929840625" y1="-0.138721875" x2="-1.887753125" y2="-0.138721875" width="0.1" layer="21"/>
<wire x1="-1.887753125" y1="-0.138721875" x2="-1.82469375" y2="-0.13021875" width="0.1" layer="21"/>
<wire x1="-1.82469375" y1="-0.13021875" x2="-1.74618125" y2="-0.105809375" width="0.1" layer="21"/>
<wire x1="-1.74618125" y1="-0.105809375" x2="-1.674990625" y2="-0.0672375" width="0.1" layer="21"/>
<wire x1="-1.674990625" y1="-0.0672375" x2="-1.62725" y2="-0.030396875" width="0.1" layer="21"/>
<wire x1="-1.62725" y1="-0.030396875" x2="-1.6130875" y2="-0.016315625" width="0.1" layer="21"/>
<wire x1="-1.6130875" y1="-0.016315625" x2="-1.599028125" y2="-0.002234375" width="0.1" layer="21"/>
<wire x1="-1.599028125" y1="-0.002234375" x2="-1.562284375" y2="0.0454875" width="0.1" layer="21"/>
<wire x1="-1.562284375" y1="0.0454875" x2="-1.52379375" y2="0.1165375" width="0.1" layer="21"/>
<wire x1="-1.52379375" y1="0.1165375" x2="-1.499403125" y2="0.195071875" width="0.1" layer="21"/>
<wire x1="-1.499403125" y1="0.195071875" x2="-1.490878125" y2="0.258134375" width="0.1" layer="21"/>
<wire x1="-1.490878125" y1="0.258134375" x2="-1.490878125" y2="0.30021875" width="0.1" layer="21"/>
<wire x1="-1.490878125" y1="0.30021875" x2="-1.499403125" y2="0.363315625" width="0.1" layer="21"/>
<wire x1="-1.499403125" y1="0.363315625" x2="-1.52379375" y2="0.44183125" width="0.1" layer="21"/>
<wire x1="-1.52379375" y1="0.44183125" x2="-1.562284375" y2="0.5127875" width="0.1" layer="21"/>
<wire x1="-1.562284375" y1="0.5127875" x2="-1.599028125" y2="0.560409375" width="0.1" layer="21"/>
<wire x1="-1.599028125" y1="0.560409375" x2="-1.6130875" y2="0.574490625" width="0.1" layer="21"/>
<wire x1="-1.6130875" y1="0.574490625" x2="-1.62725" y2="0.58866875" width="0.1" layer="21"/>
<wire x1="-1.62725" y1="0.58866875" x2="-1.674990625" y2="0.6255125" width="0.1" layer="21"/>
<wire x1="-1.674990625" y1="0.6255125" x2="-1.74618125" y2="0.66408125" width="0.1" layer="21"/>
<wire x1="-1.74618125" y1="0.66408125" x2="-1.82469375" y2="0.688609375" width="0.1" layer="21"/>
<wire x1="-1.82469375" y1="0.688609375" x2="-1.887753125" y2="0.69699375" width="0.1" layer="21"/>
<wire x1="-1.887753125" y1="0.69699375" x2="-1.929840625" y2="0.69699375" width="0.1" layer="21"/>
<wire x1="-1.929840625" y1="0.69699375" x2="-1.99281875" y2="0.688609375" width="0.1" layer="21"/>
<wire x1="-1.99281875" y1="0.688609375" x2="-2.07135625" y2="0.66408125" width="0.1" layer="21"/>
<wire x1="-2.07135625" y1="0.66408125" x2="-2.142525" y2="0.6255125" width="0.1" layer="21"/>
<wire x1="-2.142525" y1="0.6255125" x2="-2.19015" y2="0.58866875" width="0.1" layer="21"/>
<wire x1="-2.19015" y1="0.58866875" x2="-2.204209375" y2="0.574490625" width="0.1" layer="21"/>
<wire x1="-2.204209375" y1="0.574490625" x2="-2.21434375" y2="0.564475" width="0.1" layer="21"/>
<wire x1="-2.21434375" y1="0.564475" x2="-2.25843125" y2="0.507896875" width="0.1" layer="21"/>
<wire x1="-2.25843125" y1="0.507896875" x2="-2.292759375" y2="0.4441875" width="0.1" layer="21"/>
<wire x1="-2.292759375" y1="0.4441875" x2="-2.2979625" y2="0.430540625" width="0.1" layer="21"/>
<wire x1="-2.2979625" y1="0.430540625" x2="-2.64326875" y2="0.430540625" width="0.1" layer="21"/>
<wire x1="-2.64326875" y1="0.430540625" x2="-1.7825125" y2="1.999975" width="0.1" layer="21"/>
<wire x1="-1.7825125" y1="1.999975" x2="-0.441925" y2="-0.58933125" width="0.1" layer="21"/>
<wire x1="-0.441925" y1="-0.58933125" x2="-0.455259375" y2="-0.606809375" width="0.1" layer="21"/>
<wire x1="-0.455259375" y1="-0.606809375" x2="-0.504471875" y2="-0.706534375" width="0.1" layer="21"/>
<wire x1="-0.504471875" y1="-0.706534375" x2="-0.526978125" y2="-0.8081625" width="0.1" layer="21"/>
<wire x1="-0.526978125" y1="-0.8081625" x2="-0.526978125" y2="-0.86315" width="0.1" layer="21"/>
<wire x1="-0.526978125" y1="-0.86315" x2="-0.518553125" y2="-0.926209375" width="0.1" layer="21"/>
<wire x1="-0.518553125" y1="-0.926209375" x2="-0.49414375" y2="-1.004725" width="0.1" layer="21"/>
<wire x1="-0.49414375" y1="-1.004725" x2="-0.455671875" y2="-1.075815625" width="0.1" layer="21"/>
<wire x1="-0.455671875" y1="-1.075815625" x2="-0.418928125" y2="-1.1234375" width="0.1" layer="21"/>
<wire x1="-0.418928125" y1="-1.1234375" x2="-0.40475" y2="-1.1376" width="0.1" layer="21"/>
<wire x1="-0.40475" y1="-1.1376" x2="-0.39066875" y2="-1.151659375" width="0.1" layer="21"/>
<wire x1="-0.39066875" y1="-1.151659375" x2="-0.342946875" y2="-1.188540625" width="0.1" layer="21"/>
<wire x1="-0.342946875" y1="-1.188540625" x2="-0.271775" y2="-1.2271125" width="0.1" layer="21"/>
<wire x1="-0.271775" y1="-1.2271125" x2="-0.1932625" y2="-1.251503125" width="0.1" layer="21"/>
<wire x1="-0.1932625" y1="-1.251503125" x2="-0.130203125" y2="-1.260025" width="0.1" layer="21"/>
<wire x1="-0.130203125" y1="-1.260025" x2="-0.088096875" y2="-1.260025" width="0.1" layer="21"/>
<wire x1="-0.088096875" y1="-1.260025" x2="-0.0251375" y2="-1.251503125" width="0.1" layer="21"/>
<wire x1="-0.0251375" y1="-1.251503125" x2="0.05328125" y2="-1.2271125" width="0.1" layer="21"/>
<wire x1="0.05328125" y1="-1.2271125" x2="0.12445" y2="-1.188540625" width="0.1" layer="21"/>
<wire x1="0.12445" y1="-1.188540625" x2="0.17219375" y2="-1.151659375" width="0.1" layer="21"/>
<wire x1="0.17219375" y1="-1.151659375" x2="0.200315625" y2="-1.1235375" width="0.1" layer="21"/>
<wire x1="0.200315625" y1="-1.1235375" x2="0.237078125" y2="-1.075815625" width="0.1" layer="21"/>
<wire x1="0.237078125" y1="-1.075815625" x2="0.27553125" y2="-1.004725" width="0.1" layer="21"/>
<wire x1="0.27553125" y1="-1.004725" x2="0.299940625" y2="-0.926209375" width="0.1" layer="21"/>
<wire x1="0.299940625" y1="-0.926209375" x2="0.3084625" y2="-0.86315" width="0.1" layer="21"/>
<wire x1="0.3084625" y1="-0.86315" x2="0.3084625" y2="-0.821065625" width="0.1" layer="21"/>
<wire x1="0.3084625" y1="-0.821065625" x2="0.299940625" y2="-0.757965625" width="0.1" layer="21"/>
<wire x1="0.299940625" y1="-0.757965625" x2="0.27553125" y2="-0.679471875" width="0.1" layer="21"/>
<wire x1="0.27553125" y1="-0.679471875" x2="0.237078125" y2="-0.608496875" width="0.1" layer="21"/>
<wire x1="0.237078125" y1="-0.608496875" x2="0.200315625" y2="-0.56075625" width="0.1" layer="21"/>
<wire x1="0.200315625" y1="-0.56075625" x2="0.186253125" y2="-0.54679375" width="0.1" layer="21"/>
<wire x1="0.186253125" y1="-0.54679375" x2="0.177946875" y2="-0.53850625" width="0.1" layer="21"/>
<wire x1="0.177946875" y1="-0.53850625" x2="0.0816375" y2="-0.47031875" width="0.1" layer="21"/>
<wire x1="0.0816375" y1="-0.47031875" x2="0.070859375" y2="-0.46519375" width="0.1" layer="21"/>
<wire x1="0.070859375" y1="-0.46519375" x2="0.070859375" y2="1.9118375" width="0.1" layer="21"/>
<wire x1="0.070859375" y1="1.9118375" x2="1.59830625" y2="0.3034375" width="0.1" layer="21"/>
<wire x1="1.59830625" y1="0.3034375" x2="2.9293875" y2="1.86326875" width="0.1" layer="21"/>
<wire x1="2.9293875" y1="1.86326875" x2="2.9293875" y2="-0.725484375" width="0.1" layer="21"/>
<wire x1="2.9293875" y1="-0.725484375" x2="2.24398125" y2="-0.725484375" width="0.1" layer="21"/>
<wire x1="2.24398125" y1="-0.725484375" x2="2.238971875" y2="-0.70831875" width="0.1" layer="21"/>
<wire x1="2.238971875" y1="-0.70831875" x2="2.20199375" y2="-0.62874375" width="0.1" layer="21"/>
<wire x1="2.20199375" y1="-0.62874375" x2="2.150521875" y2="-0.55896875" width="0.1" layer="21"/>
<wire x1="-3.81" y1="-2.0955" x2="-3.81" y2="-1.27" width="0.1" layer="21"/>
<wire x1="-3.81" y1="-1.0287" x2="-3.81" y2="-1.27" width="0.1" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="LOGO-8MM" urn="urn:adsk.eagle:package:8516700/2" type="box" library_version="2">
<packageinstances>
<packageinstance name="LOGO-8MM"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="LOGO" urn="urn:adsk.eagle:symbol:8516699/1" library_version="2">
<wire x1="-7.124878125" y1="-6.494115625" x2="-7.14994375" y2="-6.507584375" width="0.5" layer="94"/>
<wire x1="-7.14994375" y1="-6.507584375" x2="-7.38333125" y2="-6.5796625" width="0.5" layer="94"/>
<wire x1="-7.38333125" y1="-6.5796625" x2="-7.550121875" y2="-6.5796625" width="0.5" layer="94"/>
<wire x1="-7.550121875" y1="-6.5796625" x2="-7.7916125" y2="-6.497484375" width="0.5" layer="94"/>
<wire x1="-7.7916125" y1="-6.497484375" x2="-7.8177375" y2="-6.48189375" width="0.5" layer="94"/>
<wire x1="-7.8177375" y1="-6.48189375" x2="-7.830521875" y2="-6.4744125" width="0.5" layer="94"/>
<wire x1="-7.830521875" y1="-6.4744125" x2="-7.94724375" y2="-6.384128125" width="0.5" layer="94"/>
<wire x1="-7.94724375" y1="-6.384128125" x2="-7.95771875" y2="-6.37365" width="0.5" layer="94"/>
<wire x1="-7.95771875" y1="-6.37365" x2="-7.98989375" y2="-6.341478125" width="0.5" layer="94"/>
<wire x1="-7.98989375" y1="-6.341478125" x2="-8.1082375" y2="-6.15036875" width="0.5" layer="94"/>
<wire x1="-8.1082375" y1="-6.15036875" x2="-8.163046875" y2="-5.944728125" width="0.5" layer="94"/>
<wire x1="-8.163046875" y1="-5.944728125" x2="-8.163046875" y2="-5.807678125" width="0.5" layer="94"/>
<wire x1="-8.163046875" y1="-5.807678125" x2="-8.1082375" y2="-5.602725" width="0.5" layer="94"/>
<wire x1="-8.1082375" y1="-5.602725" x2="-7.98989375" y2="-5.411240625" width="0.5" layer="94"/>
<wire x1="-7.98989375" y1="-5.411240625" x2="-7.95771875" y2="-5.379440625" width="0.5" layer="94"/>
<wire x1="-7.95771875" y1="-5.379440625" x2="-7.925984375" y2="-5.34758125" width="0.5" layer="94"/>
<wire x1="-7.925984375" y1="-5.34758125" x2="-7.7345" y2="-5.2288625" width="0.5" layer="94"/>
<wire x1="-7.7345" y1="-5.2288625" x2="-7.529484375" y2="-5.174115625" width="0.5" layer="94"/>
<wire x1="-7.529484375" y1="-5.174115625" x2="-7.392496875" y2="-5.174115625" width="0.5" layer="94"/>
<wire x1="-7.392496875" y1="-5.174115625" x2="-7.18679375" y2="-5.2288625" width="0.5" layer="94"/>
<wire x1="-7.18679375" y1="-5.2288625" x2="-6.995684375" y2="-5.34758125" width="0.5" layer="94"/>
<wire x1="-6.995684375" y1="-5.34758125" x2="-6.9635125" y2="-5.379440625" width="0.5" layer="94"/>
<wire x1="-6.9635125" y1="-5.379440625" x2="-6.9317125" y2="-5.411240625" width="0.5" layer="94"/>
<wire x1="-6.9317125" y1="-5.411240625" x2="-6.81261875" y2="-5.602725" width="0.5" layer="94"/>
<wire x1="-6.81261875" y1="-5.602725" x2="-6.7575625" y2="-5.807678125" width="0.5" layer="94"/>
<wire x1="-6.7575625" y1="-5.807678125" x2="-6.7575625" y2="-5.944728125" width="0.5" layer="94"/>
<wire x1="-6.7575625" y1="-5.944728125" x2="-6.81261875" y2="-6.15036875" width="0.5" layer="94"/>
<wire x1="-6.81261875" y1="-6.15036875" x2="-6.9317125" y2="-6.341478125" width="0.5" layer="94"/>
<wire x1="-6.9317125" y1="-6.341478125" x2="-6.9635125" y2="-6.37365" width="0.5" layer="94"/>
<wire x1="-6.9635125" y1="-6.37365" x2="-6.97566875" y2="-6.385496875" width="0.5" layer="94"/>
<wire x1="-6.97566875" y1="-6.385496875" x2="-7.11035" y2="-6.485946875" width="0.5" layer="94"/>
<wire x1="-6.557596875" y1="0.389028125" x2="-6.57449375" y2="0.4058625" width="0.5" layer="94"/>
<wire x1="-6.57449375" y1="0.4058625" x2="-6.70475" y2="0.603459375" width="0.5" layer="94"/>
<wire x1="-6.70475" y1="0.603459375" x2="-6.713540625" y2="0.62540625" width="0.5" layer="94"/>
<wire x1="-6.713540625" y1="0.62540625" x2="-6.721584375" y2="0.64579375" width="0.5" layer="94"/>
<wire x1="-6.721584375" y1="0.64579375" x2="-6.76255" y2="0.82643125" width="0.5" layer="94"/>
<wire x1="-6.76255" y1="0.82643125" x2="-6.76255" y2="0.95225625" width="0.5" layer="94"/>
<wire x1="-6.76255" y1="0.95225625" x2="-6.71216875" y2="1.149415625" width="0.5" layer="94"/>
<wire x1="-6.71216875" y1="1.149415625" x2="-6.601928125" y2="1.335475" width="0.5" layer="94"/>
<wire x1="-6.601928125" y1="1.335475" x2="-6.572125" y2="1.3669625" width="0.5" layer="94"/>
<wire x1="-6.572125" y1="1.3669625" x2="-6.571128125" y2="1.368271875" width="0.5" layer="94"/>
<wire x1="-6.571128125" y1="1.368271875" x2="-6.55896875" y2="1.3822375" width="0.5" layer="94"/>
<wire x1="-6.55896875" y1="1.3822375" x2="-6.557596875" y2="1.383175" width="0.5" layer="94"/>
<wire x1="-6.557596875" y1="1.383175" x2="-6.525796875" y2="1.4152875" width="0.5" layer="94"/>
<wire x1="-6.525796875" y1="1.4152875" x2="-6.334" y2="1.53400625" width="0.5" layer="94"/>
<wire x1="-6.334" y1="1.53400625" x2="-6.1289875" y2="1.5891875" width="0.5" layer="94"/>
<wire x1="-6.1289875" y1="1.5891875" x2="-5.991996875" y2="1.5891875" width="0.5" layer="94"/>
<wire x1="-5.991996875" y1="1.5891875" x2="-5.786671875" y2="1.53400625" width="0.5" layer="94"/>
<wire x1="-5.786671875" y1="1.53400625" x2="-5.5955625" y2="1.4152875" width="0.5" layer="94"/>
<wire x1="-5.5955625" y1="1.4152875" x2="-5.563325" y2="1.383175" width="0.5" layer="94"/>
<wire x1="-5.563325" y1="1.383175" x2="-5.531525" y2="1.351375" width="0.5" layer="94"/>
<wire x1="-5.531525" y1="1.351375" x2="-5.41249375" y2="1.159890625" width="0.5" layer="94"/>
<wire x1="-5.41249375" y1="1.159890625" x2="-5.357375" y2="0.9549375" width="0.5" layer="94"/>
<wire x1="-5.357375" y1="0.9549375" x2="-5.357375" y2="0.81795" width="0.5" layer="94"/>
<wire x1="-5.357375" y1="0.81795" x2="-5.41249375" y2="0.612934375" width="0.5" layer="94"/>
<wire x1="-5.41249375" y1="0.612934375" x2="-5.531525" y2="0.4211375" width="0.5" layer="94"/>
<wire x1="-5.531525" y1="0.4211375" x2="-5.563325" y2="0.389028125" width="0.5" layer="94"/>
<wire x1="-5.563325" y1="0.389028125" x2="-5.5955625" y2="0.357228125" width="0.5" layer="94"/>
<wire x1="-5.5955625" y1="0.357228125" x2="-5.786671875" y2="0.238759375" width="0.5" layer="94"/>
<wire x1="-5.786671875" y1="0.238759375" x2="-6.011578125" y2="0.183328125" width="0.5" layer="94"/>
<wire x1="-6.011578125" y1="0.183328125" x2="-6.0603375" y2="0.182953125" width="0.5" layer="94"/>
<wire x1="-6.0603375" y1="0.182953125" x2="-6.108971875" y2="0.183328125" width="0.5" layer="94"/>
<wire x1="-6.108971875" y1="0.183328125" x2="-6.334" y2="0.238759375" width="0.5" layer="94"/>
<wire x1="-6.334" y1="0.238759375" x2="-6.525796875" y2="0.357228125" width="0.5" layer="94"/>
<wire x1="-0.662675" y1="-3.301671875" x2="-0.679571875" y2="-3.29325625" width="0.5" layer="94"/>
<wire x1="-0.679571875" y1="-3.29325625" x2="-0.830403125" y2="-3.183953125" width="0.5" layer="94"/>
<wire x1="-0.830403125" y1="-3.183953125" x2="-0.843621875" y2="-3.17116875" width="0.5" layer="94"/>
<wire x1="-0.843621875" y1="-3.17116875" x2="-0.875796875" y2="-3.138996875" width="0.5" layer="94"/>
<wire x1="-0.875796875" y1="-3.138996875" x2="-0.994890625" y2="-2.9472" width="0.5" layer="94"/>
<wire x1="-0.994890625" y1="-2.9472" x2="-1.049634375" y2="-2.742121875" width="0.5" layer="94"/>
<wire x1="-1.049634375" y1="-2.742121875" x2="-1.049634375" y2="-2.647471875" width="0.5" layer="94"/>
<wire x1="-1.049634375" y1="-2.647471875" x2="-1.0432125" y2="-2.576703125" width="0.5" layer="94"/>
<wire x1="-1.0432125" y1="-2.576703125" x2="-1.041903125" y2="-2.568346875" width="0.5" layer="94"/>
<wire x1="-1.041903125" y1="-2.568346875" x2="-1.0371625" y2="-2.5348" width="0.5" layer="94"/>
<wire x1="-1.0371625" y1="-2.5348" x2="-0.98535" y2="-2.37954375" width="0.5" layer="94"/>
<wire x1="-0.98535" y1="-2.37954375" x2="-0.901796875" y2="-2.24180625" width="0.5" layer="94"/>
<wire x1="-0.901796875" y1="-2.24180625" x2="-0.880846875" y2="-2.217490625" width="0.5" layer="94"/>
<wire x1="-0.880846875" y1="-2.217490625" x2="-0.877790625" y2="-2.21375" width="0.5" layer="94"/>
<wire x1="-0.877790625" y1="-2.21375" x2="-0.847053125" y2="-2.179890625" width="0.5" layer="94"/>
<wire x1="-0.847053125" y1="-2.179890625" x2="-0.843621875" y2="-2.176896875" width="0.5" layer="94"/>
<wire x1="-0.843621875" y1="-2.176896875" x2="-0.811821875" y2="-2.1451" width="0.5" layer="94"/>
<wire x1="-0.811821875" y1="-2.1451" x2="-0.6203375" y2="-2.02638125" width="0.5" layer="94"/>
<wire x1="-0.6203375" y1="-2.02638125" x2="-0.4150125" y2="-1.971571875" width="0.5" layer="94"/>
<wire x1="-0.4150125" y1="-1.971571875" x2="-0.278025" y2="-1.971571875" width="0.5" layer="94"/>
<wire x1="-0.278025" y1="-1.971571875" x2="-0.073009375" y2="-2.02638125" width="0.5" layer="94"/>
<wire x1="-0.073009375" y1="-2.02638125" x2="0.118475" y2="-2.1451" width="0.5" layer="94"/>
<wire x1="0.118475" y1="-2.1451" x2="0.1505875" y2="-2.176896875" width="0.5" layer="94"/>
<wire x1="0.1505875" y1="-2.176896875" x2="0.153640625" y2="-2.179890625" width="0.5" layer="94"/>
<wire x1="0.153640625" y1="-2.179890625" x2="0.18406875" y2="-2.21375" width="0.5" layer="94"/>
<wire x1="0.18406875" y1="-2.21375" x2="0.187125" y2="-2.217490625" width="0.5" layer="94"/>
<wire x1="0.187125" y1="-2.217490625" x2="0.2135625" y2="-2.248228125" width="0.5" layer="94"/>
<wire x1="0.2135625" y1="-2.248228125" x2="0.31095625" y2="-2.426184375" width="0.5" layer="94"/>
<wire x1="0.31095625" y1="-2.426184375" x2="0.3559125" y2="-2.61199375" width="0.5" layer="94"/>
<wire x1="0.3559125" y1="-2.61199375" x2="0.3559125" y2="-2.71275625" width="0.5" layer="94"/>
<wire x1="0.3559125" y1="-2.71275625" x2="0.341384375" y2="-2.81731875" width="0.5" layer="94"/>
<wire x1="0.341384375" y1="-2.81731875" x2="0.338703125" y2="-2.829790625" width="0.5" layer="94"/>
<wire x1="0.338703125" y1="-2.829790625" x2="0.331221875" y2="-2.8625875" width="0.5" layer="94"/>
<wire x1="0.331221875" y1="-2.8625875" x2="0.267684375" y2="-3.014853125" width="0.5" layer="94"/>
<wire x1="0.267684375" y1="-3.014853125" x2="0.173284375" y2="-3.1477875" width="0.5" layer="94"/>
<wire x1="0.173284375" y1="-3.1477875" x2="0.1505875" y2="-3.17116875" width="0.5" layer="94"/>
<wire x1="0.1505875" y1="-3.17116875" x2="0.118475" y2="-3.20290625" width="0.5" layer="94"/>
<wire x1="0.118475" y1="-3.20290625" x2="-0.073009375" y2="-3.3216875" width="0.5" layer="94"/>
<wire x1="-0.073009375" y1="-3.3216875" x2="-0.278025" y2="-3.37718125" width="0.5" layer="94"/>
<wire x1="-0.278025" y1="-3.37718125" x2="-0.414078125" y2="-3.37718125" width="0.5" layer="94"/>
<wire x1="-0.414078125" y1="-3.37718125" x2="-0.595334375" y2="-3.3317875" width="0.5" layer="94"/>
<wire x1="-0.595334375" y1="-3.3317875" x2="-0.6160375" y2="-3.323309375" width="0.5" layer="94"/>
<wire x1="-0.6160375" y1="-3.323309375" x2="-0.619965625" y2="-3.3216875" width="0.5" layer="94"/>
<wire x1="-0.619965625" y1="-3.3216875" x2="-0.658934375" y2="-3.30335625" width="0.5" layer="94"/>
<wire x1="5.225571875" y1="9.15390625" x2="5.235675" y2="9.143803125" width="0.5" layer="94"/>
<wire x1="5.235675" y1="9.143803125" x2="5.324278125" y2="9.029075" width="0.5" layer="94"/>
<wire x1="5.324278125" y1="9.029075" x2="5.331696875" y2="9.01623125" width="0.5" layer="94"/>
<wire x1="5.331696875" y1="9.01623125" x2="5.347659375" y2="8.99016875" width="0.5" layer="94"/>
<wire x1="5.347659375" y1="8.99016875" x2="5.4314625" y2="8.740509375" width="0.5" layer="94"/>
<wire x1="5.4314625" y1="8.740509375" x2="5.4314625" y2="8.6065125" width="0.5" layer="94"/>
<wire x1="5.4314625" y1="8.6065125" x2="5.40209375" y2="8.454375" width="0.5" layer="94"/>
<wire x1="5.40209375" y1="8.454375" x2="5.336125" y2="8.30279375" width="0.5" layer="94"/>
<wire x1="5.336125" y1="8.30279375" x2="5.318228125" y2="8.275046875" width="0.5" layer="94"/>
<wire x1="5.318228125" y1="8.275046875" x2="5.311434375" y2="8.264884375" width="0.5" layer="94"/>
<wire x1="5.311434375" y1="8.264884375" x2="5.234303125" y2="8.1688625" width="0.5" layer="94"/>
<wire x1="5.234303125" y1="8.1688625" x2="5.225571875" y2="8.159696875" width="0.5" layer="94"/>
<wire x1="5.225571875" y1="8.159696875" x2="5.1933375" y2="8.127896875" width="0.5" layer="94"/>
<wire x1="5.1933375" y1="8.127896875" x2="5.002228125" y2="8.0094875" width="0.5" layer="94"/>
<wire x1="5.002228125" y1="8.0094875" x2="4.778009375" y2="7.953996875" width="0.5" layer="94"/>
<wire x1="4.778009375" y1="7.953996875" x2="4.729246875" y2="7.953621875" width="0.5" layer="94"/>
<wire x1="4.729246875" y1="7.953621875" x2="4.6802375" y2="7.953996875" width="0.5" layer="94"/>
<wire x1="4.6802375" y1="7.953996875" x2="4.455209375" y2="8.0094875" width="0.5" layer="94"/>
<wire x1="4.455209375" y1="8.0094875" x2="4.2637875" y2="8.127896875" width="0.5" layer="94"/>
<wire x1="4.2637875" y1="8.127896875" x2="4.199875" y2="8.191809375" width="0.5" layer="94"/>
<wire x1="4.199875" y1="8.191809375" x2="4.080784375" y2="8.383603125" width="0.5" layer="94"/>
<wire x1="4.080784375" y1="8.383603125" x2="4.025975" y2="8.58861875" width="0.5" layer="94"/>
<wire x1="4.025975" y1="8.58861875" x2="4.025975" y2="8.72560625" width="0.5" layer="94"/>
<wire x1="4.025975" y1="8.72560625" x2="4.080784375" y2="8.930684375" width="0.5" layer="94"/>
<wire x1="4.080784375" y1="8.930684375" x2="4.199875" y2="9.12216875" width="0.5" layer="94"/>
<wire x1="4.199875" y1="9.12216875" x2="4.2319875" y2="9.15390625" width="0.5" layer="94"/>
<wire x1="4.2319875" y1="9.15390625" x2="4.2637875" y2="9.18570625" width="0.5" layer="94"/>
<wire x1="4.2637875" y1="9.18570625" x2="4.455209375" y2="9.304796875" width="0.5" layer="94"/>
<wire x1="4.455209375" y1="9.304796875" x2="4.6605375" y2="9.35998125" width="0.5" layer="94"/>
<wire x1="4.6605375" y1="9.35998125" x2="4.7975875" y2="9.35998125" width="0.5" layer="94"/>
<wire x1="4.7975875" y1="9.35998125" x2="5.0025375" y2="9.304796875" width="0.5" layer="94"/>
<wire x1="5.0025375" y1="9.304796875" x2="5.1933375" y2="9.18570625" width="0.5" layer="94"/>
<wire x1="6.348603125" y1="-2.176896875" x2="6.35808125" y2="-2.1864375" width="0.5" layer="94"/>
<wire x1="6.35808125" y1="-2.1864375" x2="6.441009375" y2="-2.291875" width="0.5" layer="94"/>
<wire x1="6.441009375" y1="-2.291875" x2="6.44805625" y2="-2.3034125" width="0.5" layer="94"/>
<wire x1="6.44805625" y1="-2.3034125" x2="6.464953125" y2="-2.330159375" width="0.5" layer="94"/>
<wire x1="6.464953125" y1="-2.330159375" x2="6.554240625" y2="-2.639678125" width="0.5" layer="94"/>
<wire x1="6.554240625" y1="-2.639678125" x2="6.554615625" y2="-2.673784375" width="0.5" layer="94"/>
<wire x1="6.554615625" y1="-2.673784375" x2="6.554240625" y2="-2.708015625" width="0.5" layer="94"/>
<wire x1="6.554240625" y1="-2.708015625" x2="6.464953125" y2="-3.017534375" width="0.5" layer="94"/>
<wire x1="6.464953125" y1="-3.017534375" x2="6.44805625" y2="-3.04459375" width="0.5" layer="94"/>
<wire x1="6.44805625" y1="-3.04459375" x2="6.441009375" y2="-3.056065625" width="0.5" layer="94"/>
<wire x1="6.441009375" y1="-3.056065625" x2="6.35808125" y2="-3.16125625" width="0.5" layer="94"/>
<wire x1="6.35808125" y1="-3.16125625" x2="6.348603125" y2="-3.17116875" width="0.5" layer="94"/>
<wire x1="6.348603125" y1="-3.17116875" x2="6.316428125" y2="-3.20290625" width="0.5" layer="94"/>
<wire x1="6.316428125" y1="-3.20290625" x2="6.124634375" y2="-3.3216875" width="0.5" layer="94"/>
<wire x1="6.124634375" y1="-3.3216875" x2="5.91968125" y2="-3.37718125" width="0.5" layer="94"/>
<wire x1="5.91968125" y1="-3.37718125" x2="5.78263125" y2="-3.37718125" width="0.5" layer="94"/>
<wire x1="5.78263125" y1="-3.37718125" x2="5.577678125" y2="-3.3216875" width="0.5" layer="94"/>
<wire x1="5.577678125" y1="-3.3216875" x2="5.38619375" y2="-3.20290625" width="0.5" layer="94"/>
<wire x1="5.38619375" y1="-3.20290625" x2="5.35439375" y2="-3.17116875" width="0.5" layer="94"/>
<wire x1="5.35439375" y1="-3.17116875" x2="5.32228125" y2="-3.138996875" width="0.5" layer="94"/>
<wire x1="5.32228125" y1="-3.138996875" x2="5.2031875" y2="-2.9472" width="0.5" layer="94"/>
<wire x1="5.2031875" y1="-2.9472" x2="5.14838125" y2="-2.742121875" width="0.5" layer="94"/>
<wire x1="5.14838125" y1="-2.742121875" x2="5.14838125" y2="-2.605134375" width="0.5" layer="94"/>
<wire x1="5.14838125" y1="-2.605134375" x2="5.2031875" y2="-2.40011875" width="0.5" layer="94"/>
<wire x1="5.2031875" y1="-2.40011875" x2="5.32228125" y2="-2.2086375" width="0.5" layer="94"/>
<wire x1="5.32228125" y1="-2.2086375" x2="5.35439375" y2="-2.176896875" width="0.5" layer="94"/>
<wire x1="5.35439375" y1="-2.176896875" x2="5.38619375" y2="-2.1451" width="0.5" layer="94"/>
<wire x1="5.38619375" y1="-2.1451" x2="5.577678125" y2="-2.02638125" width="0.5" layer="94"/>
<wire x1="5.577678125" y1="-2.02638125" x2="5.78263125" y2="-1.971571875" width="0.5" layer="94"/>
<wire x1="5.78263125" y1="-1.971571875" x2="5.91968125" y2="-1.971571875" width="0.5" layer="94"/>
<wire x1="5.91968125" y1="-1.971571875" x2="6.124634375" y2="-2.02638125" width="0.5" layer="94"/>
<wire x1="6.124634375" y1="-2.02638125" x2="6.316428125" y2="-2.1451" width="0.5" layer="94"/>
<wire x1="-12.11993125" y1="-6.664525" x2="-12.287784375" y2="-6.3381125" width="0.5" layer="94"/>
<wire x1="-12.287784375" y1="-6.3381125" x2="-12.714959375" y2="-5.319521875" width="0.5" layer="94"/>
<wire x1="-12.714959375" y1="-5.319521875" x2="-13.153421875" y2="-3.8977" width="0.5" layer="94"/>
<wire x1="-13.153421875" y1="-3.8977" x2="-13.42403125" y2="-2.410284375" width="0.5" layer="94"/>
<wire x1="-13.42403125" y1="-2.410284375" x2="-13.51675" y2="-1.253709375" width="0.5" layer="94"/>
<wire x1="-13.51675" y1="-1.253709375" x2="-13.51675" y2="-0.543703125" width="0.5" layer="94"/>
<wire x1="-13.51675" y1="-0.543703125" x2="-13.451403125" y2="0.43024375" width="0.5" layer="94"/>
<wire x1="-13.451403125" y1="0.43024375" x2="-13.258984375" y2="1.691384375" width="0.5" layer="94"/>
<wire x1="-13.258984375" y1="1.691384375" x2="-12.9460375" y2="2.908190625" width="0.5" layer="94"/>
<wire x1="-12.9460375" y1="2.908190625" x2="-12.518796875" y2="4.075240625" width="0.5" layer="94"/>
<wire x1="-12.518796875" y1="4.075240625" x2="-11.984003125" y2="5.1854875" width="0.5" layer="94"/>
<wire x1="-11.984003125" y1="5.1854875" x2="-11.34800625" y2="6.232446875" width="0.5" layer="94"/>
<wire x1="-11.34800625" y1="6.232446875" x2="-10.6166125" y2="7.21006875" width="0.5" layer="94"/>
<wire x1="-10.6166125" y1="7.21006875" x2="-9.796990625" y2="8.111934375" width="0.5" layer="94"/>
<wire x1="-9.796990625" y1="8.111934375" x2="-8.895125" y2="8.93161875" width="0.5" layer="94"/>
<wire x1="-8.895125" y1="8.93161875" x2="-7.917503125" y2="9.663075" width="0.5" layer="94"/>
<wire x1="-7.917503125" y1="9.663075" x2="-6.87048125" y2="10.29894375" width="0.5" layer="94"/>
<wire x1="-6.87048125" y1="10.29894375" x2="-5.760234375" y2="10.833803125" width="0.5" layer="94"/>
<wire x1="-5.760234375" y1="10.833803125" x2="-4.593496875" y2="11.26110625" width="0.5" layer="94"/>
<wire x1="-4.593496875" y1="11.26110625" x2="-3.376378125" y2="11.5739875" width="0.5" layer="94"/>
<wire x1="-3.376378125" y1="11.5739875" x2="-2.1156125" y2="11.766471875" width="0.5" layer="94"/>
<wire x1="-2.1156125" y1="11.766471875" x2="-1.141665625" y2="11.831815625" width="0.5" layer="94"/>
<wire x1="-1.141665625" y1="11.831815625" x2="-0.263434375" y2="11.831815625" width="0.5" layer="94"/>
<wire x1="-0.263434375" y1="11.831815625" x2="1.396825" y2="11.639646875" width="0.5" layer="94"/>
<wire x1="1.396825" y1="11.639646875" x2="3.486065625" y2="11.084834375" width="0.5" layer="94"/>
<wire x1="3.486065625" y1="11.084834375" x2="5.41730625" y2="10.19955625" width="0.5" layer="94"/>
<wire x1="5.41730625" y1="10.19955625" x2="6.750525" y2="9.3460125" width="0.5" layer="94"/>
<wire x1="6.750525" y1="9.3460125" x2="7.158496875" y2="9.01623125" width="0.5" layer="94"/>
<wire x1="7.158496875" y1="9.01623125" x2="6.005540625" y2="9.01623125" width="0.5" layer="94"/>
<wire x1="6.005540625" y1="9.01623125" x2="5.990075" y2="9.0716625" width="0.5" layer="94"/>
<wire x1="5.990075" y1="9.0716625" x2="5.87191875" y2="9.329490625" width="0.5" layer="94"/>
<wire x1="5.87191875" y1="9.329490625" x2="5.7061875" y2="9.555390625" width="0.5" layer="94"/>
<wire x1="5.7061875" y1="9.555390625" x2="5.666965625" y2="9.5947375" width="0.5" layer="94"/>
<wire x1="5.666965625" y1="9.5947375" x2="5.622321875" y2="9.63969375" width="0.5" layer="94"/>
<wire x1="5.622321875" y1="9.63969375" x2="5.47080625" y2="9.756728125" width="0.5" layer="94"/>
<wire x1="5.47080625" y1="9.756728125" x2="5.245090625" y2="9.8791875" width="0.5" layer="94"/>
<wire x1="5.245090625" y1="9.8791875" x2="4.99580625" y2="9.957003125" width="0.5" layer="94"/>
<wire x1="4.99580625" y1="9.957003125" x2="4.795903125" y2="9.983753125" width="0.5" layer="94"/>
<wire x1="4.795903125" y1="9.983753125" x2="4.66228125" y2="9.983753125" width="0.5" layer="94"/>
<wire x1="4.66228125" y1="9.983753125" x2="4.46163125" y2="9.957003125" width="0.5" layer="94"/>
<wire x1="4.46163125" y1="9.957003125" x2="4.211971875" y2="9.8791875" width="0.5" layer="94"/>
<wire x1="4.211971875" y1="9.8791875" x2="3.98638125" y2="9.756728125" width="0.5" layer="94"/>
<wire x1="3.98638125" y1="9.756728125" x2="3.834803125" y2="9.63969375" width="0.5" layer="94"/>
<wire x1="3.834803125" y1="9.63969375" x2="3.79046875" y2="9.5947375" width="0.5" layer="94"/>
<wire x1="3.79046875" y1="9.5947375" x2="3.7458875" y2="9.55003125" width="0.5" layer="94"/>
<wire x1="3.7458875" y1="9.55003125" x2="3.628790625" y2="9.398825" width="0.5" layer="94"/>
<wire x1="3.628790625" y1="9.398825" x2="3.50670625" y2="9.173484375" width="0.5" layer="94"/>
<wire x1="3.50670625" y1="9.173484375" x2="3.4292" y2="8.9242" width="0.5" layer="94"/>
<wire x1="3.4292" y1="8.9242" x2="3.4025125" y2="8.723984375" width="0.5" layer="94"/>
<wire x1="3.4025125" y1="8.723984375" x2="3.4025125" y2="8.5903625" width="0.5" layer="94"/>
<wire x1="3.4025125" y1="8.5903625" x2="3.4292" y2="8.390025" width="0.5" layer="94"/>
<wire x1="3.4292" y1="8.390025" x2="3.50670625" y2="8.140678125" width="0.5" layer="94"/>
<wire x1="3.50670625" y1="8.140678125" x2="3.628790625" y2="7.9150875" width="0.5" layer="94"/>
<wire x1="3.628790625" y1="7.9150875" x2="3.7455125" y2="7.763884375" width="0.5" layer="94"/>
<wire x1="3.7455125" y1="7.763884375" x2="3.79046875" y2="7.718865625" width="0.5" layer="94"/>
<wire x1="3.79046875" y1="7.718865625" x2="3.834803125" y2="7.674284375" width="0.5" layer="94"/>
<wire x1="3.834803125" y1="7.674284375" x2="3.98638125" y2="7.557184375" width="0.5" layer="94"/>
<wire x1="3.98638125" y1="7.557184375" x2="4.211971875" y2="7.434725" width="0.5" layer="94"/>
<wire x1="4.211971875" y1="7.434725" x2="4.46163125" y2="7.35734375" width="0.5" layer="94"/>
<wire x1="4.46163125" y1="7.35734375" x2="4.66228125" y2="7.330221875" width="0.5" layer="94"/>
<wire x1="4.66228125" y1="7.330221875" x2="4.795903125" y2="7.330221875" width="0.5" layer="94"/>
<wire x1="4.795903125" y1="7.330221875" x2="4.99580625" y2="7.35734375" width="0.5" layer="94"/>
<wire x1="4.99580625" y1="7.35734375" x2="5.245090625" y2="7.434725" width="0.5" layer="94"/>
<wire x1="5.245090625" y1="7.434725" x2="5.47080625" y2="7.557184375" width="0.5" layer="94"/>
<wire x1="5.47080625" y1="7.557184375" x2="5.622321875" y2="7.674284375" width="0.5" layer="94"/>
<wire x1="5.622321875" y1="7.674284375" x2="5.666965625" y2="7.718865625" width="0.5" layer="94"/>
<wire x1="5.666965625" y1="7.718865625" x2="5.704875" y2="7.75715" width="0.5" layer="94"/>
<wire x1="5.704875" y1="7.75715" x2="5.86586875" y2="7.974321875" width="0.5" layer="94"/>
<wire x1="5.86586875" y1="7.974321875" x2="5.98259375" y2="8.221925" width="0.5" layer="94"/>
<wire x1="5.98259375" y1="8.221925" x2="5.99880625" y2="8.275046875" width="0.5" layer="94"/>
<wire x1="5.99880625" y1="8.275046875" x2="7.9973875" y2="8.275046875" width="0.5" layer="94"/>
<wire x1="7.9973875" y1="8.275046875" x2="8.0603" y2="8.21413125" width="0.5" layer="94"/>
<wire x1="8.0603" y1="8.21413125" x2="8.664559375" y2="7.581503125" width="0.5" layer="94"/>
<wire x1="8.664559375" y1="7.581503125" x2="8.722296875" y2="7.515596875" width="0.5" layer="94"/>
<wire x1="8.722296875" y1="7.515596875" x2="5.01581875" y2="3.171753125" width="0.5" layer="94"/>
<wire x1="5.01581875" y1="3.171753125" x2="-1.253278125" y2="9.77131875" width="0.5" layer="94"/>
<wire x1="-1.253278125" y1="9.77131875" x2="-1.253278125" y2="1.054015625" width="0.5" layer="94"/>
<wire x1="-1.253278125" y1="1.054015625" x2="-5.6211875" y2="9.49148125" width="0.5" layer="94"/>
<wire x1="-5.6211875" y1="9.49148125" x2="-12.208284375" y2="-2.515471875" width="0.5" layer="94"/>
<wire x1="-12.208284375" y1="-2.515471875" x2="-12.204915625" y2="-2.51721875" width="0.5" layer="94"/>
<wire x1="-12.204915625" y1="-2.51721875" x2="-12.659528125" y2="-3.238759375" width="0.5" layer="94"/>
<wire x1="-12.659528125" y1="-3.238759375" x2="-12.1060875" y2="-3.238759375" width="0.5" layer="94"/>
<wire x1="6.789371875" y1="-1.736065625" x2="6.744790625" y2="-1.691171875" width="0.5" layer="94"/>
<wire x1="6.744790625" y1="-1.691171875" x2="6.5932125" y2="-1.574075" width="0.5" layer="94"/>
<wire x1="6.5932125" y1="-1.574075" x2="6.367559375" y2="-1.451615625" width="0.5" layer="94"/>
<wire x1="6.367559375" y1="-1.451615625" x2="6.118209375" y2="-1.3738625" width="0.5" layer="94"/>
<wire x1="6.118209375" y1="-1.3738625" x2="5.917996875" y2="-1.34705" width="0.5" layer="94"/>
<wire x1="5.917996875" y1="-1.34705" x2="5.784375" y2="-1.34705" width="0.5" layer="94"/>
<wire x1="5.784375" y1="-1.34705" x2="5.5841" y2="-1.3738625" width="0.5" layer="94"/>
<wire x1="5.5841" y1="-1.3738625" x2="5.335125" y2="-1.451615625" width="0.5" layer="94"/>
<wire x1="5.335125" y1="-1.451615625" x2="5.109471875" y2="-1.574075" width="0.5" layer="94"/>
<wire x1="5.109471875" y1="-1.574075" x2="4.95826875" y2="-1.691171875" width="0.5" layer="94"/>
<wire x1="4.95826875" y1="-1.691171875" x2="4.913625" y2="-1.736065625" width="0.5" layer="94"/>
<wire x1="4.913625" y1="-1.736065625" x2="4.86860625" y2="-1.7804" width="0.5" layer="94"/>
<wire x1="4.86860625" y1="-1.7804" x2="4.75156875" y2="-1.931978125" width="0.5" layer="94"/>
<wire x1="4.75156875" y1="-1.931978125" x2="4.629109375" y2="-2.15731875" width="0.5" layer="94"/>
<wire x1="4.629109375" y1="-2.15731875" x2="4.55160625" y2="-2.406540625" width="0.5" layer="94"/>
<wire x1="4.55160625" y1="-2.406540625" x2="4.52491875" y2="-2.60688125" width="0.5" layer="94"/>
<wire x1="4.52491875" y1="-2.60688125" x2="4.52491875" y2="-2.740503125" width="0.5" layer="94"/>
<wire x1="4.52491875" y1="-2.740503125" x2="4.55160625" y2="-2.940715625" width="0.5" layer="94"/>
<wire x1="4.55160625" y1="-2.940715625" x2="4.629109375" y2="-3.19" width="0.5" layer="94"/>
<wire x1="4.629109375" y1="-3.19" x2="4.75156875" y2="-3.415715625" width="0.5" layer="94"/>
<wire x1="4.75156875" y1="-3.415715625" x2="4.86860625" y2="-3.56723125" width="0.5" layer="94"/>
<wire x1="4.86860625" y1="-3.56723125" x2="4.913625" y2="-3.611875" width="0.5" layer="94"/>
<wire x1="4.913625" y1="-3.611875" x2="4.95795625" y2="-3.656521875" width="0.5" layer="94"/>
<wire x1="4.95795625" y1="-3.656521875" x2="5.109471875" y2="-3.77361875" width="0.5" layer="94"/>
<wire x1="5.109471875" y1="-3.77361875" x2="5.335125" y2="-3.896078125" width="0.5" layer="94"/>
<wire x1="5.335125" y1="-3.896078125" x2="5.5841" y2="-3.97351875" width="0.5" layer="94"/>
<wire x1="5.5841" y1="-3.97351875" x2="5.784375" y2="-4.00058125" width="0.5" layer="94"/>
<wire x1="5.784375" y1="-4.00058125" x2="5.917996875" y2="-4.00058125" width="0.5" layer="94"/>
<wire x1="5.917996875" y1="-4.00058125" x2="6.118209375" y2="-3.97351875" width="0.5" layer="94"/>
<wire x1="6.118209375" y1="-3.97351875" x2="6.367559375" y2="-3.896078125" width="0.5" layer="94"/>
<wire x1="6.367559375" y1="-3.896078125" x2="6.5932125" y2="-3.77361875" width="0.5" layer="94"/>
<wire x1="6.5932125" y1="-3.77361875" x2="6.744790625" y2="-3.656521875" width="0.5" layer="94"/>
<wire x1="6.744790625" y1="-3.656521875" x2="6.789371875" y2="-3.611875" width="0.5" layer="94"/>
<wire x1="6.789371875" y1="-3.611875" x2="6.82790625" y2="-3.57296875" width="0.5" layer="94"/>
<wire x1="6.82790625" y1="-3.57296875" x2="6.99133125" y2="-3.35143125" width="0.5" layer="94"/>
<wire x1="6.99133125" y1="-3.35143125" x2="7.108740625" y2="-3.098715625" width="0.5" layer="94"/>
<wire x1="7.108740625" y1="-3.098715625" x2="7.124640625" y2="-3.04459375" width="0.5" layer="94"/>
<wire x1="7.124640625" y1="-3.04459375" x2="10.75648125" y2="-3.04459375" width="0.5" layer="94"/>
<wire x1="10.75648125" y1="-3.04459375" x2="10.75648125" y2="-2.673784375" width="0.5" layer="94"/>
<wire x1="10.75648125" y1="-2.673784375" x2="10.778115625" y2="-2.673784375" width="0.5" layer="94"/>
<wire x1="10.778115625" y1="-2.673784375" x2="10.778115625" y2="4.320846875" width="0.5" layer="94"/>
<wire x1="10.778115625" y1="4.320846875" x2="10.9113625" y2="4.023175" width="0.5" layer="94"/>
<wire x1="10.9113625" y1="4.023175" x2="11.250375" y2="3.10204375" width="0.5" layer="94"/>
<wire x1="11.250375" y1="3.10204375" x2="11.597115625" y2="1.825690625" width="0.5" layer="94"/>
<wire x1="11.597115625" y1="1.825690625" x2="11.8102375" y2="0.499953125" width="0.5" layer="94"/>
<wire x1="11.8102375" y1="0.499953125" x2="11.88325" y2="-0.52611875" width="0.5" layer="94"/>
<wire x1="11.88325" y1="-0.52611875" x2="11.88325" y2="-1.456728125" width="0.5" layer="94"/>
<wire x1="11.88325" y1="-1.456728125" x2="11.665453125" y2="-3.2224875" width="0.5" layer="94"/>
<wire x1="11.665453125" y1="-3.2224875" x2="11.174553125" y2="-5.078715625" width="0.5" layer="94"/>
<wire x1="11.174553125" y1="-5.078715625" x2="11.037940625" y2="-5.433253125" width="0.5" layer="94"/>
<wire x1="11.037940625" y1="-5.433253125" x2="10.84539375" y2="-5.46804375" width="0.5" layer="94"/>
<wire x1="10.84539375" y1="-5.46804375" x2="8.9790625" y2="-5.88511875" width="0.5" layer="94"/>
<wire x1="8.9790625" y1="-5.88511875" x2="8.798428125" y2="-5.93281875" width="0.5" layer="94"/>
<wire x1="8.798428125" y1="-5.93281875" x2="8.657325" y2="-5.969359375" width="0.5" layer="94"/>
<wire x1="8.657325" y1="-5.969359375" x2="7.28631875" y2="-6.376334375" width="0.5" layer="94"/>
<wire x1="7.28631875" y1="-6.376334375" x2="7.1530125" y2="-6.420290625" width="0.5" layer="94"/>
<wire x1="7.1530125" y1="-6.420290625" x2="7.08641875" y2="-6.441990625" width="0.5" layer="94"/>
<wire x1="7.08641875" y1="-6.441990625" x2="6.4287875" y2="-6.670325" width="0.5" layer="94"/>
<wire x1="6.4287875" y1="-6.670325" x2="6.364190625" y2="-6.69395625" width="0.5" layer="94"/>
<wire x1="6.364190625" y1="-6.69395625" x2="6.333078125" y2="-6.705490625" width="0.5" layer="94"/>
<wire x1="6.333078125" y1="-6.705490625" x2="6.02486875" y2="-6.82015625" width="0.5" layer="94"/>
<wire x1="6.02486875" y1="-6.82015625" x2="5.994378125" y2="-6.831690625" width="0.5" layer="94"/>
<wire x1="5.994378125" y1="-6.831690625" x2="5.962953125" y2="-6.842853125" width="0.5" layer="94"/>
<wire x1="5.962953125" y1="-6.842853125" x2="5.653434375" y2="-6.9548375" width="0.5" layer="94"/>
<wire x1="5.653434375" y1="-6.9548375" x2="5.62294375" y2="-6.966" width="0.5" layer="94"/>
<wire x1="5.62294375" y1="-6.966" x2="5.561403125" y2="-6.988321875" width="0.5" layer="94"/>
<wire x1="5.561403125" y1="-6.988321875" x2="4.95353125" y2="-7.210171875" width="0.5" layer="94"/>
<wire x1="4.95353125" y1="-7.210171875" x2="4.893671875" y2="-7.23255625" width="0.5" layer="94"/>
<wire x1="4.893671875" y1="-7.23255625" x2="4.86355625" y2="-7.24303125" width="0.5" layer="94"/>
<wire x1="4.86355625" y1="-7.24303125" x2="4.565821875" y2="-7.351275" width="0.5" layer="94"/>
<wire x1="4.565821875" y1="-7.351275" x2="4.536703125" y2="-7.360815625" width="0.5" layer="94"/>
<wire x1="4.536703125" y1="-7.360815625" x2="4.507709375" y2="-7.36985625" width="0.5" layer="94"/>
<wire x1="4.507709375" y1="-7.36985625" x2="4.219765625" y2="-7.45989375" width="0.5" layer="94"/>
<wire x1="4.219765625" y1="-7.45989375" x2="4.191334375" y2="-7.469059375" width="0.5" layer="94"/>
<wire x1="4.191334375" y1="-7.469059375" x2="4.07635625" y2="-7.5045375" width="0.5" layer="94"/>
<wire x1="4.07635625" y1="-7.5045375" x2="3.5138125" y2="-7.6844875" width="0.5" layer="94"/>
<wire x1="3.5138125" y1="-7.6844875" x2="2.9586875" y2="-7.846165625" width="0.5" layer="94"/>
<wire x1="2.9586875" y1="-7.846165625" x2="2.8480125" y2="-7.872603125" width="0.5" layer="94"/>
<wire x1="2.8480125" y1="-7.872603125" x2="2.79295625" y2="-7.88713125" width="0.5" layer="94"/>
<wire x1="2.79295625" y1="-7.88713125" x2="2.251675" y2="-8.031603125" width="0.5" layer="94"/>
<wire x1="2.251675" y1="-8.031603125" x2="2.1984875" y2="-8.04581875" width="0.5" layer="94"/>
<wire x1="2.1984875" y1="-8.04581875" x2="2.1718625" y2="-8.0536125" width="0.5" layer="94"/>
<wire x1="2.1718625" y1="-8.0536125" x2="1.906928125" y2="-8.111725" width="0.5" layer="94"/>
<wire x1="1.906928125" y1="-8.111725" x2="1.880865625" y2="-8.117834375" width="0.5" layer="94"/>
<wire x1="1.880865625" y1="-8.117834375" x2="1.854553125" y2="-8.123196875" width="0.5" layer="94"/>
<wire x1="1.854553125" y1="-8.123196875" x2="1.59404375" y2="-8.17738125" width="0.5" layer="94"/>
<wire x1="1.59404375" y1="-8.17738125" x2="1.56829375" y2="-8.18280625" width="0.5" layer="94"/>
<wire x1="1.56829375" y1="-8.18280625" x2="1.516540625" y2="-8.192909375" width="0.5" layer="94"/>
<wire x1="1.516540625" y1="-8.192909375" x2="1.009115625" y2="-8.298159375" width="0.5" layer="94"/>
<wire x1="1.009115625" y1="-8.298159375" x2="0.959421875" y2="-8.306575" width="0.5" layer="94"/>
<wire x1="0.959421875" y1="-8.306575" x2="0.909353125" y2="-8.313684375" width="0.5" layer="94"/>
<wire x1="0.909353125" y1="-8.313684375" x2="0.420509375" y2="-8.386075" width="0.5" layer="94"/>
<wire x1="0.420509375" y1="-8.386075" x2="0.373121875" y2="-8.393184375" width="0.5" layer="94"/>
<wire x1="0.373121875" y1="-8.393184375" x2="0.100890625" y2="-8.43415" width="0.5" layer="94"/>
<wire x1="0.100890625" y1="-8.43415" x2="-0.72390625" y2="-8.506540625" width="0.5" layer="94"/>
<wire x1="-0.72390625" y1="-8.506540625" x2="-1.70620625" y2="-8.51533125" width="0.5" layer="94"/>
<wire x1="-1.70620625" y1="-8.51533125" x2="-2.5590625" y2="-8.44119375" width="0.5" layer="94"/>
<wire x1="-2.5590625" y1="-8.44119375" x2="-3.093921875" y2="-8.34985" width="0.5" layer="94"/>
<wire x1="-3.093921875" y1="-8.34985" x2="-3.268384375" y2="-8.306575" width="0.5" layer="94"/>
<wire x1="-3.268384375" y1="-8.306575" x2="-3.373634375" y2="-8.27814375" width="0.5" layer="94"/>
<wire x1="-3.373634375" y1="-8.27814375" x2="-3.81883125" y2="-8.1269375" width="0.5" layer="94"/>
<wire x1="-3.81883125" y1="-8.1269375" x2="-4.157465625" y2="-7.979475" width="0.5" layer="94"/>
<wire x1="-4.157465625" y1="-7.979475" x2="-4.20890625" y2="-7.9530375" width="0.5" layer="94"/>
<wire x1="-4.20890625" y1="-7.9530375" x2="-4.25660625" y2="-7.926725" width="0.5" layer="94"/>
<wire x1="-4.25660625" y1="-7.926725" x2="-4.42576875" y2="-7.822534375" width="0.5" layer="94"/>
<wire x1="-4.42576875" y1="-7.822534375" x2="-4.50489375" y2="-7.76598125" width="0.5" layer="94"/>
<wire x1="-4.50489375" y1="-7.76598125" x2="-4.507575" y2="-7.763675" width="0.5" layer="94"/>
<wire x1="-4.507575" y1="-7.763675" x2="-4.50526875" y2="-7.76635625" width="0.5" layer="94"/>
<wire x1="-4.50526875" y1="-7.76635625" x2="-4.4382375" y2="-7.8376875" width="0.5" layer="94"/>
<wire x1="-4.4382375" y1="-7.8376875" x2="-4.289403125" y2="-7.9741125" width="0.5" layer="94"/>
<wire x1="-4.289403125" y1="-7.9741125" x2="-4.24613125" y2="-8.009590625" width="0.5" layer="94"/>
<wire x1="-4.24613125" y1="-8.009590625" x2="-4.199365625" y2="-8.04544375" width="0.5" layer="94"/>
<wire x1="-4.199365625" y1="-8.04544375" x2="-3.883428125" y2="-8.255821875" width="0.5" layer="94"/>
<wire x1="-3.883428125" y1="-8.255821875" x2="-3.45381875" y2="-8.488584375" width="0.5" layer="94"/>
<wire x1="-3.45381875" y1="-8.488584375" x2="-3.350003125" y2="-8.535971875" width="0.5" layer="94"/>
<wire x1="-3.350003125" y1="-8.535971875" x2="-3.3239375" y2="-8.54713125" width="0.5" layer="94"/>
<wire x1="-3.3239375" y1="-8.54713125" x2="-3.0451625" y2="-8.6560625" width="0.5" layer="94"/>
<wire x1="-3.0451625" y1="-8.6560625" x2="-3.01541875" y2="-8.667534375" width="0.5" layer="94"/>
<wire x1="-3.01541875" y1="-8.667534375" x2="-3.000578125" y2="-8.672959375" width="0.5" layer="94"/>
<wire x1="-3.000578125" y1="-8.672959375" x2="-2.848628125" y2="-8.731509375" width="0.5" layer="94"/>
<wire x1="-2.848628125" y1="-8.731509375" x2="-2.832728125" y2="-8.73686875" width="0.5" layer="94"/>
<wire x1="-2.832728125" y1="-8.73686875" x2="-2.8172" y2="-8.741296875" width="0.5" layer="94"/>
<wire x1="-2.8172" y1="-8.741296875" x2="-2.655459375" y2="-8.788996875" width="0.5" layer="94"/>
<wire x1="-2.655459375" y1="-8.788996875" x2="-2.638934375" y2="-8.794046875" width="0.5" layer="94"/>
<wire x1="-2.638934375" y1="-8.794046875" x2="-2.6057625" y2="-8.8032125" width="0.5" layer="94"/>
<wire x1="-2.6057625" y1="-8.8032125" x2="-2.2573375" y2="-8.902978125" width="0.5" layer="94"/>
<wire x1="-2.2573375" y1="-8.902978125" x2="-2.220425" y2="-8.91183125" width="0.5" layer="94"/>
<wire x1="-2.220425" y1="-8.91183125" x2="-2.1838875" y2="-8.91925" width="0.5" layer="94"/>
<wire x1="-2.1838875" y1="-8.91925" x2="-1.80129375" y2="-8.997378125" width="0.5" layer="94"/>
<wire x1="-1.80129375" y1="-8.997378125" x2="-1.7613875" y2="-9.005484375" width="0.5" layer="94"/>
<wire x1="-1.7613875" y1="-9.005484375" x2="-1.72179375" y2="-9.0149625" width="0.5" layer="94"/>
<wire x1="-1.72179375" y1="-9.0149625" x2="-1.307775" y2="-9.07513125" width="0.5" layer="94"/>
<wire x1="-1.307775" y1="-9.07513125" x2="-1.2645" y2="-9.08061875" width="0.5" layer="94"/>
<wire x1="-1.2645" y1="-9.08061875" x2="-1.221853125" y2="-9.085359375" width="0.5" layer="94"/>
<wire x1="-1.221853125" y1="-9.085359375" x2="-0.778028125" y2="-9.137046875" width="0.5" layer="94"/>
<wire x1="-0.778028125" y1="-9.137046875" x2="-0.73195" y2="-9.140415625" width="0.5" layer="94"/>
<wire x1="-0.73195" y1="-9.140415625" x2="-0.686309375" y2="-9.142534375" width="0.5" layer="94"/>
<wire x1="-0.686309375" y1="-9.142534375" x2="-0.215734375" y2="-9.164171875" width="0.5" layer="94"/>
<wire x1="-0.215734375" y1="-9.164171875" x2="-0.1674125" y2="-9.166478125" width="0.5" layer="94"/>
<wire x1="-0.1674125" y1="-9.166478125" x2="-0.14309375" y2="-9.1675375" width="0.5" layer="94"/>
<wire x1="-0.14309375" y1="-9.1675375" x2="0.1028875" y2="-9.1780125" width="0.5" layer="94"/>
<wire x1="0.1028875" y1="-9.1780125" x2="0.127890625" y2="-9.1787" width="0.5" layer="94"/>
<wire x1="0.127890625" y1="-9.1787" x2="0.15258125" y2="-9.1780125" width="0.5" layer="94"/>
<wire x1="0.15258125" y1="-9.1780125" x2="0.4039875" y2="-9.171278125" width="0.5" layer="94"/>
<wire x1="0.4039875" y1="-9.171278125" x2="0.4293625" y2="-9.17090625" width="0.5" layer="94"/>
<wire x1="0.4293625" y1="-9.17090625" x2="0.48011875" y2="-9.169159375" width="0.5" layer="94"/>
<wire x1="0.48011875" y1="-9.169159375" x2="1.002009375" y2="-9.15463125" width="0.5" layer="94"/>
<wire x1="1.002009375" y1="-9.15463125" x2="1.05544375" y2="-9.153321875" width="0.5" layer="94"/>
<wire x1="1.05544375" y1="-9.153321875" x2="1.10863125" y2="-9.150578125" width="0.5" layer="94"/>
<wire x1="1.10863125" y1="-9.150578125" x2="1.6495375" y2="-9.10188125" width="0.5" layer="94"/>
<wire x1="1.6495375" y1="-9.10188125" x2="1.70496875" y2="-9.097140625" width="0.5" layer="94"/>
<wire x1="1.70496875" y1="-9.097140625" x2="1.73240625" y2="-9.0944625" width="0.5" layer="94"/>
<wire x1="1.73240625" y1="-9.0944625" x2="2.01180625" y2="-9.0674" width="0.5" layer="94"/>
<wire x1="2.01180625" y1="-9.0674" x2="2.040175" y2="-9.06503125" width="0.5" layer="94"/>
<wire x1="2.040175" y1="-9.06503125" x2="2.068359375" y2="-9.0616" width="0.5" layer="94"/>
<wire x1="2.068359375" y1="-9.0616" x2="2.352125" y2="-9.030175" width="0.5" layer="94"/>
<wire x1="2.352125" y1="-9.030175" x2="2.380559375" y2="-9.0250625" width="0.5" layer="94"/>
<wire x1="2.380559375" y1="-9.0250625" x2="2.4369875" y2="-9.015959375" width="0.5" layer="94"/>
<wire x1="2.4369875" y1="-9.015959375" x2="3.016178125" y2="-8.922990625" width="0.5" layer="94"/>
<wire x1="3.016178125" y1="-8.922990625" x2="3.0751" y2="-8.913453125" width="0.5" layer="94"/>
<wire x1="3.0751" y1="-8.913453125" x2="3.193134375" y2="-8.897240625" width="0.5" layer="94"/>
<wire x1="3.193134375" y1="-8.897240625" x2="4.407196875" y2="-8.6442125" width="0.5" layer="94"/>
<wire x1="4.407196875" y1="-8.6442125" x2="4.5327125" y2="-8.61653125" width="0.5" layer="94"/>
<wire x1="4.5327125" y1="-8.61653125" x2="4.563828125" y2="-8.609296875" width="0.5" layer="94"/>
<wire x1="4.563828125" y1="-8.609296875" x2="4.87914375" y2="-8.53703125" width="0.5" layer="94"/>
<wire x1="4.87914375" y1="-8.53703125" x2="4.91094375" y2="-8.529921875" width="0.5" layer="94"/>
<wire x1="4.91094375" y1="-8.529921875" x2="4.94236875" y2="-8.522440625" width="0.5" layer="94"/>
<wire x1="4.94236875" y1="-8.522440625" x2="5.24883125" y2="-8.437828125" width="0.5" layer="94"/>
<wire x1="5.24883125" y1="-8.437828125" x2="5.27994375" y2="-8.429346875" width="0.5" layer="94"/>
<wire x1="5.27994375" y1="-8.429346875" x2="5.3412375" y2="-8.411765625" width="0.5" layer="94"/>
<wire x1="5.3412375" y1="-8.411765625" x2="5.9640125" y2="-8.235184375" width="0.5" layer="94"/>
<wire x1="5.9640125" y1="-8.235184375" x2="6.0272375" y2="-8.2172875" width="0.5" layer="94"/>
<wire x1="6.0272375" y1="-8.2172875" x2="6.058353125" y2="-8.208184375" width="0.5" layer="94"/>
<wire x1="6.058353125" y1="-8.208184375" x2="6.3756625" y2="-8.11715" width="0.5" layer="94"/>
<wire x1="6.3756625" y1="-8.11715" x2="6.4074625" y2="-8.107984375" width="0.5" layer="94"/>
<wire x1="6.4074625" y1="-8.107984375" x2="6.439575" y2="-8.09819375" width="0.5" layer="94"/>
<wire x1="6.439575" y1="-8.09819375" x2="6.76368125" y2="-8.0008" width="0.5" layer="94"/>
<wire x1="6.76368125" y1="-8.0008" x2="6.79610625" y2="-7.991321875" width="0.5" layer="94"/>
<wire x1="6.79610625" y1="-7.991321875" x2="6.85876875" y2="-7.972740625" width="0.5" layer="94"/>
<wire x1="6.85876875" y1="-7.972740625" x2="7.495075" y2="-7.79204375" width="0.5" layer="94"/>
<wire x1="7.495075" y1="-7.79204375" x2="7.560046875" y2="-7.774834375" width="0.5" layer="94"/>
<wire x1="7.560046875" y1="-7.774834375" x2="7.689240625" y2="-7.739915625" width="0.5" layer="94"/>
<wire x1="7.689240625" y1="-7.739915625" x2="9.017346875" y2="-7.41181875" width="0.5" layer="94"/>
<wire x1="9.017346875" y1="-7.41181875" x2="9.15365" y2="-7.381765625" width="0.5" layer="94"/>
<wire x1="9.15365" y1="-7.381765625" x2="9.24131875" y2="-7.362125" width="0.5" layer="94"/>
<wire x1="9.24131875" y1="-7.362125" x2="10.128965625" y2="-7.175378125" width="0.5" layer="94"/>
<wire x1="10.128965625" y1="-7.175378125" x2="10.218940625" y2="-7.15816875" width="0.5" layer="94"/>
<wire x1="10.218940625" y1="-7.15816875" x2="10.112690625" y2="-7.34379375" width="0.5" layer="94"/>
<wire x1="10.112690625" y1="-7.34379375" x2="9.5281375" y2="-8.236553125" width="0.5" layer="94"/>
<wire x1="9.5281375" y1="-8.236553125" x2="8.875246875" y2="-9.0775625" width="0.5" layer="94"/>
<wire x1="8.875246875" y1="-9.0775625" x2="8.735203125" y2="-9.237496875" width="0.5" layer="94"/>
<wire x1="8.735203125" y1="-9.237496875" x2="8.679334375" y2="-9.25483125" width="0.5" layer="94"/>
<wire x1="8.679334375" y1="-9.25483125" x2="8.130384375" y2="-9.41888125" width="0.5" layer="94"/>
<wire x1="8.130384375" y1="-9.41888125" x2="8.077196875" y2="-9.43235" width="0.5" layer="94"/>
<wire x1="8.077196875" y1="-9.43235" x2="7.963590625" y2="-9.4628375" width="0.5" layer="94"/>
<wire x1="7.963590625" y1="-9.4628375" x2="6.8512875" y2="-9.770734375" width="0.5" layer="94"/>
<wire x1="6.8512875" y1="-9.770734375" x2="6.741671875" y2="-9.79948125" width="0.5" layer="94"/>
<wire x1="6.741671875" y1="-9.79948125" x2="6.3019" y2="-9.913459375" width="0.5" layer="94"/>
<wire x1="6.3019" y1="-9.913459375" x2="4.22625" y2="-10.359590625" width="0.5" layer="94"/>
<wire x1="4.22625" y1="-10.359590625" x2="2.345390625" y2="-10.63799375" width="0.5" layer="94"/>
<wire x1="2.345390625" y1="-10.63799375" x2="2.0036375" y2="-10.668171875" width="0.5" layer="94"/>
<wire x1="2.0036375" y1="-10.668171875" x2="1.76015" y2="-10.690121875" width="0.5" layer="94"/>
<wire x1="1.76015" y1="-10.690121875" x2="1.0267" y2="-10.7304" width="0.5" layer="94"/>
<wire x1="1.0267" y1="-10.7304" x2="0.15258125" y2="-10.737196875" width="0.5" layer="94"/>
<wire x1="0.15258125" y1="-10.737196875" x2="-0.608928125" y2="-10.6985375" width="0.5" layer="94"/>
<wire x1="-0.608928125" y1="-10.6985375" x2="-1.08860625" y2="-10.64921875" width="0.5" layer="94"/>
<wire x1="-1.08860625" y1="-10.64921875" x2="-1.24723125" y2="-10.6248375" width="0.5" layer="94"/>
<wire x1="-1.24723125" y1="-10.6248375" x2="-1.271546875" y2="-10.62215625" width="0.5" layer="94"/>
<wire x1="-1.271546875" y1="-10.62215625" x2="-1.495515625" y2="-10.581878125" width="0.5" layer="94"/>
<wire x1="-1.495515625" y1="-10.581878125" x2="-1.516465625" y2="-10.5782" width="0.5" layer="94"/>
<wire x1="-1.516465625" y1="-10.5782" x2="-1.537790625" y2="-10.573771875" width="0.5" layer="94"/>
<wire x1="-1.537790625" y1="-10.573771875" x2="-1.735696875" y2="-10.534553125" width="0.5" layer="94"/>
<wire x1="-1.735696875" y1="-10.534553125" x2="-1.75359375" y2="-10.5304375" width="0.5" layer="94"/>
<wire x1="-1.75359375" y1="-10.5304375" x2="-1.789446875" y2="-10.520646875" width="0.5" layer="94"/>
<wire x1="-1.789446875" y1="-10.520646875" x2="-2.095346875" y2="-10.44015" width="0.5" layer="94"/>
<wire x1="-2.095346875" y1="-10.44015" x2="-2.119290625" y2="-10.431671875" width="0.5" layer="94"/>
<wire x1="-2.119290625" y1="-10.431671875" x2="-2.1315125" y2="-10.42768125" width="0.5" layer="94"/>
<wire x1="-2.1315125" y1="-10.42768125" x2="-2.23906875" y2="-10.393821875" width="0.5" layer="94"/>
<wire x1="-2.23906875" y1="-10.393821875" x2="-2.248171875" y2="-10.391140625" width="0.5" layer="94"/>
<wire x1="-2.248171875" y1="-10.391140625" x2="-2.2573375" y2="-10.3874" width="0.5" layer="94"/>
<wire x1="-2.2573375" y1="-10.3874" x2="-2.33409375" y2="-10.35796875" width="0.5" layer="94"/>
<wire x1="-2.33409375" y1="-10.35796875" x2="-2.34020625" y2="-10.3556" width="0.5" layer="94"/>
<wire x1="-2.34020625" y1="-10.3556" x2="-2.346628125" y2="-10.35316875" width="0.5" layer="94"/>
<wire x1="-2.346628125" y1="-10.35316875" x2="-2.39264375" y2="-10.3342125" width="0.5" layer="94"/>
<wire x1="-2.39264375" y1="-10.3342125" x2="-2.395325" y2="-10.332903125" width="0.5" layer="94"/>
<wire x1="-2.395325" y1="-10.332903125" x2="-2.39806875" y2="-10.331534375" width="0.5" layer="94"/>
<wire x1="-2.39806875" y1="-10.331534375" x2="-2.41296875" y2="-10.32616875" width="0.5" layer="94"/>
<wire x1="-2.41359375" y1="-10.32616875" x2="-2.41296875" y2="-10.32616875" width="0.5" layer="94"/>
<wire x1="-2.41296875" y1="-10.32616875" x2="-2.4000625" y2="-10.335275" width="0.5" layer="94"/>
<wire x1="-2.4000625" y1="-10.335275" x2="-2.39806875" y2="-10.33733125" width="0.5" layer="94"/>
<wire x1="-2.39806875" y1="-10.33733125" x2="-2.3956375" y2="-10.339015625" width="0.5" layer="94"/>
<wire x1="-2.3956375" y1="-10.339015625" x2="-2.354421875" y2="-10.367384375" width="0.5" layer="94"/>
<wire x1="-2.354421875" y1="-10.367384375" x2="-2.348621875" y2="-10.371125" width="0.5" layer="94"/>
<wire x1="-2.348621875" y1="-10.371125" x2="-2.343259375" y2="-10.37449375" width="0.5" layer="94"/>
<wire x1="-2.343259375" y1="-10.37449375" x2="-2.2738625" y2="-10.420196875" width="0.5" layer="94"/>
<wire x1="-2.2738625" y1="-10.420196875" x2="-2.26538125" y2="-10.425559375" width="0.5" layer="94"/>
<wire x1="-2.26538125" y1="-10.425559375" x2="-2.257028125" y2="-10.4303625" width="0.5" layer="94"/>
<wire x1="-2.257028125" y1="-10.4303625" x2="-2.1568875" y2="-10.486478125" width="0.5" layer="94"/>
<wire x1="-2.1568875" y1="-10.486478125" x2="-2.145353125" y2="-10.4932125" width="0.5" layer="94"/>
<wire x1="-2.145353125" y1="-10.4932125" x2="-2.12303125" y2="-10.50680625" width="0.5" layer="94"/>
<wire x1="-2.12303125" y1="-10.50680625" x2="-1.834153125" y2="-10.64953125" width="0.5" layer="94"/>
<wire x1="-1.834153125" y1="-10.64953125" x2="-1.799671875" y2="-10.666428125" width="0.5" layer="94"/>
<wire x1="-1.799671875" y1="-10.666428125" x2="-1.7823375" y2="-10.674221875" width="0.5" layer="94"/>
<wire x1="-1.7823375" y1="-10.674221875" x2="-1.591540625" y2="-10.7524125" width="0.5" layer="94"/>
<wire x1="-1.591540625" y1="-10.7524125" x2="-1.5709625" y2="-10.760828125" width="0.5" layer="94"/>
<wire x1="-1.5709625" y1="-10.760828125" x2="-1.5507" y2="-10.768934375" width="0.5" layer="94"/>
<wire x1="-1.5507" y1="-10.768934375" x2="-1.33071875" y2="-10.853859375" width="0.5" layer="94"/>
<wire x1="-1.33071875" y1="-10.853859375" x2="-1.3067125" y2="-10.861278125" width="0.5" layer="94"/>
<wire x1="-1.3067125" y1="-10.861278125" x2="-1.150771875" y2="-10.9164625" width="0.5" layer="94"/>
<wire x1="-1.150771875" y1="-10.9164625" x2="-0.672153125" y2="-11.054821875" width="0.5" layer="94"/>
<wire x1="-0.672153125" y1="-11.054821875" x2="0.100515625" y2="-11.2280375" width="0.5" layer="94"/>
<wire x1="0.100515625" y1="-11.2280375" x2="1.0006375" y2="-11.3643375" width="0.5" layer="94"/>
<wire x1="1.0006375" y1="-11.3643375" x2="1.764203125" y2="-11.43535625" width="0.5" layer="94"/>
<wire x1="1.764203125" y1="-11.43535625" x2="2.019225" y2="-11.447578125" width="0.5" layer="94"/>
<wire x1="2.019225" y1="-11.447578125" x2="2.3525" y2="-11.464475" width="0.5" layer="94"/>
<wire x1="2.3525" y1="-11.464475" x2="4.188340625" y2="-11.42825" width="0.5" layer="94"/>
<wire x1="4.188340625" y1="-11.42825" x2="6.213296875" y2="-11.218496875" width="0.5" layer="94"/>
<wire x1="6.213296875" y1="-11.218496875" x2="6.641221875" y2="-11.148846875" width="0.5" layer="94"/>
<wire x1="6.641221875" y1="-11.148846875" x2="6.2488375" y2="-11.433675" width="0.5" layer="94"/>
<wire x1="6.2488375" y1="-11.433675" x2="4.98426875" y2="-12.168434375" width="0.5" layer="94"/>
<wire x1="4.98426875" y1="-12.168434375" x2="3.171809375" y2="-12.928509375" width="0.5" layer="94"/>
<wire x1="3.171809375" y1="-12.928509375" x2="1.22971875" y2="-13.4035125" width="0.5" layer="94"/>
<wire x1="1.22971875" y1="-13.4035125" x2="-0.305459375" y2="-13.567559375" width="0.5" layer="94"/>
<wire x1="-0.305459375" y1="-13.567559375" x2="-1.240121875" y2="-13.567559375" width="0.5" layer="94"/>
<wire x1="-1.240121875" y1="-13.567559375" x2="-2.509678125" y2="-13.455575" width="0.5" layer="94"/>
<wire x1="-2.509678125" y1="-13.455575" x2="-4.13476875" y2="-13.129846875" width="0.5" layer="94"/>
<wire x1="-4.13476875" y1="-13.129846875" x2="-5.678053125" y2="-12.603778125" width="0.5" layer="94"/>
<wire x1="-5.678053125" y1="-12.603778125" x2="-6.77608125" y2="-12.092675" width="0.5" layer="94"/>
<wire x1="-6.77608125" y1="-12.092675" x2="-7.124878125" y2="-11.892775" width="0.5" layer="94"/>
<wire x1="-7.124878125" y1="-11.892775" x2="-7.124878125" y2="-7.16085" width="0.5" layer="94"/>
<wire x1="-7.124878125" y1="-7.16085" x2="-7.067078125" y2="-7.1452625" width="0.5" layer="94"/>
<wire x1="-7.067078125" y1="-7.1452625" x2="-6.797715625" y2="-7.02579375" width="0.5" layer="94"/>
<wire x1="-6.797715625" y1="-7.02579375" x2="-6.562959375" y2="-6.8553875" width="0.5" layer="94"/>
<wire x1="-6.562959375" y1="-6.8553875" x2="-6.52205625" y2="-6.814421875" width="0.5" layer="94"/>
<wire x1="-6.52205625" y1="-6.814421875" x2="-6.4774125" y2="-6.769775" width="0.5" layer="94"/>
<wire x1="-6.4774125" y1="-6.769775" x2="-6.3606875" y2="-6.618196875" width="0.5" layer="94"/>
<wire x1="-6.3606875" y1="-6.618196875" x2="-6.238540625" y2="-6.39260625" width="0.5" layer="94"/>
<wire x1="-6.238540625" y1="-6.39260625" x2="-6.161096875" y2="-6.143259375" width="0.5" layer="94"/>
<wire x1="-6.161096875" y1="-6.143259375" x2="-6.1340375" y2="-5.942984375" width="0.5" layer="94"/>
<wire x1="-6.1340375" y1="-5.942984375" x2="-6.1340375" y2="-5.8093625" width="0.5" layer="94"/>
<wire x1="-6.1340375" y1="-5.8093625" x2="-6.161096875" y2="-5.609459375" width="0.5" layer="94"/>
<wire x1="-6.161096875" y1="-5.609459375" x2="-6.238540625" y2="-5.360175" width="0.5" layer="94"/>
<wire x1="-6.238540625" y1="-5.360175" x2="-6.3606875" y2="-5.134459375" width="0.5" layer="94"/>
<wire x1="-6.3606875" y1="-5.134459375" x2="-6.4774125" y2="-4.98294375" width="0.5" layer="94"/>
<wire x1="-6.4774125" y1="-4.98294375" x2="-6.52205625" y2="-4.938671875" width="0.5" layer="94"/>
<wire x1="-6.52205625" y1="-4.938671875" x2="-6.5667" y2="-4.89365625" width="0.5" layer="94"/>
<wire x1="-6.5667" y1="-4.89365625" x2="-6.718215625" y2="-4.77699375" width="0.5" layer="94"/>
<wire x1="-6.718215625" y1="-4.77699375" x2="-6.94424375" y2="-4.654784375" width="0.5" layer="94"/>
<wire x1="-6.94424375" y1="-4.654784375" x2="-7.193903125" y2="-4.577340625" width="0.5" layer="94"/>
<wire x1="-7.193903125" y1="-4.577340625" x2="-7.394115625" y2="-4.550590625" width="0.5" layer="94"/>
<wire x1="-7.394115625" y1="-4.550590625" x2="-7.5278" y2="-4.550590625" width="0.5" layer="94"/>
<wire x1="-7.5278" y1="-4.550590625" x2="-7.727703125" y2="-4.577340625" width="0.5" layer="94"/>
<wire x1="-7.727703125" y1="-4.577340625" x2="-7.97705" y2="-4.654784375" width="0.5" layer="94"/>
<wire x1="-7.97705" y1="-4.654784375" x2="-8.202640625" y2="-4.77699375" width="0.5" layer="94"/>
<wire x1="-8.202640625" y1="-4.77699375" x2="-8.35384375" y2="-4.893965625" width="0.5" layer="94"/>
<wire x1="-8.35384375" y1="-4.893965625" x2="-8.398553125" y2="-4.938671875" width="0.5" layer="94"/>
<wire x1="-8.398553125" y1="-4.938671875" x2="-8.443134375" y2="-4.98325625" width="0.5" layer="94"/>
<wire x1="-8.443134375" y1="-4.98325625" x2="-8.56023125" y2="-5.134459375" width="0.5" layer="94"/>
<wire x1="-8.56023125" y1="-5.134459375" x2="-8.682690625" y2="-5.3598" width="0.5" layer="94"/>
<wire x1="-8.682690625" y1="-5.3598" x2="-8.760196875" y2="-5.60915" width="0.5" layer="94"/>
<wire x1="-8.760196875" y1="-5.60915" x2="-8.78688125" y2="-5.8093625" width="0.5" layer="94"/>
<wire x1="-8.78688125" y1="-5.8093625" x2="-8.786509375" y2="-5.876390625" width="0.5" layer="94"/>
<wire x1="-8.786509375" y1="-5.876390625" x2="-8.78688125" y2="-5.942984375" width="0.5" layer="94"/>
<wire x1="-8.78688125" y1="-5.942984375" x2="-8.760196875" y2="-6.143259375" width="0.5" layer="94"/>
<wire x1="-8.760196875" y1="-6.143259375" x2="-8.682690625" y2="-6.39260625" width="0.5" layer="94"/>
<wire x1="-8.682690625" y1="-6.39260625" x2="-8.56023125" y2="-6.618196875" width="0.5" layer="94"/>
<wire x1="-8.56023125" y1="-6.618196875" x2="-8.443509375" y2="-6.769403125" width="0.5" layer="94"/>
<wire x1="-8.443509375" y1="-6.769403125" x2="-8.398553125" y2="-6.814421875" width="0.5" layer="94"/>
<wire x1="-8.398553125" y1="-6.814421875" x2="-8.358959375" y2="-6.8540125" width="0.5" layer="94"/>
<wire x1="-8.358959375" y1="-6.8540125" x2="-8.13230625" y2="-7.020434375" width="0.5" layer="94"/>
<wire x1="-8.13230625" y1="-7.020434375" x2="-7.87348125" y2="-7.138153125" width="0.5" layer="94"/>
<wire x1="-7.87348125" y1="-7.138153125" x2="-7.8177375" y2="-7.153740625" width="0.5" layer="94"/>
<wire x1="-7.8177375" y1="-7.153740625" x2="-7.8177375" y2="-11.465846875" width="0.5" layer="94"/>
<wire x1="-7.8177375" y1="-11.465846875" x2="-8.081553125" y2="-11.2908875" width="0.5" layer="94"/>
<wire x1="-8.081553125" y1="-11.2908875" x2="-8.838321875" y2="-10.7145" width="0.5" layer="94"/>
<wire x1="-8.838321875" y1="-10.7145" x2="-9.7774125" y2="-9.86744375" width="0.5" layer="94"/>
<wire x1="-9.7774125" y1="-9.86744375" x2="-10.628521875" y2="-8.93209375" width="0.5" layer="94"/>
<wire x1="-10.628521875" y1="-8.93209375" x2="-11.208275" y2="-8.17806875" width="0.5" layer="94"/>
<wire x1="-11.208275" y1="-8.17806875" x2="-11.383859375" y2="-7.915190625" width="0.5" layer="94"/>
<wire x1="-11.383859375" y1="-7.915190625" x2="-11.36459375" y2="-3.238759375" width="0.5" layer="94"/>
<wire x1="-11.36459375" y1="-3.238759375" x2="-10.955934375" y2="-3.238759375" width="0.5" layer="94"/>
<wire x1="-10.955934375" y1="-3.238759375" x2="-10.955934375" y2="-3.20328125" width="0.5" layer="94"/>
<wire x1="-10.955934375" y1="-3.20328125" x2="-10.9126625" y2="-3.2266" width="0.5" layer="94"/>
<wire x1="-10.9126625" y1="-3.2266" x2="-8.7997875" y2="0.62540625" width="0.5" layer="94"/>
<wire x1="-8.7997875" y1="0.62540625" x2="-7.361009375" y2="0.62540625" width="0.5" layer="94"/>
<wire x1="-7.361009375" y1="0.62540625" x2="-7.3477875" y2="0.560121875" width="0.5" layer="94"/>
<wire x1="-7.3477875" y1="0.560121875" x2="-7.228071875" y2="0.256653125" width="0.5" layer="94"/>
<wire x1="-7.228071875" y1="0.256653125" x2="-7.043696875" y2="-0.006471875" width="0.5" layer="94"/>
<wire x1="-7.043696875" y1="-0.006471875" x2="-6.998365625" y2="-0.051803125" width="0.5" layer="94"/>
<wire x1="-6.998365625" y1="-0.051803125" x2="-6.954034375" y2="-0.096509375" width="0.5" layer="94"/>
<wire x1="-6.954034375" y1="-0.096509375" x2="-6.802515625" y2="-0.213484375" width="0.5" layer="94"/>
<wire x1="-6.802515625" y1="-0.213484375" x2="-6.576553125" y2="-0.33594375" width="0.5" layer="94"/>
<wire x1="-6.576553125" y1="-0.33594375" x2="-6.32720625" y2="-0.413446875" width="0.5" layer="94"/>
<wire x1="-6.32720625" y1="-0.413446875" x2="-6.127240625" y2="-0.440446875" width="0.5" layer="94"/>
<wire x1="-6.127240625" y1="-0.440446875" x2="-5.99361875" y2="-0.440446875" width="0.5" layer="94"/>
<wire x1="-5.99361875" y1="-0.440446875" x2="-5.79340625" y2="-0.413446875" width="0.5" layer="94"/>
<wire x1="-5.79340625" y1="-0.413446875" x2="-5.544121875" y2="-0.33594375" width="0.5" layer="94"/>
<wire x1="-5.544121875" y1="-0.33594375" x2="-5.31809375" y2="-0.213484375" width="0.5" layer="94"/>
<wire x1="-5.31809375" y1="-0.213484375" x2="-5.166515625" y2="-0.096509375" width="0.5" layer="94"/>
<wire x1="-5.166515625" y1="-0.096509375" x2="-5.121559375" y2="-0.051803125" width="0.5" layer="94"/>
<wire x1="-5.121559375" y1="-0.051803125" x2="-5.0769125" y2="-0.007096875" width="0.5" layer="94"/>
<wire x1="-5.0769125" y1="-0.007096875" x2="-4.960253125" y2="0.14441875" width="0.5" layer="94"/>
<wire x1="-4.960253125" y1="0.14441875" x2="-4.838040625" y2="0.370009375" width="0.5" layer="94"/>
<wire x1="-4.838040625" y1="0.370009375" x2="-4.7606" y2="0.619359375" width="0.5" layer="94"/>
<wire x1="-4.7606" y1="0.619359375" x2="-4.733540625" y2="0.819571875" width="0.5" layer="94"/>
<wire x1="-4.733540625" y1="0.819571875" x2="-4.733540625" y2="0.953190625" width="0.5" layer="94"/>
<wire x1="-4.733540625" y1="0.953190625" x2="-4.7606" y2="1.15353125" width="0.5" layer="94"/>
<wire x1="-4.7606" y1="1.15353125" x2="-4.838040625" y2="1.402815625" width="0.5" layer="94"/>
<wire x1="-4.838040625" y1="1.402815625" x2="-4.960253125" y2="1.62809375" width="0.5" layer="94"/>
<wire x1="-4.960253125" y1="1.62809375" x2="-5.0769125" y2="1.7793" width="0.5" layer="94"/>
<wire x1="-5.0769125" y1="1.7793" x2="-5.121559375" y2="1.82400625" width="0.5" layer="94"/>
<wire x1="-5.121559375" y1="1.82400625" x2="-5.166515625" y2="1.869025" width="0.5" layer="94"/>
<wire x1="-5.166515625" y1="1.869025" x2="-5.31809375" y2="1.985996875" width="0.5" layer="94"/>
<wire x1="-5.31809375" y1="1.985996875" x2="-5.544121875" y2="2.108459375" width="0.5" layer="94"/>
<wire x1="-5.544121875" y1="2.108459375" x2="-5.79340625" y2="2.1863375" width="0.5" layer="94"/>
<wire x1="-5.79340625" y1="2.1863375" x2="-5.99361875" y2="2.212959375" width="0.5" layer="94"/>
<wire x1="-5.99361875" y1="2.212959375" x2="-6.127240625" y2="2.212959375" width="0.5" layer="94"/>
<wire x1="-6.127240625" y1="2.212959375" x2="-6.32720625" y2="2.1863375" width="0.5" layer="94"/>
<wire x1="-6.32720625" y1="2.1863375" x2="-6.576553125" y2="2.108459375" width="0.5" layer="94"/>
<wire x1="-6.576553125" y1="2.108459375" x2="-6.802515625" y2="1.985996875" width="0.5" layer="94"/>
<wire x1="-6.802515625" y1="1.985996875" x2="-6.953721875" y2="1.869025" width="0.5" layer="94"/>
<wire x1="-6.953721875" y1="1.869025" x2="-6.998365625" y2="1.82400625" width="0.5" layer="94"/>
<wire x1="-6.998365625" y1="1.82400625" x2="-7.030540625" y2="1.79220625" width="0.5" layer="94"/>
<wire x1="-7.030540625" y1="1.79220625" x2="-7.170521875" y2="1.61256875" width="0.5" layer="94"/>
<wire x1="-7.170521875" y1="1.61256875" x2="-7.2795125" y2="1.410296875" width="0.5" layer="94"/>
<wire x1="-7.2795125" y1="1.410296875" x2="-7.2960375" y2="1.3669625" width="0.5" layer="94"/>
<wire x1="-7.2960375" y1="1.3669625" x2="-8.392378125" y2="1.3669625" width="0.5" layer="94"/>
<wire x1="-8.392378125" y1="1.3669625" x2="-5.659471875" y2="6.34991875" width="0.5" layer="94"/>
<wire x1="-5.659471875" y1="6.34991875" x2="-1.4031125" y2="-1.871121875" width="0.5" layer="94"/>
<wire x1="-1.4031125" y1="-1.871121875" x2="-1.445446875" y2="-1.926615625" width="0.5" layer="94"/>
<wire x1="-1.445446875" y1="-1.926615625" x2="-1.601703125" y2="-2.243240625" width="0.5" layer="94"/>
<wire x1="-1.601703125" y1="-2.243240625" x2="-1.673159375" y2="-2.565915625" width="0.5" layer="94"/>
<wire x1="-1.673159375" y1="-2.565915625" x2="-1.673159375" y2="-2.740503125" width="0.5" layer="94"/>
<wire x1="-1.673159375" y1="-2.740503125" x2="-1.646409375" y2="-2.940715625" width="0.5" layer="94"/>
<wire x1="-1.646409375" y1="-2.940715625" x2="-1.56890625" y2="-3.19" width="0.5" layer="94"/>
<wire x1="-1.56890625" y1="-3.19" x2="-1.44675625" y2="-3.415715625" width="0.5" layer="94"/>
<wire x1="-1.44675625" y1="-3.415715625" x2="-1.330096875" y2="-3.56691875" width="0.5" layer="94"/>
<wire x1="-1.330096875" y1="-3.56691875" x2="-1.285078125" y2="-3.611875" width="0.5" layer="94"/>
<wire x1="-1.285078125" y1="-3.611875" x2="-1.240371875" y2="-3.656521875" width="0.5" layer="94"/>
<wire x1="-1.240371875" y1="-3.656521875" x2="-1.08885625" y2="-3.77361875" width="0.5" layer="94"/>
<wire x1="-1.08885625" y1="-3.77361875" x2="-0.862890625" y2="-3.896078125" width="0.5" layer="94"/>
<wire x1="-0.862890625" y1="-3.896078125" x2="-0.61360625" y2="-3.97351875" width="0.5" layer="94"/>
<wire x1="-0.61360625" y1="-3.97351875" x2="-0.413390625" y2="-4.00058125" width="0.5" layer="94"/>
<wire x1="-0.413390625" y1="-4.00058125" x2="-0.27970625" y2="-4.00058125" width="0.5" layer="94"/>
<wire x1="-0.27970625" y1="-4.00058125" x2="-0.07980625" y2="-3.97351875" width="0.5" layer="94"/>
<wire x1="-0.07980625" y1="-3.97351875" x2="0.169165625" y2="-3.896078125" width="0.5" layer="94"/>
<wire x1="0.169165625" y1="-3.896078125" x2="0.395134375" y2="-3.77361875" width="0.5" layer="94"/>
<wire x1="0.395134375" y1="-3.77361875" x2="0.5467125" y2="-3.656521875" width="0.5" layer="94"/>
<wire x1="0.5467125" y1="-3.656521875" x2="0.59135625" y2="-3.611875" width="0.5" layer="94"/>
<wire x1="0.59135625" y1="-3.611875" x2="0.636" y2="-3.56723125" width="0.5" layer="94"/>
<wire x1="0.636" y1="-3.56723125" x2="0.752725" y2="-3.415715625" width="0.5" layer="94"/>
<wire x1="0.752725" y1="-3.415715625" x2="0.874809375" y2="-3.19" width="0.5" layer="94"/>
<wire x1="0.874809375" y1="-3.19" x2="0.9523125" y2="-2.940715625" width="0.5" layer="94"/>
<wire x1="0.9523125" y1="-2.940715625" x2="0.979375" y2="-2.740503125" width="0.5" layer="94"/>
<wire x1="0.979375" y1="-2.740503125" x2="0.979375" y2="-2.60688125" width="0.5" layer="94"/>
<wire x1="0.979375" y1="-2.60688125" x2="0.9523125" y2="-2.406540625" width="0.5" layer="94"/>
<wire x1="0.9523125" y1="-2.406540625" x2="0.874809375" y2="-2.15731875" width="0.5" layer="94"/>
<wire x1="0.874809375" y1="-2.15731875" x2="0.752725" y2="-1.931978125" width="0.5" layer="94"/>
<wire x1="0.752725" y1="-1.931978125" x2="0.636" y2="-1.7804" width="0.5" layer="94"/>
<wire x1="0.636" y1="-1.7804" x2="0.59135625" y2="-1.736065625" width="0.5" layer="94"/>
<wire x1="0.59135625" y1="-1.736065625" x2="0.56498125" y2="-1.709753125" width="0.5" layer="94"/>
<wire x1="0.56498125" y1="-1.709753125" x2="0.259203125" y2="-1.493265625" width="0.5" layer="94"/>
<wire x1="0.259203125" y1="-1.493265625" x2="0.224971875" y2="-1.47699375" width="0.5" layer="94"/>
<wire x1="0.224971875" y1="-1.47699375" x2="0.224971875" y2="6.07008125" width="0.5" layer="94"/>
<wire x1="0.224971875" y1="6.07008125" x2="5.07461875" y2="0.96341875" width="0.5" layer="94"/>
<wire x1="5.07461875" y1="0.96341875" x2="9.300803125" y2="5.915884375" width="0.5" layer="94"/>
<wire x1="9.300803125" y1="5.915884375" x2="9.300803125" y2="-2.3034125" width="0.5" layer="94"/>
<wire x1="9.300803125" y1="-2.3034125" x2="7.124640625" y2="-2.3034125" width="0.5" layer="94"/>
<wire x1="7.124640625" y1="-2.3034125" x2="7.108740625" y2="-2.248915625" width="0.5" layer="94"/>
<wire x1="7.108740625" y1="-2.248915625" x2="6.99133125" y2="-1.9962625" width="0.5" layer="94"/>
<wire x1="6.99133125" y1="-1.9962625" x2="6.82790625" y2="-1.774725" width="0.5" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LOGO" urn="urn:adsk.eagle:component:8516701/2" library_version="2">
<gates>
<gate name="G$1" symbol="LOGO" x="0" y="0"/>
</gates>
<devices>
<device name="8MM" package="LOGO-8MM">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8516700/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="E-Switch" urn="urn:adsk.eagle:library:8712447">
<packages>
<package name="TL3301-4.3MM" urn="urn:adsk.eagle:footprint:8712448/2" library_version="5">
<smd name="P$1" x="-4.55" y="2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$2" x="4.55" y="2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$3" x="-4.55" y="-2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$4" x="4.55" y="-2.25" dx="2.1" dy="1.4" layer="1"/>
<wire x1="3" y1="3" x2="-2.5" y2="3" width="0.254" layer="21"/>
<wire x1="-2.5" y1="3" x2="-3" y2="2.5" width="0.254" layer="21"/>
<wire x1="-3" y1="2.5" x2="-3" y2="-3" width="0.254" layer="21"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.254" layer="21"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.254" layer="21"/>
<circle x="0" y="0" radius="1.75" width="0.254" layer="21"/>
<text x="-3" y="3.25" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="TL3301-5MM" urn="urn:adsk.eagle:footprint:8712463/1" library_version="5">
<smd name="P$1" x="-4.55" y="2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$2" x="4.55" y="2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$3" x="-4.55" y="-2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$4" x="4.55" y="-2.25" dx="2.1" dy="1.4" layer="1"/>
<wire x1="3" y1="3" x2="-2.5" y2="3" width="0.254" layer="21"/>
<wire x1="-2.5" y1="3" x2="-3" y2="2.5" width="0.254" layer="21"/>
<wire x1="-3" y1="2.5" x2="-3" y2="-3" width="0.254" layer="21"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.254" layer="21"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.254" layer="21"/>
<circle x="0" y="0" radius="1.75" width="0.254" layer="21"/>
<text x="-3" y="3.25" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="TL3301-6MM" urn="urn:adsk.eagle:footprint:8712462/1" library_version="5">
<smd name="P$1" x="-4.55" y="2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$2" x="4.55" y="2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$3" x="-4.55" y="-2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$4" x="4.55" y="-2.25" dx="2.1" dy="1.4" layer="1"/>
<wire x1="3" y1="3" x2="-2.5" y2="3" width="0.254" layer="21"/>
<wire x1="-2.5" y1="3" x2="-3" y2="2.5" width="0.254" layer="21"/>
<wire x1="-3" y1="2.5" x2="-3" y2="-3" width="0.254" layer="21"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.254" layer="21"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.254" layer="21"/>
<circle x="0" y="0" radius="1.75" width="0.254" layer="21"/>
<text x="-3" y="3.25" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="TL3301-7MM" urn="urn:adsk.eagle:footprint:8712461/1" library_version="5">
<smd name="P$1" x="-4.55" y="2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$2" x="4.55" y="2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$3" x="-4.55" y="-2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$4" x="4.55" y="-2.25" dx="2.1" dy="1.4" layer="1"/>
<wire x1="3" y1="3" x2="-2.5" y2="3" width="0.254" layer="21"/>
<wire x1="-2.5" y1="3" x2="-3" y2="2.5" width="0.254" layer="21"/>
<wire x1="-3" y1="2.5" x2="-3" y2="-3" width="0.254" layer="21"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.254" layer="21"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.254" layer="21"/>
<circle x="0" y="0" radius="1.75" width="0.254" layer="21"/>
<text x="-3" y="3.25" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="TL3301-8MM" urn="urn:adsk.eagle:footprint:8712460/1" library_version="5">
<smd name="P$1" x="-4.55" y="2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$2" x="4.55" y="2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$3" x="-4.55" y="-2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$4" x="4.55" y="-2.25" dx="2.1" dy="1.4" layer="1"/>
<wire x1="3" y1="3" x2="-2.5" y2="3" width="0.254" layer="21"/>
<wire x1="-2.5" y1="3" x2="-3" y2="2.5" width="0.254" layer="21"/>
<wire x1="-3" y1="2.5" x2="-3" y2="-3" width="0.254" layer="21"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.254" layer="21"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.254" layer="21"/>
<circle x="0" y="0" radius="1.75" width="0.254" layer="21"/>
<text x="-3" y="3.25" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="TL3301-9MM" urn="urn:adsk.eagle:footprint:8712459/1" library_version="5">
<smd name="P$1" x="-4.55" y="2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$2" x="4.55" y="2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$3" x="-4.55" y="-2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$4" x="4.55" y="-2.25" dx="2.1" dy="1.4" layer="1"/>
<wire x1="3" y1="3" x2="-2.5" y2="3" width="0.254" layer="21"/>
<wire x1="-2.5" y1="3" x2="-3" y2="2.5" width="0.254" layer="21"/>
<wire x1="-3" y1="2.5" x2="-3" y2="-3" width="0.254" layer="21"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.254" layer="21"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.254" layer="21"/>
<circle x="0" y="0" radius="1.75" width="0.254" layer="21"/>
<text x="-3" y="3.25" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="TL3301-10MM" urn="urn:adsk.eagle:footprint:8712458/1" library_version="5">
<smd name="P$1" x="-4.55" y="2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$2" x="4.55" y="2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$3" x="-4.55" y="-2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="P$4" x="4.55" y="-2.25" dx="2.1" dy="1.4" layer="1"/>
<wire x1="3" y1="3" x2="-2.5" y2="3" width="0.254" layer="21"/>
<wire x1="-2.5" y1="3" x2="-3" y2="2.5" width="0.254" layer="21"/>
<wire x1="-3" y1="2.5" x2="-3" y2="-3" width="0.254" layer="21"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.254" layer="21"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.254" layer="21"/>
<circle x="0" y="0" radius="1.75" width="0.254" layer="21"/>
<text x="-3" y="3.25" size="1.016" layer="21">&gt;NAME</text>
</package>
</packages>
<packages3d>
<package3d name="TL3301-4.3MM" urn="urn:adsk.eagle:package:8712472/2" type="model" library_version="5">
<packageinstances>
<packageinstance name="TL3301-4.3MM"/>
</packageinstances>
</package3d>
<package3d name="TL3301-5MM" urn="urn:adsk.eagle:package:8712471/2" type="model" library_version="5">
<packageinstances>
<packageinstance name="TL3301-5MM"/>
</packageinstances>
</package3d>
<package3d name="TL3301-6MM" urn="urn:adsk.eagle:package:8712470/2" type="model" library_version="5">
<packageinstances>
<packageinstance name="TL3301-6MM"/>
</packageinstances>
</package3d>
<package3d name="TL3301-7MM" urn="urn:adsk.eagle:package:8712469/2" type="model" library_version="5">
<packageinstances>
<packageinstance name="TL3301-7MM"/>
</packageinstances>
</package3d>
<package3d name="TL3301-8MM" urn="urn:adsk.eagle:package:8712468/2" type="model" library_version="5">
<packageinstances>
<packageinstance name="TL3301-8MM"/>
</packageinstances>
</package3d>
<package3d name="TL3301-9MM" urn="urn:adsk.eagle:package:8712467/2" type="model" library_version="5">
<packageinstances>
<packageinstance name="TL3301-9MM"/>
</packageinstances>
</package3d>
<package3d name="TL3301-10MM" urn="urn:adsk.eagle:package:8712466/2" type="model" library_version="5">
<packageinstances>
<packageinstance name="TL3301-10MM"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="TL3301" urn="urn:adsk.eagle:symbol:8712449/2" library_version="5">
<pin name="P$1" x="-5.08" y="2.54" visible="off" length="short"/>
<pin name="P$2" x="5.08" y="2.54" visible="off" length="short" rot="R180"/>
<pin name="P$3" x="-5.08" y="-2.54" visible="off" length="short"/>
<pin name="P$4" x="5.08" y="-2.54" visible="off" length="short" rot="R180"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.143" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.143" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="0.635" width="0.254" layer="94"/>
<wire x1="1.905" y1="0.635" x2="1.905" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.905" y1="-0.635" x2="1.27" y2="-0.635" width="0.254" layer="94"/>
<circle x="0" y="0.889" radius="0.254" width="0.254" layer="94"/>
<circle x="0" y="-0.889" radius="0.254" width="0.254" layer="94"/>
<text x="-2.54" y="3.175" size="1.016" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TL3301" urn="urn:adsk.eagle:component:8712451/3" prefix="SW" library_version="5">
<gates>
<gate name="G$1" symbol="TL3301" x="0" y="0"/>
</gates>
<devices>
<device name="6X6-4.3MM" package="TL3301-4.3MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8712472/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6X6-5MM" package="TL3301-5MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8712471/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6X6-6MM" package="TL3301-6MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8712470/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6X6-7MM" package="TL3301-7MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8712469/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6X6-8MM" package="TL3301-8MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8712468/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6X6-9MM" package="TL3301-9MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8712467/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6X6-10MM" package="TL3301-10MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8712466/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6X6-11MM" package="TL3301-10MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8712466/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6X6-12MM" package="TL3301-10MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8712466/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SII" urn="urn:adsk.eagle:library:8712646">
<packages>
<package name="SOT23-5P" urn="urn:adsk.eagle:footprint:8577890/2" library_version="3">
<smd name="P$1" x="-0.95" y="-1.1" dx="1.06" dy="0.65" layer="1" rot="R90"/>
<smd name="P$2" x="0" y="-1.1" dx="1.06" dy="0.65" layer="1" rot="R90"/>
<smd name="P$3" x="0.95" y="-1.1" dx="1.06" dy="0.65" layer="1" rot="R90"/>
<smd name="P$4" x="0.95" y="1.1" dx="1.06" dy="0.65" layer="1" rot="R90"/>
<smd name="P$5" x="-0.95" y="1.1" dx="1.06" dy="0.65" layer="1" rot="R90"/>
<wire x1="-1.45" y1="0.8" x2="1.45" y2="0.8" width="0.254" layer="21"/>
<wire x1="1.45" y1="0.8" x2="1.45" y2="-0.8" width="0.254" layer="21"/>
<wire x1="1.45" y1="-0.8" x2="-1.45" y2="-0.8" width="0.254" layer="21"/>
<wire x1="-1.45" y1="-0.8" x2="-1.45" y2="0.8" width="0.254" layer="21"/>
<circle x="-1.778" y="-1.27" radius="0.15" width="0.254" layer="21"/>
<text x="-2.5" y="2" size="1.016" layer="21">&gt;NAME</text>
</package>
</packages>
<packages3d>
<package3d name="SOT95P280X120-5" urn="urn:adsk.eagle:package:8577893/3" type="model" library_version="3">
<packageinstances>
<packageinstance name="SOT23-5P"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="OPAMP" urn="urn:adsk.eagle:symbol:8712647/2" library_version="3">
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="0" width="0.4064" layer="94"/>
<wire x1="5.08" y1="0" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="3.175" x2="-3.81" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="94"/>
<pin name="+IN" x="-7.62" y="2.54" visible="off" length="short" direction="in"/>
<pin name="-IN" x="-7.62" y="-2.54" visible="off" length="short" direction="in"/>
<pin name="OUT" x="7.62" y="0" visible="off" length="short" direction="out" rot="R180"/>
<pin name="VDD" x="0" y="7.62" visible="off" length="middle" direction="out" rot="R270"/>
<pin name="VSS" x="0" y="-7.62" visible="off" length="middle" direction="out" rot="R90"/>
<text x="2.54" y="3.175" size="1.778" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="S89431" urn="urn:adsk.eagle:component:8712648/3" prefix="U" library_version="3">
<gates>
<gate name="G$1" symbol="OPAMP" x="0" y="0"/>
</gates>
<devices>
<device name="SOT23-5" package="SOT23-5P">
<connects>
<connect gate="G$1" pin="+IN" pad="P$1"/>
<connect gate="G$1" pin="-IN" pad="P$3"/>
<connect gate="G$1" pin="OUT" pad="P$4"/>
<connect gate="G$1" pin="VDD" pad="P$5"/>
<connect gate="G$1" pin="VSS" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8577893/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="STMicroelectronics" library_urn="urn:adsk.eagle:library:7560276" deviceset="STM32F446RET6" device="QFP" package3d_urn="urn:adsk.eagle:package:7539880/3"/>
<part name="CN8" library="con-jst-ph" library_urn="urn:adsk.eagle:library:7974963" deviceset="PH-6P" device="B6B" package3d_urn="urn:adsk.eagle:package:7974999/3"/>
<part name="CN7" library="con-jst-ph" library_urn="urn:adsk.eagle:library:7974963" deviceset="PH-6P" device="B6B" package3d_urn="urn:adsk.eagle:package:7974999/3"/>
<part name="CN9" library="con-jst-ph" library_urn="urn:adsk.eagle:library:7974963" deviceset="PH-3P" device="B3B" package3d_urn="urn:adsk.eagle:package:7975002/3"/>
<part name="CN13" library="con-jst-ph" library_urn="urn:adsk.eagle:library:7974963" deviceset="PH-3P" device="B3B" package3d_urn="urn:adsk.eagle:package:7975002/3"/>
<part name="CN10" library="con-jst-ph" library_urn="urn:adsk.eagle:library:7974963" deviceset="PH-4P" device="B4B" package3d_urn="urn:adsk.eagle:package:7975001/3"/>
<part name="JP1" library="pinsockets_2" library_urn="urn:adsk.eagle:library:8479384" deviceset="PINSOCKET-2X20" device="_2.54" package3d_urn="urn:adsk.eagle:package:7524224/9"/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="X1" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="XTAL" device="HC49S" package3d_urn="urn:adsk.eagle:package:7672691/5" value="8MHz"/>
<part name="CN2" library="con-jst-zh" library_urn="urn:adsk.eagle:library:8322185" deviceset="ZH-6P" device="B6B" package3d_urn="urn:adsk.eagle:package:8322221/2"/>
<part name="C6" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="CAPACITOR" device="2012-1608" package3d_urn="urn:adsk.eagle:package:7517717/4" value="18p"/>
<part name="C7" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="CAPACITOR" device="2012-1608" package3d_urn="urn:adsk.eagle:package:7517717/4" value="18p"/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C8" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="CAPACITOR" device="2012-1608" package3d_urn="urn:adsk.eagle:package:7517717/4" value="100n"/>
<part name="C10" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="CAPACITOR" device="2012-1608" package3d_urn="urn:adsk.eagle:package:7517717/4" value="100n"/>
<part name="C11" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="CAPACITOR" device="2012-1608" package3d_urn="urn:adsk.eagle:package:7517717/4" value="100n"/>
<part name="C12" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="CAPACITOR" device="2012-1608" package3d_urn="urn:adsk.eagle:package:7517717/4" value="100n"/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="+3V6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R4" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="RESISTER" device="2012" package3d_urn="urn:adsk.eagle:package:7517727/4" value="10k"/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C9" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="CAPACITOR" device="2012-1608" package3d_urn="urn:adsk.eagle:package:7517717/4" value="4.7u"/>
<part name="GND16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SW4" library="grayhill" library_urn="urn:adsk.eagle:library:8431917" deviceset="97C02ST" device="" package3d_urn="urn:adsk.eagle:package:8431923/3"/>
<part name="GND18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="IMU1" library="TDK-InvenSense" library_urn="urn:adsk.eagle:library:8545760" deviceset="ICM-20689" device="QFN" package3d_urn="urn:adsk.eagle:package:7564501/3"/>
<part name="GND19" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R5" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="RESISTER" device="2012" package3d_urn="urn:adsk.eagle:package:7517727/4" value="10k"/>
<part name="GND26" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R1" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="RESISTER" device="2012" package3d_urn="urn:adsk.eagle:package:7517727/4" value="10k"/>
<part name="+3V10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="C5" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="CAPACITOR" device="2012-1608" package3d_urn="urn:adsk.eagle:package:7517717/4" value="10u"/>
<part name="GND27" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C28" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="E_CAPACITOR" device="D63" package3d_urn="urn:adsk.eagle:package:7517723/5" value="100u"/>
<part name="C20" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="CAPACITOR" device="2012-1608" package3d_urn="urn:adsk.eagle:package:7517717/4" value="470n"/>
<part name="GND29" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="C21" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="CAPACITOR" device="2012-1608" package3d_urn="urn:adsk.eagle:package:7517717/4" value="10n"/>
<part name="C22" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="CAPACITOR" device="2012-1608" package3d_urn="urn:adsk.eagle:package:7517717/4" value="100n"/>
<part name="C23" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="CAPACITOR" device="2012-1608" package3d_urn="urn:adsk.eagle:package:7517717/4" value="2u2"/>
<part name="R18" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="RESISTER" device="2012" package3d_urn="urn:adsk.eagle:package:7517727/4" value="470"/>
<part name="GND30" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="Q1" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="N-MOSFET" device="SOT23" package3d_urn="urn:adsk.eagle:package:7672745/2"/>
<part name="GND31" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R17" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="RESISTER" device="2012" package3d_urn="urn:adsk.eagle:package:7517727/4" value="10k"/>
<part name="P+7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="JP2" library="pinheads_2" library_urn="urn:adsk.eagle:library:7523781" deviceset="PINHD-2X02" device="_2.54" package3d_urn="urn:adsk.eagle:package:7524206/3"/>
<part name="LOGO1" library="ajisaka-manufacturing" library_urn="urn:adsk.eagle:library:8516697" deviceset="LOGO" device="8MM" package3d_urn="urn:adsk.eagle:package:8516700/2"/>
<part name="R12" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="RESISTER" device="2012" package3d_urn="urn:adsk.eagle:package:7517727/4" value="4k7"/>
<part name="R13" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="RESISTER" device="2012" package3d_urn="urn:adsk.eagle:package:7517727/4" value="4k7"/>
<part name="+3V1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="SW1" library="E-Switch" library_urn="urn:adsk.eagle:library:8712447" deviceset="TL3301" device="6X6-5MM" package3d_urn="urn:adsk.eagle:package:8712471/2"/>
<part name="GND35" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND38" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="R11" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="RESISTER" device="2012" package3d_urn="urn:adsk.eagle:package:7517727/4" value="N.M."/>
<part name="C19" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="CAPACITOR" device="2012-1608" package3d_urn="urn:adsk.eagle:package:7517717/4" value="100n"/>
<part name="GND39" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U6" library="SII" library_urn="urn:adsk.eagle:library:8712646" deviceset="S89431" device="SOT23-5" package3d_urn="urn:adsk.eagle:package:8577893/3"/>
<part name="R25" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="RESISTER" device="2012" package3d_urn="urn:adsk.eagle:package:7517727/4" value="270"/>
<part name="R26" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="RESISTER" device="2012" package3d_urn="urn:adsk.eagle:package:7517727/4" value="270"/>
<part name="D2" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="LED" device="2012G" package3d_urn="urn:adsk.eagle:package:7669193/2"/>
<part name="D3" library="discrete" library_urn="urn:adsk.eagle:library:7520964" deviceset="LED" device="2012G" package3d_urn="urn:adsk.eagle:package:7669193/2"/>
<part name="+3V17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND40" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND41" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="-210.82" y1="44.45" x2="-210.82" y2="-135.89" width="0.1524" layer="97"/>
<wire x1="-210.82" y1="-135.89" x2="-11.43" y2="-135.89" width="0.1524" layer="97"/>
<wire x1="-11.43" y1="-135.89" x2="-11.43" y2="44.45" width="0.1524" layer="97"/>
<wire x1="-11.43" y1="44.45" x2="-210.82" y2="44.45" width="0.1524" layer="97"/>
<wire x1="-60.96" y1="-138.43" x2="-60.96" y2="-243.84" width="0.1524" layer="97"/>
<wire x1="-60.96" y1="-243.84" x2="29.21" y2="-243.84" width="0.1524" layer="97"/>
<wire x1="140.97" y1="44.45" x2="140.97" y2="-17.78" width="0.1524" layer="97"/>
<wire x1="140.97" y1="-17.78" x2="270.51" y2="-17.78" width="0.1524" layer="97"/>
<wire x1="270.51" y1="-17.78" x2="270.51" y2="44.45" width="0.1524" layer="97"/>
<wire x1="270.51" y1="44.45" x2="140.97" y2="44.45" width="0.1524" layer="97"/>
<wire x1="140.97" y1="-105.41" x2="140.97" y2="-167.64" width="0.1524" layer="97"/>
<wire x1="140.97" y1="-167.64" x2="270.51" y2="-167.64" width="0.1524" layer="97"/>
<wire x1="270.51" y1="-167.64" x2="270.51" y2="-105.41" width="0.1524" layer="97"/>
<wire x1="270.51" y1="-105.41" x2="140.97" y2="-105.41" width="0.1524" layer="97"/>
<wire x1="-63.5" y1="-243.84" x2="-63.5" y2="-138.43" width="0.1524" layer="97"/>
<wire x1="-63.5" y1="-138.43" x2="-210.82" y2="-138.43" width="0.1524" layer="97"/>
<wire x1="-210.82" y1="-138.43" x2="-210.82" y2="-243.84" width="0.1524" layer="97"/>
<wire x1="-210.82" y1="-243.84" x2="-63.5" y2="-243.84" width="0.1524" layer="97"/>
<wire x1="149.86" y1="-182.88" x2="149.86" y2="-243.84" width="0.1524" layer="97"/>
<wire x1="149.86" y1="-243.84" x2="179.07" y2="-243.84" width="0.1524" layer="97"/>
<wire x1="179.07" y1="-243.84" x2="179.07" y2="-182.88" width="0.1524" layer="97"/>
<wire x1="179.07" y1="-182.88" x2="149.86" y2="-182.88" width="0.1524" layer="97"/>
<text x="-209.55" y="36.83" size="6.4516" layer="97">CPU</text>
<text x="-59.69" y="-146.05" size="6.4516" layer="97">INTERFACE</text>
<text x="142.24" y="36.83" size="6.4516" layer="97">IMU</text>
<text x="142.24" y="-113.03" size="6.4516" layer="97">NEOPIXEL</text>
<text x="-209.55" y="-146.05" size="6.4516" layer="97">RASPBERRY PI</text>
<text x="151.13" y="-190.5" size="6.4516" layer="97">LED</text>
<text x="33.02" y="-146.05" size="6.4516" layer="97">LOGO</text>
</plain>
<instances>
<instance part="U1" gate="G$1" x="-100.33" y="-22.86" smashed="yes">
<attribute name="NAME" x="-130.81" y="-2.54" size="1.778" layer="95"/>
<attribute name="VALUE" x="-130.81" y="-88.9" size="1.778" layer="96"/>
</instance>
<instance part="CN8" gate="G$1" x="13.97" y="-177.8" smashed="yes">
<attribute name="NAME" x="16.51" y="-175.26" size="1.27" layer="95"/>
</instance>
<instance part="CN7" gate="G$1" x="13.97" y="-154.94" smashed="yes">
<attribute name="NAME" x="16.51" y="-152.4" size="1.27" layer="95"/>
</instance>
<instance part="CN9" gate="G$1" x="13.97" y="-200.66" smashed="yes">
<attribute name="NAME" x="16.51" y="-198.12" size="1.27" layer="95"/>
</instance>
<instance part="CN13" gate="G$1" x="214.63" y="-135.89" smashed="yes">
<attribute name="NAME" x="217.17" y="-133.223" size="1.27" layer="95"/>
</instance>
<instance part="CN10" gate="G$1" x="13.97" y="-215.9" smashed="yes">
<attribute name="NAME" x="16.51" y="-213.36" size="1.27" layer="95"/>
</instance>
<instance part="JP1" gate="A" x="-132.08" y="-186.69" smashed="yes">
<attribute name="NAME" x="-138.43" y="-160.655" size="1.778" layer="95"/>
<attribute name="VALUE" x="-138.43" y="-217.17" size="1.778" layer="96"/>
</instance>
<instance part="P+1" gate="1" x="-140.97" y="-156.21" smashed="yes">
<attribute name="VALUE" x="-142.875" y="-155.575" size="1.778" layer="96"/>
</instance>
<instance part="GND7" gate="1" x="-140.97" y="-232.41" smashed="yes">
<attribute name="VALUE" x="-140.335" y="-238.76" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND1" gate="1" x="-120.65" y="-232.41" smashed="yes">
<attribute name="VALUE" x="-120.015" y="-238.76" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="X1" gate="G$1" x="-163.83" y="-16.51" smashed="yes" rot="R90">
<attribute name="NAME" x="-163.576" y="-19.558" size="1.778" layer="95"/>
<attribute name="VALUE" x="-172.085" y="-17.145" size="1.778" layer="96"/>
</instance>
<instance part="CN2" gate="G$1" x="-24.13" y="-40.64" smashed="yes">
<attribute name="NAME" x="-20.955" y="-37.465" size="1.27" layer="95"/>
</instance>
<instance part="C6" gate="G$1" x="-170.18" y="-11.43" smashed="yes" rot="R90">
<attribute name="NAME" x="-174.625" y="-10.795" size="1.778" layer="95"/>
<attribute name="VALUE" x="-168.91" y="-10.795" size="1.778" layer="96"/>
</instance>
<instance part="C7" gate="G$1" x="-170.18" y="-21.59" smashed="yes" rot="R90">
<attribute name="NAME" x="-174.625" y="-20.955" size="1.778" layer="95"/>
<attribute name="VALUE" x="-168.91" y="-20.955" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="1" x="-179.07" y="-16.51" smashed="yes" rot="R270">
<attribute name="VALUE" x="-185.42" y="-17.145" size="1.778" layer="96"/>
</instance>
<instance part="+3V2" gate="G$1" x="-57.15" y="5.08" smashed="yes">
<attribute name="VALUE" x="-60.96" y="5.715" size="1.778" layer="96"/>
</instance>
<instance part="+3V3" gate="G$1" x="-146.05" y="29.21" smashed="yes">
<attribute name="VALUE" x="-149.86" y="29.845" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="-138.43" y="-93.98" smashed="yes">
<attribute name="VALUE" x="-137.795" y="-100.33" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND5" gate="1" x="-64.77" y="-93.98" smashed="yes">
<attribute name="VALUE" x="-64.135" y="-100.33" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C8" gate="G$1" x="-142.24" y="-48.26" smashed="yes" rot="R90">
<attribute name="NAME" x="-146.05" y="-48.26" size="1.778" layer="95"/>
<attribute name="VALUE" x="-139.7" y="-51.181" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C10" gate="G$1" x="-142.24" y="-81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="-146.685" y="-84.455" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-139.7" y="-84.455" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C11" gate="G$1" x="-60.96" y="-48.26" smashed="yes" rot="R90">
<attribute name="NAME" x="-59.055" y="-50.165" size="1.778" layer="95"/>
<attribute name="VALUE" x="-58.42" y="-51.435" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C12" gate="G$1" x="-60.96" y="-7.62" smashed="yes" rot="R90">
<attribute name="NAME" x="-56.515" y="-9.525" size="1.778" layer="95"/>
<attribute name="VALUE" x="-58.42" y="-10.795" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+2" gate="1" x="212.09" y="-120.65" smashed="yes">
<attribute name="VALUE" x="210.185" y="-120.015" size="1.778" layer="96"/>
</instance>
<instance part="+3V6" gate="G$1" x="-26.67" y="-33.02" smashed="yes">
<attribute name="VALUE" x="-30.48" y="-32.385" size="1.778" layer="96"/>
</instance>
<instance part="GND14" gate="1" x="-26.67" y="-60.96" smashed="yes">
<attribute name="VALUE" x="-26.035" y="-67.31" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R4" gate="G$1" x="-153.67" y="-73.66" smashed="yes">
<attribute name="NAME" x="-160.02" y="-73.0504" size="1.778" layer="95"/>
<attribute name="VALUE" x="-150.876" y="-73.406" size="1.778" layer="96"/>
</instance>
<instance part="GND15" gate="1" x="-166.37" y="-73.66" smashed="yes" rot="R270">
<attribute name="VALUE" x="-172.72" y="-74.295" size="1.778" layer="96"/>
</instance>
<instance part="C9" gate="G$1" x="-153.67" y="-78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="-159.385" y="-78.105" size="1.778" layer="95"/>
<attribute name="VALUE" x="-155.575" y="-78.994" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND16" gate="1" x="-166.37" y="-78.74" smashed="yes" rot="R270">
<attribute name="VALUE" x="-172.72" y="-79.375" size="1.778" layer="96"/>
</instance>
<instance part="SW4" gate="G$1" x="-130.81" y="-114.3" smashed="yes" rot="R270">
<attribute name="NAME" x="-135.89" y="-111.76" size="1.778" layer="95"/>
</instance>
<instance part="GND18" gate="1" x="11.43" y="-231.14" smashed="yes">
<attribute name="VALUE" x="12.065" y="-237.49" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V8" gate="G$1" x="8.89" y="-147.32" smashed="yes">
<attribute name="VALUE" x="5.08" y="-146.685" size="1.778" layer="96"/>
</instance>
<instance part="IMU1" gate="G$1" x="207.01" y="33.02" smashed="yes">
<attribute name="NAME" x="191.77" y="35.56" size="1.778" layer="95"/>
</instance>
<instance part="GND19" gate="1" x="207.01" y="-5.08" smashed="yes">
<attribute name="VALUE" x="207.645" y="-11.43" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND20" gate="1" x="247.65" y="17.78" smashed="yes" rot="R90">
<attribute name="VALUE" x="254" y="18.415" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R5" gate="G$1" x="-34.29" y="-22.86" smashed="yes" rot="R90">
<attribute name="NAME" x="-36.195" y="-24.765" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-31.115" y="-24.765" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND26" gate="1" x="-34.29" y="-33.02" smashed="yes">
<attribute name="VALUE" x="-33.655" y="-39.37" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R1" gate="G$1" x="-191.77" y="-17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="-193.04" y="-19.05" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-188.595" y="-19.685" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V10" gate="G$1" x="-191.77" y="-7.62" smashed="yes">
<attribute name="VALUE" x="-195.58" y="-6.985" size="1.778" layer="96"/>
</instance>
<instance part="C5" gate="G$1" x="-142.24" y="19.05" smashed="yes" rot="R270">
<attribute name="NAME" x="-140.208" y="16.51" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-144.145" y="21.463" size="1.778" layer="96"/>
</instance>
<instance part="GND27" gate="1" x="-133.35" y="19.05" smashed="yes" rot="R90">
<attribute name="VALUE" x="-127" y="19.685" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C28" gate="G$1" x="220.98" y="-128.27" smashed="yes" rot="R90">
<attribute name="NAME" x="222.885" y="-127.6604" size="1.778" layer="95"/>
<attribute name="VALUE" x="222.885" y="-130.2004" size="1.778" layer="96"/>
</instance>
<instance part="C20" gate="G$1" x="175.26" y="10.16" smashed="yes" rot="R270">
<attribute name="NAME" x="176.149" y="8.255" size="1.778" layer="95"/>
<attribute name="VALUE" x="174.371" y="12.192" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND29" gate="1" x="166.37" y="10.16" smashed="yes" rot="R270">
<attribute name="VALUE" x="160.02" y="9.525" size="1.778" layer="96"/>
</instance>
<instance part="+3V11" gate="G$1" x="184.15" y="20.32" smashed="yes">
<attribute name="VALUE" x="180.34" y="20.955" size="1.778" layer="96"/>
</instance>
<instance part="+3V12" gate="G$1" x="247.65" y="5.08" smashed="yes" rot="R270">
<attribute name="VALUE" x="248.285" y="8.89" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C21" gate="G$1" x="232.41" y="11.43" smashed="yes">
<attribute name="NAME" x="234.315" y="12.319" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="230.378" y="10.541" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C22" gate="G$1" x="237.49" y="11.43" smashed="yes">
<attribute name="NAME" x="239.395" y="12.319" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="235.458" y="10.541" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C23" gate="G$1" x="242.57" y="11.43" smashed="yes">
<attribute name="NAME" x="244.475" y="12.319" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="240.538" y="10.541" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R18" gate="G$1" x="201.93" y="-138.43" smashed="yes" rot="R180">
<attribute name="NAME" x="204.47" y="-138.2014" size="1.778" layer="95"/>
<attribute name="VALUE" x="199.136" y="-138.684" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND30" gate="1" x="232.41" y="-128.27" smashed="yes" rot="R90">
<attribute name="VALUE" x="232.41" y="-128.905" size="1.778" layer="96"/>
</instance>
<instance part="Q1" gate="G$1" x="184.15" y="-146.05" smashed="yes">
<attribute name="NAME" x="189.23" y="-146.05" size="1.778" layer="95"/>
</instance>
<instance part="GND31" gate="1" x="186.69" y="-158.75" smashed="yes">
<attribute name="VALUE" x="189.23" y="-159.385" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R17" gate="G$1" x="186.69" y="-130.81" smashed="yes" rot="R90">
<attribute name="NAME" x="184.3786" y="-133.35" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="186.436" y="-128.016" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+7" gate="1" x="186.69" y="-120.65" smashed="yes">
<attribute name="VALUE" x="184.785" y="-120.015" size="1.778" layer="96"/>
</instance>
<instance part="JP2" gate="A" x="-129.54" y="-224.79" smashed="yes">
<attribute name="NAME" x="-138.43" y="-219.075" size="1.778" layer="95"/>
<attribute name="VALUE" x="-138.43" y="-229.87" size="1.778" layer="96"/>
</instance>
<instance part="LOGO1" gate="G$1" x="50.8" y="-162.56" smashed="yes"/>
<instance part="R12" gate="G$1" x="6.35" y="-228.6" smashed="yes" rot="R270">
<attribute name="NAME" x="6.1214" y="-226.06" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="6.604" y="-231.394" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R13" gate="G$1" x="3.81" y="-228.6" smashed="yes" rot="R270">
<attribute name="NAME" x="3.5814" y="-226.06" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="4.064" y="-231.394" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="+3V1" gate="G$1" x="-1.27" y="-236.22" smashed="yes" rot="R90">
<attribute name="VALUE" x="-1.905" y="-240.03" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SW1" gate="G$1" x="-191.77" y="-35.56" smashed="yes">
<attribute name="NAME" x="-192.405" y="-37.465" size="1.016" layer="95" rot="R90"/>
</instance>
<instance part="GND35" gate="1" x="-191.77" y="-48.26" smashed="yes">
<attribute name="VALUE" x="-191.135" y="-54.61" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND38" gate="1" x="-29.21" y="-220.98" smashed="yes">
<attribute name="VALUE" x="-28.575" y="-227.33" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V16" gate="G$1" x="-29.21" y="-185.42" smashed="yes">
<attribute name="VALUE" x="-33.02" y="-184.785" size="1.778" layer="96"/>
</instance>
<instance part="R11" gate="G$1" x="-21.59" y="-195.58" smashed="yes" rot="R180">
<attribute name="NAME" x="-24.13" y="-194.31" size="1.778" layer="95"/>
<attribute name="VALUE" x="-24.13" y="-198.501" size="1.778" layer="96"/>
</instance>
<instance part="C19" gate="G$1" x="-34.29" y="-190.5" smashed="yes" rot="MR270">
<attribute name="NAME" x="-36.195" y="-192.405" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-35.56" y="-189.865" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND39" gate="1" x="-43.18" y="-190.5" smashed="yes" rot="MR90">
<attribute name="VALUE" x="-45.085" y="-193.04" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="U6" gate="G$1" x="-29.21" y="-205.74" smashed="yes" rot="MR0">
<attribute name="NAME" x="-31.75" y="-202.565" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="R25" gate="G$1" x="166.37" y="-210.82" smashed="yes" rot="R270">
<attribute name="NAME" x="168.6814" y="-208.28" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="166.624" y="-213.614" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R26" gate="G$1" x="161.29" y="-210.82" smashed="yes" rot="R270">
<attribute name="NAME" x="163.6014" y="-208.28" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="161.544" y="-213.614" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="D2" gate="G$1" x="166.37" y="-223.52" smashed="yes" rot="MR0">
<attribute name="NAME" x="166.624" y="-223.012" size="1.778" layer="95" rot="MR90"/>
</instance>
<instance part="D3" gate="G$1" x="161.29" y="-223.52" smashed="yes" rot="MR0">
<attribute name="NAME" x="161.544" y="-223.012" size="1.778" layer="95" rot="MR90"/>
</instance>
<instance part="+3V17" gate="G$1" x="166.37" y="-198.12" smashed="yes">
<attribute name="VALUE" x="167.005" y="-198.12" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V18" gate="G$1" x="161.29" y="-198.12" smashed="yes">
<attribute name="VALUE" x="161.925" y="-198.12" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND40" gate="1" x="166.37" y="-236.22" smashed="yes">
<attribute name="VALUE" x="165.735" y="-236.22" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P+11" gate="VCC" x="-166.37" y="-38.1" smashed="yes" rot="R90">
<attribute name="VALUE" x="-166.37" y="-36.83" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND41" gate="1" x="212.09" y="-158.75" smashed="yes">
<attribute name="VALUE" x="214.63" y="-159.385" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+3" gate="1" x="8.89" y="-208.28" smashed="yes">
<attribute name="VALUE" x="6.985" y="-207.645" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="+5V" class="0">
<segment>
<pinref part="JP1" gate="A" pin="1"/>
<pinref part="P+1" gate="1" pin="+5V"/>
<wire x1="-134.62" y1="-163.83" x2="-140.97" y2="-163.83" width="0.1524" layer="91"/>
<wire x1="-140.97" y1="-163.83" x2="-140.97" y2="-158.75" width="0.1524" layer="91"/>
<wire x1="-140.97" y1="-163.83" x2="-140.97" y2="-166.37" width="0.1524" layer="91"/>
<junction x="-140.97" y="-163.83"/>
<pinref part="JP1" gate="A" pin="3"/>
<wire x1="-140.97" y1="-166.37" x2="-134.62" y2="-166.37" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+2" gate="1" pin="+5V"/>
<wire x1="212.09" y1="-123.19" x2="212.09" y2="-128.27" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="+"/>
<wire x1="217.17" y1="-128.27" x2="212.09" y2="-128.27" width="0.1524" layer="91"/>
<junction x="212.09" y="-128.27"/>
<wire x1="212.09" y1="-128.27" x2="212.09" y2="-135.89" width="0.1524" layer="91"/>
<pinref part="CN13" gate="G$1" pin="1"/>
<wire x1="214.63" y1="-135.89" x2="212.09" y2="-135.89" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+7" gate="1" pin="+5V"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="186.69" y1="-123.19" x2="186.69" y2="-125.73" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CN10" gate="G$1" pin="1"/>
<wire x1="13.97" y1="-215.9" x2="8.89" y2="-215.9" width="0.1524" layer="91"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<wire x1="8.89" y1="-210.82" x2="8.89" y2="-215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VDD4"/>
<wire x1="-67.31" y1="-5.08" x2="-57.15" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<wire x1="-57.15" y1="-5.08" x2="-57.15" y2="2.54" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDD3"/>
<wire x1="-67.31" y1="-45.72" x2="-57.15" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="-57.15" y1="-45.72" x2="-57.15" y2="-7.62" width="0.1524" layer="91"/>
<junction x="-57.15" y="-5.08"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="-57.15" y1="-7.62" x2="-57.15" y2="-5.08" width="0.1524" layer="91"/>
<junction x="-57.15" y="-7.62"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="-57.15" y1="-48.26" x2="-57.15" y2="-45.72" width="0.1524" layer="91"/>
<junction x="-57.15" y="-45.72"/>
</segment>
<segment>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<wire x1="-146.05" y1="19.05" x2="-146.05" y2="26.67" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<junction x="-146.05" y="19.05"/>
<pinref part="U1" gate="G$1" pin="VBAT"/>
<wire x1="-135.89" y1="-5.08" x2="-146.05" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="-146.05" y1="-5.08" x2="-146.05" y2="19.05" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDD2"/>
<wire x1="-135.89" y1="-83.82" x2="-146.05" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="-146.05" y1="-83.82" x2="-146.05" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="-146.05" y1="-81.28" x2="-146.05" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<pinref part="C10" gate="G$1" pin="1"/>
<junction x="-146.05" y="-81.28"/>
<wire x1="-146.05" y1="-50.8" x2="-146.05" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="-146.05" y1="-48.26" x2="-146.05" y2="-5.08" width="0.1524" layer="91"/>
<junction x="-146.05" y="-48.26"/>
<junction x="-146.05" y="-5.08"/>
<pinref part="U1" gate="G$1" pin="VDD1"/>
<wire x1="-135.89" y1="-50.8" x2="-146.05" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-146.05" y="-50.8"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="1"/>
<wire x1="-24.13" y1="-40.64" x2="-26.67" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
<wire x1="-26.67" y1="-40.64" x2="-26.67" y2="-35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CN7" gate="G$1" pin="1"/>
<wire x1="13.97" y1="-154.94" x2="8.89" y2="-154.94" width="0.1524" layer="91"/>
<wire x1="8.89" y1="-154.94" x2="8.89" y2="-177.8" width="0.1524" layer="91"/>
<pinref part="CN8" gate="G$1" pin="1"/>
<wire x1="8.89" y1="-177.8" x2="13.97" y2="-177.8" width="0.1524" layer="91"/>
<wire x1="8.89" y1="-177.8" x2="8.89" y2="-200.66" width="0.1524" layer="91"/>
<junction x="8.89" y="-177.8"/>
<pinref part="CN9" gate="G$1" pin="1"/>
<wire x1="8.89" y1="-200.66" x2="13.97" y2="-200.66" width="0.1524" layer="91"/>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
<wire x1="8.89" y1="-149.86" x2="8.89" y2="-154.94" width="0.1524" layer="91"/>
<junction x="8.89" y="-154.94"/>
</segment>
<segment>
<pinref part="+3V10" gate="G$1" pin="+3V3"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="-191.77" y1="-10.16" x2="-191.77" y2="-12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IMU1" gate="G$1" pin="VDDIO"/>
<wire x1="189.23" y1="15.24" x2="184.15" y2="15.24" width="0.1524" layer="91"/>
<pinref part="+3V11" gate="G$1" pin="+3V3"/>
<wire x1="184.15" y1="15.24" x2="184.15" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IMU1" gate="G$1" pin="VDD"/>
<wire x1="224.79" y1="5.08" x2="232.41" y2="5.08" width="0.1524" layer="91"/>
<pinref part="+3V12" gate="G$1" pin="+3V3"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="232.41" y1="5.08" x2="237.49" y2="5.08" width="0.1524" layer="91"/>
<wire x1="237.49" y1="5.08" x2="242.57" y2="5.08" width="0.1524" layer="91"/>
<wire x1="242.57" y1="5.08" x2="245.11" y2="5.08" width="0.1524" layer="91"/>
<wire x1="232.41" y1="7.62" x2="232.41" y2="5.08" width="0.1524" layer="91"/>
<junction x="232.41" y="5.08"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="237.49" y1="7.62" x2="237.49" y2="5.08" width="0.1524" layer="91"/>
<junction x="237.49" y="5.08"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="242.57" y1="7.62" x2="242.57" y2="5.08" width="0.1524" layer="91"/>
<junction x="242.57" y="5.08"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="6.35" y1="-233.68" x2="6.35" y2="-236.22" width="0.1524" layer="91"/>
<wire x1="6.35" y1="-236.22" x2="3.81" y2="-236.22" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="3.81" y1="-236.22" x2="3.81" y2="-233.68" width="0.1524" layer="91"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="1.27" y1="-236.22" x2="3.81" y2="-236.22" width="0.1524" layer="91"/>
<junction x="3.81" y="-236.22"/>
</segment>
<segment>
<pinref part="+3V16" gate="G$1" pin="+3V3"/>
<wire x1="-29.21" y1="-198.12" x2="-29.21" y2="-190.5" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="-29.21" y1="-190.5" x2="-29.21" y2="-187.96" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-190.5" x2="-29.21" y2="-190.5" width="0.1524" layer="91"/>
<junction x="-29.21" y="-190.5"/>
<pinref part="U6" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="166.37" y1="-205.74" x2="166.37" y2="-200.66" width="0.1524" layer="91"/>
<pinref part="+3V17" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="161.29" y1="-205.74" x2="161.29" y2="-200.66" width="0.1524" layer="91"/>
<pinref part="+3V18" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="JP1" gate="A" pin="33"/>
<wire x1="-134.62" y1="-204.47" x2="-140.97" y2="-204.47" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="-140.97" y1="-229.87" x2="-140.97" y2="-204.47" width="0.1524" layer="91"/>
<junction x="-140.97" y="-204.47"/>
<pinref part="JP1" gate="A" pin="29"/>
<wire x1="-134.62" y1="-199.39" x2="-140.97" y2="-199.39" width="0.1524" layer="91"/>
<wire x1="-140.97" y1="-204.47" x2="-140.97" y2="-199.39" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="19"/>
<wire x1="-134.62" y1="-186.69" x2="-140.97" y2="-186.69" width="0.1524" layer="91"/>
<wire x1="-140.97" y1="-199.39" x2="-140.97" y2="-186.69" width="0.1524" layer="91"/>
<junction x="-140.97" y="-199.39"/>
<junction x="-140.97" y="-186.69"/>
<pinref part="JP1" gate="A" pin="13"/>
<wire x1="-134.62" y1="-179.07" x2="-140.97" y2="-179.07" width="0.1524" layer="91"/>
<wire x1="-140.97" y1="-186.69" x2="-140.97" y2="-179.07" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="5"/>
<wire x1="-140.97" y1="-168.91" x2="-134.62" y2="-168.91" width="0.1524" layer="91"/>
<wire x1="-140.97" y1="-179.07" x2="-140.97" y2="-168.91" width="0.1524" layer="91"/>
<junction x="-140.97" y="-179.07"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="10"/>
<wire x1="-127" y1="-173.99" x2="-120.65" y2="-173.99" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="-120.65" y1="-229.87" x2="-120.65" y2="-212.09" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="40"/>
<wire x1="-120.65" y1="-212.09" x2="-120.65" y2="-194.31" width="0.1524" layer="91"/>
<wire x1="-120.65" y1="-194.31" x2="-120.65" y2="-173.99" width="0.1524" layer="91"/>
<wire x1="-127" y1="-212.09" x2="-120.65" y2="-212.09" width="0.1524" layer="91"/>
<junction x="-120.65" y="-212.09"/>
<pinref part="JP1" gate="A" pin="26"/>
<wire x1="-127" y1="-194.31" x2="-120.65" y2="-194.31" width="0.1524" layer="91"/>
<junction x="-120.65" y="-194.31"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="-173.99" y1="-21.59" x2="-176.53" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="-176.53" y1="-21.59" x2="-176.53" y2="-16.51" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="-176.53" y1="-16.51" x2="-176.53" y2="-11.43" width="0.1524" layer="91"/>
<wire x1="-176.53" y1="-11.43" x2="-173.99" y2="-11.43" width="0.1524" layer="91"/>
<pinref part="GND3" gate="1" pin="GND"/>
<junction x="-176.53" y="-16.51"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="-138.43" y1="-91.44" x2="-138.43" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VSS2"/>
<wire x1="-138.43" y1="-81.28" x2="-135.89" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VSS1"/>
<wire x1="-135.89" y1="-48.26" x2="-138.43" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="-138.43" y1="-48.26" x2="-138.43" y2="-81.28" width="0.1524" layer="91"/>
<junction x="-138.43" y="-81.28"/>
<junction x="-138.43" y="-48.26"/>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="U1" gate="G$1" pin="VSSA"/>
<wire x1="-135.89" y1="-33.02" x2="-138.43" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-138.43" y1="-33.02" x2="-138.43" y2="-48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VSS3"/>
<wire x1="-67.31" y1="-48.26" x2="-64.77" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="-64.77" y1="-48.26" x2="-64.77" y2="-91.44" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VSS4"/>
<wire x1="-67.31" y1="-7.62" x2="-64.77" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-64.77" y1="-7.62" x2="-64.77" y2="-48.26" width="0.1524" layer="91"/>
<junction x="-64.77" y="-48.26"/>
<pinref part="C11" gate="G$1" pin="1"/>
<pinref part="C12" gate="G$1" pin="1"/>
<junction x="-64.77" y="-7.62"/>
</segment>
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<pinref part="CN2" gate="G$1" pin="3"/>
<wire x1="-26.67" y1="-45.72" x2="-26.67" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="-24.13" y1="-45.72" x2="-26.67" y2="-45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="-163.83" y1="-73.66" x2="-158.75" y2="-73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="1"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="-157.48" y1="-78.74" x2="-163.83" y2="-78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="11.43" y1="-228.6" x2="11.43" y2="-223.52" width="0.1524" layer="91"/>
<pinref part="CN8" gate="G$1" pin="6"/>
<wire x1="11.43" y1="-223.52" x2="11.43" y2="-205.74" width="0.1524" layer="91"/>
<wire x1="11.43" y1="-205.74" x2="11.43" y2="-190.5" width="0.1524" layer="91"/>
<wire x1="11.43" y1="-190.5" x2="13.97" y2="-190.5" width="0.1524" layer="91"/>
<pinref part="CN7" gate="G$1" pin="6"/>
<wire x1="11.43" y1="-167.64" x2="13.97" y2="-167.64" width="0.1524" layer="91"/>
<wire x1="11.43" y1="-190.5" x2="11.43" y2="-167.64" width="0.1524" layer="91"/>
<junction x="11.43" y="-190.5"/>
<pinref part="CN9" gate="G$1" pin="3"/>
<wire x1="13.97" y1="-205.74" x2="11.43" y2="-205.74" width="0.1524" layer="91"/>
<junction x="11.43" y="-205.74"/>
<pinref part="CN10" gate="G$1" pin="4"/>
<wire x1="13.97" y1="-223.52" x2="11.43" y2="-223.52" width="0.1524" layer="91"/>
<junction x="11.43" y="-223.52"/>
</segment>
<segment>
<pinref part="IMU1" gate="G$1" pin="GND2"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="207.01" y1="0" x2="207.01" y2="-2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IMU1" gate="G$1" pin="GND1"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="224.79" y1="17.78" x2="232.41" y2="17.78" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="232.41" y1="17.78" x2="237.49" y2="17.78" width="0.1524" layer="91"/>
<wire x1="237.49" y1="17.78" x2="242.57" y2="17.78" width="0.1524" layer="91"/>
<wire x1="242.57" y1="17.78" x2="245.11" y2="17.78" width="0.1524" layer="91"/>
<wire x1="232.41" y1="15.24" x2="232.41" y2="17.78" width="0.1524" layer="91"/>
<junction x="232.41" y="17.78"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="237.49" y1="15.24" x2="237.49" y2="17.78" width="0.1524" layer="91"/>
<junction x="237.49" y="17.78"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="242.57" y1="15.24" x2="242.57" y2="17.78" width="0.1524" layer="91"/>
<junction x="242.57" y="17.78"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="-34.29" y1="-27.94" x2="-34.29" y2="-30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND27" gate="1" pin="GND"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="-135.89" y1="19.05" x2="-138.43" y2="19.05" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND29" gate="1" pin="GND"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="168.91" y1="10.16" x2="171.45" y2="10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="-"/>
<pinref part="GND30" gate="1" pin="GND"/>
<wire x1="224.79" y1="-128.27" x2="229.87" y2="-128.27" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="S"/>
<pinref part="GND31" gate="1" pin="GND"/>
<wire x1="186.69" y1="-151.13" x2="186.69" y2="-156.21" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND35" gate="1" pin="GND"/>
<wire x1="-191.77" y1="-45.72" x2="-191.77" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="P$3"/>
<wire x1="-196.85" y1="-38.1" x2="-196.85" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-196.85" y1="-40.64" x2="-191.77" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="P$4"/>
<wire x1="-186.69" y1="-38.1" x2="-186.69" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-186.69" y1="-40.64" x2="-191.77" y2="-40.64" width="0.1524" layer="91"/>
<junction x="-191.77" y="-40.64"/>
</segment>
<segment>
<pinref part="GND38" gate="1" pin="GND"/>
<wire x1="-29.21" y1="-218.44" x2="-29.21" y2="-213.36" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="VSS"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="-38.1" y1="-190.5" x2="-40.64" y2="-190.5" width="0.1524" layer="91"/>
<pinref part="GND39" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D2" gate="G$1" pin="K"/>
<wire x1="166.37" y1="-228.6" x2="166.37" y2="-233.68" width="0.1524" layer="91"/>
<pinref part="GND40" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="CN13" gate="G$1" pin="3"/>
<wire x1="214.63" y1="-140.97" x2="212.09" y2="-140.97" width="0.1524" layer="91"/>
<wire x1="212.09" y1="-140.97" x2="212.09" y2="-156.21" width="0.1524" layer="91"/>
<pinref part="GND41" gate="1" pin="GND"/>
</segment>
</net>
<net name="RPI_TX" class="0">
<segment>
<pinref part="JP1" gate="A" pin="7"/>
<wire x1="-134.62" y1="-171.45" x2="-146.05" y2="-171.45" width="0.1524" layer="91"/>
<label x="-146.05" y="-171.45" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="RPI_RX" class="0">
<segment>
<pinref part="JP1" gate="A" pin="9"/>
<wire x1="-134.62" y1="-173.99" x2="-146.05" y2="-173.99" width="0.1524" layer="91"/>
<label x="-146.05" y="-173.99" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="RPI_NRST" class="0">
<segment>
<pinref part="JP1" gate="A" pin="12"/>
<wire x1="-127" y1="-176.53" x2="-115.57" y2="-176.53" width="0.1524" layer="91"/>
<label x="-115.57" y="-176.53" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SW4" gate="G$1" pin="4"/>
<wire x1="-123.19" y1="-114.3" x2="-118.11" y2="-114.3" width="0.1524" layer="91"/>
<label x="-118.11" y="-114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="RPI_BOOT0" class="0">
<segment>
<pinref part="JP1" gate="A" pin="16"/>
<wire x1="-127" y1="-181.61" x2="-115.57" y2="-181.61" width="0.1524" layer="91"/>
<label x="-115.57" y="-181.61" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SW4" gate="G$1" pin="3"/>
<wire x1="-123.19" y1="-116.84" x2="-118.11" y2="-116.84" width="0.1524" layer="91"/>
<label x="-118.11" y="-116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="RPI_LED_PWM" class="0">
<segment>
<pinref part="JP1" gate="A" pin="34"/>
<wire x1="-127" y1="-204.47" x2="-118.11" y2="-204.47" width="0.1524" layer="91"/>
<label x="-118.11" y="-204.47" size="1.778" layer="95"/>
</segment>
<segment>
<label x="161.29" y="-148.59" size="1.778" layer="95" rot="MR0"/>
<pinref part="Q1" gate="G$1" pin="G"/>
<wire x1="181.61" y1="-148.59" x2="161.29" y2="-148.59" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PH1/OSC_OUT"/>
<wire x1="-135.89" y1="-17.78" x2="-158.75" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-158.75" y1="-17.78" x2="-158.75" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="-158.75" y1="-21.59" x2="-163.83" y2="-21.59" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="1"/>
<wire x1="-163.83" y1="-21.59" x2="-163.83" y2="-19.05" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="-166.37" y1="-21.59" x2="-163.83" y2="-21.59" width="0.1524" layer="91"/>
<junction x="-163.83" y="-21.59"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PH0/OSC_IN"/>
<wire x1="-135.89" y1="-15.24" x2="-158.75" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="-158.75" y1="-15.24" x2="-158.75" y2="-11.43" width="0.1524" layer="91"/>
<wire x1="-158.75" y1="-11.43" x2="-163.83" y2="-11.43" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="2"/>
<wire x1="-163.83" y1="-11.43" x2="-163.83" y2="-13.97" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="-166.37" y1="-11.43" x2="-163.83" y2="-11.43" width="0.1524" layer="91"/>
<junction x="-163.83" y="-11.43"/>
</segment>
</net>
<net name="ADC0" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC0"/>
<wire x1="-135.89" y1="-22.86" x2="-156.21" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="-156.21" y1="-22.86" x2="-156.21" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-156.21" y1="-27.94" x2="-163.83" y2="-27.94" width="0.1524" layer="91"/>
<label x="-163.83" y="-27.94" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="CN8" gate="G$1" pin="2"/>
<label x="3.81" y="-180.34" size="1.778" layer="95" rot="R180"/>
<wire x1="13.97" y1="-180.34" x2="3.81" y2="-180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADC1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC1"/>
<wire x1="-135.89" y1="-25.4" x2="-154.94" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="-154.94" y1="-25.4" x2="-154.94" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-154.94" y1="-30.48" x2="-163.83" y2="-30.48" width="0.1524" layer="91"/>
<label x="-163.83" y="-30.48" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="CN8" gate="G$1" pin="3"/>
<label x="3.81" y="-182.88" size="1.778" layer="95" rot="R180"/>
<wire x1="13.97" y1="-182.88" x2="3.81" y2="-182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADC2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC2"/>
<wire x1="-135.89" y1="-27.94" x2="-153.67" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-153.67" y1="-27.94" x2="-153.67" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-153.67" y1="-33.02" x2="-163.83" y2="-33.02" width="0.1524" layer="91"/>
<label x="-163.83" y="-33.02" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="CN8" gate="G$1" pin="4"/>
<label x="3.81" y="-185.42" size="1.778" layer="95" rot="R180"/>
<wire x1="13.97" y1="-185.42" x2="3.81" y2="-185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADC3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC3"/>
<wire x1="-135.89" y1="-30.48" x2="-152.4" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-152.4" y1="-30.48" x2="-152.4" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-152.4" y1="-35.56" x2="-163.83" y2="-35.56" width="0.1524" layer="91"/>
<label x="-163.83" y="-35.56" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="CN8" gate="G$1" pin="5"/>
<label x="3.81" y="-187.96" size="1.778" layer="95" rot="R180"/>
<wire x1="13.97" y1="-187.96" x2="3.81" y2="-187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="NRST" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="NRST"/>
<wire x1="-135.89" y1="-20.32" x2="-157.48" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="-20.32" x2="-157.48" y2="-25.4" width="0.1524" layer="91"/>
<label x="-196.85" y="-25.4" size="1.778" layer="95" rot="MR0"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-191.77" y1="-22.86" x2="-191.77" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="-191.77" y1="-25.4" x2="-157.48" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="-191.77" y1="-25.4" x2="-196.85" y2="-25.4" width="0.1524" layer="91"/>
<junction x="-191.77" y="-25.4"/>
<wire x1="-191.77" y1="-25.4" x2="-191.77" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="P$1"/>
<wire x1="-196.85" y1="-33.02" x2="-196.85" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-196.85" y1="-30.48" x2="-191.77" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="P$2"/>
<wire x1="-186.69" y1="-33.02" x2="-186.69" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-186.69" y1="-30.48" x2="-191.77" y2="-30.48" width="0.1524" layer="91"/>
<junction x="-191.77" y="-30.48"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="5"/>
<wire x1="-24.13" y1="-50.8" x2="-31.75" y2="-50.8" width="0.1524" layer="91"/>
<label x="-31.75" y="-50.8" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="SW4" gate="G$1" pin="1"/>
<wire x1="-138.43" y1="-114.3" x2="-143.51" y2="-114.3" width="0.1524" layer="91"/>
<label x="-143.51" y="-114.3" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="STM_TX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA2"/>
<wire x1="-135.89" y1="-43.18" x2="-163.83" y2="-43.18" width="0.1524" layer="91"/>
<label x="-163.83" y="-43.18" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="STM_RX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA3"/>
<wire x1="-135.89" y1="-45.72" x2="-163.83" y2="-45.72" width="0.1524" layer="91"/>
<label x="-163.83" y="-45.72" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="IMU_NCS" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA15/JTDI"/>
<wire x1="-67.31" y1="-40.64" x2="-52.07" y2="-40.64" width="0.1524" layer="91"/>
<label x="-52.07" y="-40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IMU1" gate="G$1" pin="NCS"/>
<wire x1="224.79" y1="27.94" x2="229.87" y2="27.94" width="0.1524" layer="91"/>
<label x="229.87" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="IMU_NINT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD2"/>
<wire x1="-67.31" y1="-30.48" x2="-52.07" y2="-30.48" width="0.1524" layer="91"/>
<label x="-52.07" y="-30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IMU1" gate="G$1" pin="INT"/>
<label x="168.91" y="2.54" size="1.778" layer="95" rot="MR0"/>
<wire x1="189.23" y1="5.08" x2="179.07" y2="5.08" width="0.1524" layer="91"/>
<wire x1="179.07" y1="5.08" x2="179.07" y2="2.54" width="0.1524" layer="91"/>
<wire x1="179.07" y1="2.54" x2="168.91" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IMU_SCK" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB3/JTDO/TRACESWO"/>
<wire x1="-67.31" y1="-27.94" x2="-52.07" y2="-27.94" width="0.1524" layer="91"/>
<label x="-52.07" y="-27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IMU1" gate="G$1" pin="SCL/SCLK"/>
<wire x1="224.79" y1="30.48" x2="229.87" y2="30.48" width="0.1524" layer="91"/>
<label x="229.87" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="IMU_MISO" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB4/NJTRST"/>
<wire x1="-67.31" y1="-25.4" x2="-52.07" y2="-25.4" width="0.1524" layer="91"/>
<label x="-52.07" y="-25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IMU1" gate="G$1" pin="ADO/SDO"/>
<label x="168.91" y="15.24" size="1.778" layer="95" rot="MR0"/>
<wire x1="189.23" y1="12.7" x2="179.07" y2="12.7" width="0.1524" layer="91"/>
<wire x1="179.07" y1="12.7" x2="179.07" y2="15.24" width="0.1524" layer="91"/>
<wire x1="179.07" y1="15.24" x2="168.91" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IMU_MOSI" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB5"/>
<wire x1="-67.31" y1="-22.86" x2="-52.07" y2="-22.86" width="0.1524" layer="91"/>
<label x="-52.07" y="-22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IMU1" gate="G$1" pin="SDA/SDI"/>
<wire x1="224.79" y1="33.02" x2="229.87" y2="33.02" width="0.1524" layer="91"/>
<label x="229.87" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C_SCL" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB6"/>
<wire x1="-67.31" y1="-20.32" x2="-52.07" y2="-20.32" width="0.1524" layer="91"/>
<label x="-52.07" y="-20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CN10" gate="G$1" pin="3"/>
<wire x1="13.97" y1="-220.98" x2="3.81" y2="-220.98" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="3.81" y1="-220.98" x2="-3.81" y2="-220.98" width="0.1524" layer="91"/>
<wire x1="3.81" y1="-223.52" x2="3.81" y2="-220.98" width="0.1524" layer="91"/>
<junction x="3.81" y="-220.98"/>
<label x="-3.81" y="-220.98" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="6"/>
<wire x1="-127" y1="-168.91" x2="-115.57" y2="-168.91" width="0.1524" layer="91"/>
<label x="-115.57" y="-168.91" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C_SDA" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB7"/>
<wire x1="-67.31" y1="-17.78" x2="-52.07" y2="-17.78" width="0.1524" layer="91"/>
<label x="-52.07" y="-17.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CN10" gate="G$1" pin="2"/>
<wire x1="13.97" y1="-218.44" x2="6.35" y2="-218.44" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="6.35" y1="-218.44" x2="-3.81" y2="-218.44" width="0.1524" layer="91"/>
<wire x1="6.35" y1="-223.52" x2="6.35" y2="-218.44" width="0.1524" layer="91"/>
<junction x="6.35" y="-218.44"/>
<label x="-3.81" y="-218.44" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="4"/>
<wire x1="-127" y1="-166.37" x2="-115.57" y2="-166.37" width="0.1524" layer="91"/>
<label x="-115.57" y="-166.37" size="1.778" layer="95"/>
</segment>
</net>
<net name="SWCLK" class="0">
<segment>
<pinref part="CN2" gate="G$1" pin="2"/>
<pinref part="U1" gate="G$1" pin="PA14/JTCK/SWCLK"/>
<label x="-52.07" y="-43.18" size="1.778" layer="95"/>
<wire x1="-24.13" y1="-43.18" x2="-67.31" y2="-43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<pinref part="CN2" gate="G$1" pin="4"/>
<wire x1="-24.13" y1="-48.26" x2="-41.91" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="-41.91" y1="-48.26" x2="-41.91" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PA13/STMS/SWDIO"/>
<wire x1="-67.31" y1="-50.8" x2="-41.91" y2="-50.8" width="0.1524" layer="91"/>
<label x="-52.07" y="-50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="DIO3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA11"/>
<wire x1="-67.31" y1="-55.88" x2="-52.07" y2="-55.88" width="0.1524" layer="91"/>
<label x="-52.07" y="-55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CN7" gate="G$1" pin="5"/>
<label x="3.81" y="-165.1" size="1.778" layer="95" rot="R180"/>
<wire x1="13.97" y1="-165.1" x2="3.81" y2="-165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DIO2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA10"/>
<wire x1="-67.31" y1="-58.42" x2="-52.07" y2="-58.42" width="0.1524" layer="91"/>
<label x="-52.07" y="-58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CN7" gate="G$1" pin="4"/>
<label x="3.81" y="-162.56" size="1.778" layer="95" rot="R180"/>
<wire x1="13.97" y1="-162.56" x2="3.81" y2="-162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DIO1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA9"/>
<wire x1="-67.31" y1="-60.96" x2="-52.07" y2="-60.96" width="0.1524" layer="91"/>
<label x="-52.07" y="-60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CN7" gate="G$1" pin="3"/>
<label x="3.81" y="-160.02" size="1.778" layer="95" rot="R180"/>
<wire x1="13.97" y1="-160.02" x2="3.81" y2="-160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DIO0" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA8"/>
<wire x1="-67.31" y1="-63.5" x2="-52.07" y2="-63.5" width="0.1524" layer="91"/>
<label x="-52.07" y="-63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CN7" gate="G$1" pin="2"/>
<label x="3.81" y="-157.48" size="1.778" layer="95" rot="R180"/>
<wire x1="13.97" y1="-157.48" x2="3.81" y2="-157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ADC4" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA4"/>
<wire x1="-135.89" y1="-53.34" x2="-163.83" y2="-53.34" width="0.1524" layer="91"/>
<label x="-163.83" y="-53.34" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<wire x1="-21.59" y1="-208.28" x2="-16.51" y2="-208.28" width="0.1524" layer="91"/>
<wire x1="-39.37" y1="-215.9" x2="-39.37" y2="-205.74" width="0.1524" layer="91"/>
<wire x1="-39.37" y1="-205.74" x2="-36.83" y2="-205.74" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="-26.67" y1="-195.58" x2="-39.37" y2="-195.58" width="0.1524" layer="91"/>
<wire x1="-39.37" y1="-195.58" x2="-39.37" y2="-205.74" width="0.1524" layer="91"/>
<junction x="-39.37" y="-205.74"/>
<wire x1="-39.37" y1="-205.74" x2="-49.53" y2="-205.74" width="0.1524" layer="91"/>
<label x="-49.53" y="-205.74" size="1.778" layer="95" rot="R180"/>
<pinref part="U6" gate="G$1" pin="-IN"/>
<pinref part="U6" gate="G$1" pin="OUT"/>
<wire x1="-39.37" y1="-215.9" x2="-16.51" y2="-215.9" width="0.1524" layer="91"/>
<wire x1="-16.51" y1="-215.9" x2="-16.51" y2="-208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA5"/>
<wire x1="-135.89" y1="-55.88" x2="-163.83" y2="-55.88" width="0.1524" layer="91"/>
<label x="-163.83" y="-55.88" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="K"/>
<wire x1="161.29" y1="-228.6" x2="161.29" y2="-233.68" width="0.1524" layer="91"/>
<label x="161.29" y="-233.68" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB2/BOOT1"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="-135.89" y1="-73.66" x2="-148.59" y2="-73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VCAP_1"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="-135.89" y1="-78.74" x2="-149.86" y2="-78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="IMU1" gate="G$1" pin="REGOUT"/>
<wire x1="189.23" y1="10.16" x2="179.07" y2="10.16" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
</segment>
</net>
<net name="BOOT0" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="BOOT0"/>
<wire x1="-67.31" y1="-15.24" x2="-34.29" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-34.29" y1="-15.24" x2="-34.29" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-34.29" y1="-15.24" x2="-29.21" y2="-15.24" width="0.1524" layer="91"/>
<junction x="-34.29" y="-15.24"/>
<label x="-29.21" y="-15.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SW4" gate="G$1" pin="2"/>
<wire x1="-138.43" y1="-116.84" x2="-143.51" y2="-116.84" width="0.1524" layer="91"/>
<label x="-143.51" y="-116.84" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="214.63" y1="-138.43" x2="207.01" y2="-138.43" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="CN13" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="196.85" y1="-138.43" x2="186.69" y2="-138.43" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="186.69" y1="-138.43" x2="186.69" y2="-140.97" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="186.69" y1="-135.89" x2="186.69" y2="-138.43" width="0.1524" layer="91"/>
<junction x="186.69" y="-138.43"/>
</segment>
</net>
<net name="ADC_CTRL" class="0">
<segment>
<wire x1="-21.59" y1="-203.2" x2="-13.97" y2="-203.2" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="+IN"/>
<wire x1="-13.97" y1="-195.58" x2="-13.97" y2="-203.2" width="0.1524" layer="91"/>
<junction x="-13.97" y="-203.2"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="-16.51" y1="-195.58" x2="-13.97" y2="-195.58" width="0.1524" layer="91"/>
<pinref part="CN9" gate="G$1" pin="2"/>
<label x="-11.43" y="-203.2" size="1.778" layer="95"/>
<wire x1="-13.97" y1="-203.2" x2="13.97" y2="-203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="166.37" y1="-215.9" x2="166.37" y2="-220.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="161.29" y1="-215.9" x2="161.29" y2="-220.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VDDA"/>
<wire x1="-135.89" y1="-35.56" x2="-151.13" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-151.13" y1="-35.56" x2="-151.13" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="-151.13" y1="-38.1" x2="-163.83" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="P+11" gate="VCC" pin="VCC"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
